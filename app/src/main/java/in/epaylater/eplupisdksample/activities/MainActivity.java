package in.epaylater.eplupisdksample.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import in.epaylater.eplpaymentsdk.constant.EPLConstants;
import in.epaylater.eplpaymentsdk.fragments.EPLBaseFragment;
import in.epaylater.eplpaymentsdk.fragments.EPLDashboardFragment;
import in.epaylater.eplpaymentsdk.fragments.EPLHomeFragment;
import in.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner;
import in.epaylater.eplpaymentsdk.managers.EPLManager;
import in.epaylater.eplpaymentsdk.models.IDFCInitProfileModel;
import in.epaylater.eplpaymentsdk.utils.EPLCommonUtil;
import in.epaylater.eplupisdksample.BuildConfig;
import in.epaylater.eplupisdksample.R;
import in.epaylater.eplupisdksample.fragments.HomeFragment;
import in.epaylater.eplupisdksample.fragments.PostPaidFragment;
import in.epaylater.sdksampleapp.IntegrateSDK;
import org.jetbrains.annotations.NotNull;

import java.util.List;


public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, IntegrateSDK, EpayFragmentListner {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BuildConfig.DEBUG) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        addFragment(new HomeFragment(), "IDFCHomeFragment");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (EPLCommonUtil.INSTANCE.objectValidator(fragment)) {
                if (fragment instanceof EPLDashboardFragment) {
                    if (fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                        Fragment currentVisibleFragment = fragment.getChildFragmentManager().findFragmentById(R.id.eplContainer);
                        if (EPLCommonUtil.INSTANCE.objectValidator(currentVisibleFragment) && currentVisibleFragment instanceof EPLHomeFragment) {
                            ((EPLHomeFragment) currentVisibleFragment).onBackPressed();
                        } else {
                            ((EPLBaseFragment) fragment).popTopFragment(fragment);
                        }
                    } else {
                        List<Fragment> fragmentList = fragment.getChildFragmentManager().getFragments();
                        if (fragmentList.size() > 0) {
                            if (fragmentList.get(0) instanceof EPLHomeFragment) {
                                ((EPLHomeFragment) fragmentList.get(0)).onBackPressed();
                            } else {
                                super.onBackPressed();
                            }
                        } else {
                            super.onBackPressed();
                        }
                    }
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_home:
                replaceFragment(new HomeFragment(), "IDFCHomeFragment");
                break;

            case R.id.nav_postpaid:
                replaceFragment(new PostPaidFragment(), "Postpaid");
                break;

            default:
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EPLConstants.REQUEST_CODE.EPL_REQUEST_SETTING || requestCode == EPLConstants.REQUEST_CODE.EPL_REQUEST_SEND_SMS) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (EPLCommonUtil.INSTANCE.objectValidator(fragment)) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == EPLConstants.REQUEST_CODE.REQUEST_CODE_PERMISSION || requestCode == EPLConstants.REQUEST_CODE.EPL_REQUEST_SEND_EMAIL) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void changeEplFragment(Fragment fragment, int type) {
        Fragment currentVisibleFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (EPLCommonUtil.INSTANCE.objectValidator(currentVisibleFragment) && currentVisibleFragment instanceof EPLDashboardFragment) {
            ((EPLDashboardFragment) currentVisibleFragment).switchFragment(fragment, type);
        }
    }

    @Override
    public void startIntegrate(@NotNull IDFCInitProfileModel model) {
        EPLManager.INSTANCE.startIntegration(getApplication(), model, getSupportFragmentManager(), R.id.container);
    }


    @Override
    public void setActionTitle(@NotNull String title, int headerViewType, int keyboardAdjustType) {
        try {
            getSupportActionBar().setTitle(title);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (keyboardAdjustType == 1) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        } else {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }
}
