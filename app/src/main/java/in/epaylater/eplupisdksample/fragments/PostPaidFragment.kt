package `in`.epaylater.eplupisdksample.fragments

import `in`.epaylater.eplpaymentsdk.models.IDFCInitProfileModel
import `in`.epaylater.eplupisdksample.R
import `in`.epaylater.eplupisdksample.interfaces.MyTextChangeListner
import `in`.epaylater.sdksampleapp.IntegrateSDK
import `in`.epaylater.sdksampleapp.MyCustomTextWatcher
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_postpaid.*

class PostPaidFragment : Fragment(), View.OnClickListener, MyTextChangeListner {


    val isDebug = false

    private var integrateSDK: IntegrateSDK? = null

    var idfcMerchantKey =
        if (isDebug) "4228714c-8eea-4d3a-882a-3200046f204e" else "c3e25c9e-996b-11e9-a2a3-2a2ae2dbcce4"


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        integrateSDK = activity as IntegrateSDK
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_postpaid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnIntegrate.setOnClickListener(this)
        setDefaultValue()
        etFirstName.addTextChangedListener(MyCustomTextWatcher(this))
        etLastName.addTextChangedListener(MyCustomTextWatcher(this))
        etMiddleName.addTextChangedListener(MyCustomTextWatcher(this))
        etPhone.addTextChangedListener(MyCustomTextWatcher(this))
        etEmail.addTextChangedListener(MyCustomTextWatcher(this))
        etSavingAccountNo.addTextChangedListener(MyCustomTextWatcher(this))
        etSavingAccountName.addTextChangedListener(MyCustomTextWatcher(this))
        etODAccountNo.addTextChangedListener(MyCustomTextWatcher(this))
        etNameOnSavingAccount.addTextChangedListener(MyCustomTextWatcher(this))
        etGender.addTextChangedListener(MyCustomTextWatcher(this))

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnIntegrate -> {
                checkValidation()
            }
        }
    }

    fun checkValidation() {
        if (etFirstName.text.isNullOrEmpty()) {
            etFirstName.setError(getString(R.string.error))
            etFirstName.requestFocus()
        } else if (etLastName.text.isNullOrEmpty()) {
            etLastName.setError(getString(R.string.error))
            etLastName.requestFocus()
        } else if (etGender.text.isNullOrEmpty()) {
            etGender.setError(getString(R.string.error))
            etGender.requestFocus()
        } else if (etEmail.text.isNullOrEmpty()) {
            etEmail.setError(getString(R.string.error))
            etEmail.requestFocus()
        } else if (etPhone.text.isNullOrEmpty()) {
            etPhone.setError(getString(R.string.error))
            etPhone.requestFocus()
        } else if (etODAccountNo.text.isNullOrEmpty()) {
            etODAccountNo.setError(getString(R.string.error))
            etODAccountNo.requestFocus()
        } else if (etSavingAccountNo.text.isNullOrEmpty()) {
            etSavingAccountNo.setError(getString(R.string.error))
            etSavingAccountNo.requestFocus()
        } else if (etSavingAccountName.text.isNullOrEmpty()) {
            etSavingAccountName.setError(getString(R.string.error))
            etSavingAccountName.requestFocus()
        } else if (etNameOnSavingAccount.text.isNullOrEmpty()) {
            etNameOnSavingAccount.setError(getString(R.string.error))
            etNameOnSavingAccount.requestFocus()
        } else {
            val firstName = etFirstName.text.toString()
            val lastName = etLastName.text.toString()
            val middleName = etMiddleName.text.toString()
            val phone = etPhone.text.toString()
            val email = etEmail.text.toString()
            val savingAccountNo = etSavingAccountNo.text.toString()
            val savingAccountName = etSavingAccountName.text.toString()
            val odAccountNo = etODAccountNo.text.toString()
            val nameOnSavingAcc = etNameOnSavingAccount.text.toString()
            val gender = etGender.text.toString()

            var userProfileBean = IDFCInitProfileModel.Builder.newInstance()
                .setCustPhoneNo(phone)
                .setCustFirstName(firstName)
                .setCustLastName(lastName)
                .setCustMiddleName(middleName)
                .setGender(gender)
                .setCustEmail(email)
                .setMerchantSDKKey(idfcMerchantKey)
                .setCustODAccount(odAccountNo)
                .setSavingAccountName(savingAccountName)
                .setSavingAccountNo(savingAccountNo)
                .setCustNameLinkedSavingAcc(nameOnSavingAcc)
                .setFCMToken("c8kF88jMaQ0:APA91bG1ay0V1GukCxRAqeS4Tr4gzcizIcweNjajGQAgqlcea2iGfL3B2C1fo_QjTBZnM8-mv3CO2tcC3RhjHcT_Zvo0iEDUNRYMPU3j7aIHj0pQHKT5q-NbOtVO51zAkRjM9JwaRkuC1")
                .setLatitude(4533.44)
                .setLongitude(1234.4)
                .setAccuracy(100.0)
                .build();

            /* EPLLogger.INSTANCE.e(EPLSDKConfig.EPL_BASE_URL)
             EPLLogger.INSTANCE.e(EPLSDKConfig.EPL_IDFC_MERCHANT_ID)
             EPLLogger.INSTANCE.e(EPLSDKConfig.EPL_SDL_VERSION)*/
            integrateSDK?.startIntegrate(userProfileBean)
        }
    }

    override fun onTextChange() {
        etFirstName.setError(null)
        etLastName.setError(null)
        etMiddleName.setError(null)
        etPhone.setError(null)
        etEmail.setError(null)
        etODAccountNo.setError(null)
        etSavingAccountName.setError(null)
        etSavingAccountNo.setError(null)
        etNameOnSavingAccount.setError(null)
        etGender.setError(null)
    }

    private fun setDefaultValue() {
        etFirstName.setText("Atishay")
        etLastName.setText("Kumar")
        etMiddleName.setText("")
        etEmail.setText("atishay.kumar@gmail.com")
        etPhone.setText("9821860771")
        etODAccountNo.setText("10001233225")
        etSavingAccountName.setText("Savings Account")
        etSavingAccountNo.setText("10001163710")
        etNameOnSavingAccount.setText("Atishay Kumar")
        etGender.setText("Male")
    }
    /*
    private fun setDefaultValue(){
          etFirstName.setText("Umita")
          etLastName.setText("Rajshri")
          etMiddleName.setText("k")
          etEmail.setText("umita.rajshri@epaylater.in")
          etPhone.setText("7987919639")
          etODAccountNo.setText("10001237117")
          etSavingAccountName.setText("Savings Account")
          etSavingAccountNo.setText("10001237106")
          etNameOnSavingAccount.setText("Umita")
          etGender.setText("Female")

      }*/
    /* private fun setDefaultValue() {
        etFirstName.setText("")
        etLastName.setText("")
        etMiddleName.setText("")
        etEmail.setText("")
        etPhone.setText("")
        etODAccountNo.setText("")
        etSavingAccountName.setText("Savings Account")
        etSavingAccountNo.setText("")
        etNameOnSavingAccount.setText("")
        etGender.setText("Male")
    }*/
    /*private fun setDefaultValue() {
        etFirstName.setText("Gulshan")
        etLastName.setText("Saini")
        etMiddleName.setText("k")
        etEmail.setText("gulshansaini45@gmail.com")
        etPhone.setText("9999359447")
        etODAccountNo.setText("10001237004")
        etSavingAccountName.setText("Savings Account")
        etSavingAccountNo.setText("10001236985")
        etNameOnSavingAccount.setText("Gulshan Saini")
        etGender.setText("Male")

    }
*/
    /* private fun setDefaultValue() {
         etFirstName.setText("Priyanka Dogra")
         etLastName.setText("Dogra")
         etMiddleName.setText("k")
         etEmail.setText("pd@gmail.com")
         etPhone.setText("8264719867")
         etODAccountNo.setText("10001234536")
         etSavingAccountName.setText("Savings Account")
         etSavingAccountNo.setText("10001107347")
         etNameOnSavingAccount.setText("Priyanka")
         etGender.setText("Female")
     }*/

    /*private fun setDefaultValue() {
        etFirstName.setText("Shweta")
        etLastName.setText("Parihar")
        etMiddleName.setText("k")
        etEmail.setText("shweta.parihar@epaylater.in")
        etPhone.setText("9011526079")
        etODAccountNo.setText("10001239078")
        etSavingAccountName.setText("Savings Account")
        etSavingAccountNo.setText("10001232798")
        etNameOnSavingAccount.setText("Shweta Parihar")
        etGender.setText("Female")
    }*/

    /*private fun setDefaultValue() {
        etFirstName.setText("Bharat")
        etLastName.setText("Lalwani")
        etMiddleName.setText("k")
        etEmail.setText("Bharat Lalwani@epaylater.in")
        etPhone.setText("9652033740")
        etODAccountNo.setText("10001239078")
        etSavingAccountName.setText("Savings Account")
        etSavingAccountNo.setText("10001232798")
        etNameOnSavingAccount.setText("Shweta Parihar")
        etGender.setText("Female")
    }*/

   /* private fun setDefaultValue() {
        etFirstName.setText("rv")
        etLastName.setText("balaji")
        etMiddleName.setText("trackd")
        etEmail.setText("balaji.r@epaylater.in")
        etPhone.setText("8667396506")
        etODAccountNo.setText("10001241237")
        etSavingAccountName.setText("Savings Account")
        etSavingAccountNo.setText("10001238519")
        etNameOnSavingAccount.setText("balaji")
        etGender.setText("male")
    }*/
}