package `in`.epaylater.sdksampleapp

import `in`.epaylater.eplupisdksample.interfaces.MyTextChangeListner
import android.text.Editable
import android.text.TextWatcher

class MyCustomTextWatcher(val textChangeListner: MyTextChangeListner) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
    }
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        textChangeListner.onTextChange()
    }
}