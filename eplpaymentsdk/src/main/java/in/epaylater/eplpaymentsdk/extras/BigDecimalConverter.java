package in.epaylater.eplpaymentsdk.extras;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class BigDecimalConverter {
    public static BigDecimal toBigDecimal(Double value) {
        if (value == 0) return BigDecimal.ZERO;
        return new BigDecimal(value);
    }

    public static Double toDouble(BigDecimal value) {
        if (value == null) return BigDecimal.ZERO.doubleValue();
        return value.doubleValue();
    }

    public static BigDecimal toTwoDigitPlace(BigDecimal value) {
        return value.setScale(2, BigDecimal.ROUND_UP);
    }

    public static String numberFormatterUS(BigDecimal value) {
        return String.format("%,.2f", value);
    }
}
