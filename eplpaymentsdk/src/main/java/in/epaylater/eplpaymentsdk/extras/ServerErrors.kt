package `in`.epaylater.eplpaymentsdk.extras

object ServerErrors {
    val unAuthorizedUser: String? = "UnAuthorizedUser"
    val upiInvalidMobileAppVersionException: String? = "UPIInvalidMobileAppVersionException"
    val defaultErrorResponse: String? = "DefaultErrorResponse"
    val userNotSignedUp: String? = "UserNotSignedUp"
    val userNotAuthorized: String? = "UserNotAuthorized"
    val idfcErrorResponse: String? = "IDFCErrorResponse"
    val serverError: String? = "ServerError"
    val upiSimBindingRequiredException: String? = "UPISimBindingRequiredException"
    val upiProfileNotFoundException: String? = "UPIProfileNotFoundException"
    val upiSimUsedIsNotSameException: String? = "UPISimUsedIsNotSameException"
    val upiProfileAlreadyExistException: String? = "UPIProfileAlreadyExistException"
    val upiSimRebindingNotRequiredException: String? = "UPISimRebindingNotRequiredException"
    val upiFailedToCreateUpiProfileException: String? = "UPIFailedToCreateUpiProfileException"
    val upiApprovalIsNotPendingException: String? = "UPIApprovalIsNotPendingException"
    val upiInvalidOtpForTransactionException: String? = "UPIInvalidOtpForTransactionException"
    val upiFailedToApproveUpiTransactionException: String? = "UPIFailedToApproveUpiTransactionException"
    val upiTransactionExpired: String? = "UPITransactionExpired"
    val upiTransactionInProgress: String? = "UPITransactionInProgress"
    val noConnectAvailable: String? = "NoConnectionAvailable"
}