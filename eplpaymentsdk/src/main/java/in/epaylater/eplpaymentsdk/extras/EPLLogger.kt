package `in`.epaylater.eplpaymentsdk.extras

import `in`.epaylater.eplpaymentsdk.BuildConfig
import android.os.Build
import android.util.Log
enum class EPLLogger {
    INSTANCE;
    val TAG :String? = "EPLLOGGER"
    fun e(e: Exception): Unit {
        if (BuildConfig.DEBUG) {
            Log.e(TAG,e.message)
        }
    }

    fun e(e: String): Unit {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, e)
        }
    }

    fun v(e: Exception): Unit {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    fun d(s:String): Unit {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, s)
        }
    }
}