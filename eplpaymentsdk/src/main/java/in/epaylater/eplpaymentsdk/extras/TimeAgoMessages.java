package in.epaylater.eplpaymentsdk.extras;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public final class TimeAgoMessages {

    /**
     * The property path for MESSAGES.
     */
    private static final String MESSAGES = "com.github.marlonlom.utilities.timeago.messages";
    /**
     * The bundle.
     */
    private ResourceBundle bundle;

    /**
     * Instantiates a new time ago messages.
     */
    private TimeAgoMessages() {
        super();
    }

    /**
     * Gets the bundle.
     *
     * @return the bundle
     */
    private ResourceBundle getBundle() {
        return bundle;
    }

    /**
     * Sets the resource bundle.
     *
     * @param bundle the new resource bundle
     */
    private void setBundle(ResourceBundle bundle) {
        this.bundle = bundle;
    }

    /**
     * Gets the property value.
     *
     * @param property the property key
     * @return the property value
     */
    public String getPropertyValue(final String property) {
        final String propertyVal = getBundle().getString(property);
        return propertyVal;
    }

    /**
     * Gets the property value.
     *
     * @param property the property key
     * @param values   the property values
     * @return the property value
     */
    public String getPropertyValue(final String property, Object... values) {
        String propertyVal = getPropertyValue(property);
        return MessageFormat.format(propertyVal, values);
    }

    /**
     * The Inner Class Builder for <i>TimeAgoMessages</i>.
     *
     * @author marlonlom
     */
    public static final class Builder {

        /**
         * The inner bundle.
         */
        private ResourceBundle innerBundle;

        /**
         * Builds the TimeAgoMessages instance.
         *
         * @return the time ago messages instance.
         */
        public TimeAgoMessages build() {
            TimeAgoMessages resources = new TimeAgoMessages();
            resources.setBundle(this.getInnerBundle());
            return resources;
        }

        /**
         * Build messages with the default locale.
         *
         * @return the builder
         */
        public Builder defaultLocale() {
            this.setInnerBundle(ResourceBundle.getBundle(TimeAgoMessages.MESSAGES));
            return this;
        }

        /**
         * Gets the inner bundle.
         *
         * @return the inner bundle
         */
        public ResourceBundle getInnerBundle() {
            return innerBundle;
        }

        /**
         * Sets the inner bundle.
         *
         * @param bundle the new inner bundle
         */
        public void setInnerBundle(ResourceBundle bundle) {
            this.innerBundle = bundle;
        }

        /**
         * Build messages with the selected locale.
         *
         * @param locale the locale
         * @return the builder
         */
        public Builder withLocale(Locale locale) {
            this.setInnerBundle(ResourceBundle.getBundle(TimeAgoMessages.MESSAGES, locale));
            return this;
        }
    }
}
