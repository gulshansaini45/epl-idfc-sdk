package `in`.epaylater.eplpaymentsdk.extras

object EPLSDKConfig {

    val EPL_LOGIN = "/api/v1/merchant/customer/login"
    val EPL_DEVICE = "/api/v1/user/device"
    val EPL_USER_PROFILE = "/api/v2/user/profile"
    val EPL_CUSTOMER_PROFILE = "/api/v3/upi/customer/profile"
    val EPL_ENCRYPT_SMS = "/api/v3/upi/idfc/encrypted-sms/"

    val isDebugBuild = false //BuildConfig.DEBUG

    val EPL_BASE_URL = if (isDebugBuild) "https://api-sandbox-2.epaylater.in:443/" else "https://api1.epaylater.in:443/"
    val EPL_IDFC_MERCHANT_ID =
        if (isDebugBuild) "4228714c-8eea-4d3a-882a-3200046f204e" else "c3e25c9e-996b-11e9-a2a3-2a2ae2dbcce4"
    val EPL_SDK_VERSION = if (isDebugBuild) "sdk-1.0.1" else "idfc-sdk-1.0.1"
    val EPL_SSL_CERT =
        if (isDebugBuild) "sha256/YLh1dUR9y6Kja30RrAn7JKnbQG/uEtLMkBgFF2Fuihg=" else "sha256/E0FzGXQNEQFZaNJaFabMvhLdgZzNsE/FYNB1SqGGcBY="
    val EPL_SSL_URL = if (isDebugBuild) "api-sandbox-2.epaylater.in:443/" else "api1.epaylater.in:443/"
    val EPL_MERCHANT_URL =
        if (isDebugBuild) "https://d28ntcjzg26o5u.cloudfront.net/" else "https://d1c1c3vi74jbap.cloudfront.net/"
    /*for api-sandbox.epaylater.in
    Pin SHA256: G5rkS9Ji9EALxM2VNUf6ojEEHOAMOD1cuusTZ4BHDkA=*/
}