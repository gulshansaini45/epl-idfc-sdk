package `in`.epaylater.eplpaymentsdk.extras

import `in`.epaylater.eplpaymentsdk.models.FAQSModel

object FAQSData {


    fun getTopQuestions(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("How can I transact using my IDFC FIRST Postpaid account")
        arrayList.add("Where can I use IDFC FIRST Postpaid")
        arrayList.add("Where can I see my statement")

        return arrayList
    }

    private fun getTopQuestionsAnswer(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add(
            "It's really easy to use your postpaid limit. It can be spent at any merchant accepting UPI payments. \n" +
                    "For Online Merchants: Choose UPI payment option and enter UPI generated while setting up your postpaid account. i.e. <mobile number>.pp@idfcfirst and click on the Pay button. In the next step enter the OTP that will be sent to your registered mobile number. \n" +
                    "\n" +
                    "For Offline Merchants: Launch you IDFC FIRST Bank mobile banking app and click on Scan & Pay. Scan the UPI QR code at the merchant and authorize the payment using OTP sent on your registered number."
        )
        arrayList.add("IDFC FIRST Postpaid can be used at any merchant accepting UPI payments. You can use the UPI ID on - 1) Online e-commerce platforms like shopping, travel, movie tickets, cab booking, ordering food etc 2) Any physical store that accepts UPI QR code based payments")
        arrayList.add("We will send you a SMS on your registered mobile number and a PDF copy of your monthly statement to your registered email address. You can also view all your ransactions in the Home section of the Postpaid section.")
        return arrayList;
    }
    fun getTopQuestionData(): ArrayList<FAQSModel> {
        var arrayList = ArrayList<FAQSModel>()
        var topQuestion = getTopQuestions()
        var topQuestionAnswer = getTopQuestionsAnswer()
        for (i in 0..topQuestion.size - 1) {
            var faqModel = FAQSModel()
            faqModel.title = topQuestion.get(i)
            faqModel.answer = topQuestionAnswer.get(i)
            arrayList.add(faqModel)
        }
        return arrayList
    }

    fun getCategoryListData(): ArrayList<FAQSModel> {
        var arrayList = ArrayList<FAQSModel>()
        var firstCategoryQuestion = getCategoryFirstQuestion()
        var firstCategoryAnswer = getCategoryFirstAnswer()
        for (i in 0..firstCategoryQuestion.size - 1) {
            var faqModel = FAQSModel()
            faqModel.title = firstCategoryQuestion.get(i)
            faqModel.answer = firstCategoryAnswer.get(i)
            arrayList.add(faqModel)
        }
        return arrayList
    }

    fun getCategoryList(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("About IDFC FIRST Postpaid")
        arrayList.add("Using IDFC FIRST Postpaid")
        arrayList.add("Billing, payment and charges")
        arrayList.add("Managing your account")

        return arrayList
    }
    fun getFirstCategoryData(): ArrayList<FAQSModel> {
        var arrayList = ArrayList<FAQSModel>()
        var firstCategoryQuestion = getCategoryFirstQuestion()
        var firstCategoryAnswer = getCategoryFirstAnswer()
        for (i in 0..firstCategoryQuestion.size - 1) {
            var faqModel = FAQSModel()
            faqModel.title = firstCategoryQuestion.get(i)
            faqModel.answer = firstCategoryAnswer.get(i)
            arrayList.add(faqModel)
        }
        return arrayList
    }

    fun getSecondCategoryData(): ArrayList<FAQSModel> {
        var arrayList = ArrayList<FAQSModel>()
        var SecondCategoryQuestion = getCategorySecondQuestion()
        var SecondCategoryAnswer = getCategorySecondAnswer()
        for (i in 0..SecondCategoryQuestion.size - 1) {
            var faqModel = FAQSModel()
            faqModel.title = SecondCategoryQuestion.get(i)
            faqModel.answer = SecondCategoryAnswer.get(i)
            arrayList.add(faqModel)
        }
        return arrayList
    }

    fun getThirdCategoryData(): ArrayList<FAQSModel> {
        var arrayList = ArrayList<FAQSModel>()
        var ThirdCategoryQuestion = getCategoryThirdQuestion()
        var ThirdCategoryAnswer = getCategoryThirdAnswer()
        for (i in 0..ThirdCategoryQuestion.size - 1) {
            var faqModel = FAQSModel()
            faqModel.title = ThirdCategoryQuestion.get(i)
            faqModel.answer = ThirdCategoryAnswer.get(i)
            arrayList.add(faqModel)
        }
        return arrayList
    }

    fun getFourthCategoryData(): ArrayList<FAQSModel> {
        var arrayList = ArrayList<FAQSModel>()
        var fourthCategoryQuestion = getCategoryFourthQuestion()
        var fourthCategoryAnswer = getCategoryFourthAnswer()
        for (i in 0..fourthCategoryQuestion.size - 1) {
            var faqModel = FAQSModel()
            faqModel.title = fourthCategoryQuestion.get(i)
            faqModel.answer = fourthCategoryAnswer.get(i)
            arrayList.add(faqModel)
        }
        return arrayList
    }


    private fun getCategoryFirstQuestion(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("What is IDFC FIRST Postpaid")
        arrayList.add("How do I activate my IDFC FIRST Postpaid account")
        arrayList.add("Do I need to provide any documents to use the postpaid account")
        arrayList.add("Where can I use IDFC FIRST Postpaid")
        arrayList.add("How do I learn more about the product")
        arrayList.add("My query is not listed here")
        return arrayList;
    }

    private fun getCategoryFirstAnswer(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("IDFC FIRST Postpaid packs together the benefits of ‘buy now’ & ‘pay later’ with the convenience of digital payments. A pre-approved limit is assigned to you along with a UPI ID to use while transacting. You can make purchases without worrying to pay for 15 days and when it is time to pay the bill, you can do it easily from your mobile phone.")
        arrayList.add("Your IDFC FIRST Postpaid account is activated once you Accept the Terms & Conditions. Then a UPI ID is created and your account is ready to use instantly.")
        arrayList.add("No, you don't have to submit any additional documents for using IDFC FIRST Postpaid")
        arrayList.add("IDFC FIRST Postpaid can be used at any merchant accepting UPI payments. You can use the UPI ID on - 1) Online e-commerce platforms like shopping, travel, movie tickets, cab booking, ordering food etc 2) Any physical store that accepts UPI QR code based payments")
        arrayList.add("Refer to other sections in the FAQ to understand how to use the product")
        arrayList.add("Kindly reach out to us at banker@idfcfirstbank.com or call us at 1800 419 4332.")
        return arrayList;
    }

    private fun getCategorySecondQuestion(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("How can I transact using my IDFC FIRST Postpaid account ")
        arrayList.add("What is my UPI ID")
        arrayList.add("Where can I not use IDFC FIRST Postpaid")
        arrayList.add("Can I transfer my IDFC FIRST Postpaid balance to a bank account")
        arrayList.add("Can I do multiple transactions using my IDFC FIRST Postpaid account")
        arrayList.add("How can I check my available limit")
        arrayList.add("What happens once I exhaust the limit allocated to me within 15 days")
        arrayList.add("My transaction has failed despite money being debited from my postpaid account ")
        arrayList.add("How do I get a refund if I cancel the order placed on the merchant site (e.g. Amazon)")
        arrayList.add("I want to raise a transaction dispute e.g. transaction was not done by me or merchant refused transaction")
        arrayList.add("Can I use this UPI ID to accept fund from others")
        arrayList.add("What are the transaction limits on my Postpaid account")
        return arrayList;
    }

    private fun getCategorySecondAnswer(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add(
            "It's really easy to use your postpaid limit. It can be spent at any merchant accepting UPI payments. \n" +
                    "For Online Merchants: Choose UPI payment option and enter UPI generated while setting up your postpaid account. i.e. <mobile number>.pp@idfcfirst and click on the Pay button. In the next step enter the OTP that will be sent to your registered mobile number. \n" +
                    "\n" +
                    "For Offline Merchants: Launch you IDFC FIRST Bank mobile banking app and click on Scan & Pay. Scan the UPI QR code at the merchant and authorize the payment using OTP sent on your registered number."
        )
        arrayList.add("Your UPI ID will be <mobile number>.pp@idfcfirst. You will find the same on the Postpaid home page.")
        arrayList.add("IDFC FIRST Postpaid can currently be used  at few types of merchants. For instance, online ecommerce site for shopping, travel, movie ticket booking, cab booking food ordering etc. and at certain stores where you can scan & make payments. Credit card payments, wallet recharge and few types of in store payments etc. are currently not permitted.")
        arrayList.add("IDFC FIRST Postpaid can only be used for merchant payments. Transferring this balance to a bank account is not possible.")
        arrayList.add("Yes, you can continue to transact with your postpaid account as long as you have the balance limit available and provided the earlier bills are paid off.")
        arrayList.add("Details of available limit can be viewed under the Postpaid menu in the IDFC FIRST mobile banking app.")
        arrayList.add(
            "You will not be able to do further transactions unless you make a payment to the postpaid account. Example: Say your limit is Rs. 5000 and you have exhausted it. If you need to use the limit you can make a payment to your UPI account <mobile number>.pp@idfcfirst. Your limit will be reset with the same amount which you paid e.g. you made a payment of Rs. 3000, your available limit will be come Rs. 3000."
        )
        arrayList.add(
            "In such a situation the payment will be refunded back to your postpaid account within 3 working days. In a rare situation if this does not happen please feel free to reach out to us at banker@idfcfirstbank.com or call us at 1800 419 4332. You can also use your mobile banking app and internet banking page to initiate a service request.\n" +
                    "\n" +
                    "If your due date is approaching and you are unsure about a refund amout, it is recommended to pay the amount. The refund will be credited to your account subsequently and it will be adjusted in your next billing cycle."
        )

        arrayList.add(
            "In such a situation the payment will be refunded back to your postpaid account within 3 working days. In a rare situation if this does not happen please feel free to reach out to us at banker@idfcfirstbank.com or call us at 1800 419 4332. You can also use your mobile banking app and internet banking page to initiate a service request.\n" +
                    "\n" +
                    "If your due date is approaching and you are unsure about a refund amout, it is recommended to pay the amount. The refund will be credited to your account subsequently and it will be adjusted in your next billing cycle."
        )
        arrayList.add("Kindly reach out to us at banker@idfcfirstbank.com or call us at 1800 419 4332 with the details of such a transaction and we shall resolve it for you")
        arrayList.add("Yes, if you or someone else transfers funds to your postpaid UPI ID, these funds will be considered as part of your repayment and will be adjusted against your available limit.")
        arrayList.add("You can make merchant payments up to the available limits on your IDFC FIRST Postpaid. For your security there is limit of Rs 5,000 for the first 24 hrs of onboarding and post that a per transaction limit of Rs 25,000.")


        return arrayList;
    }


    private fun getCategoryThirdQuestion(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("What is a postpaid statement")
        arrayList.add("Where can I see my statement")
        arrayList.add("Where can I see my transactions")
        arrayList.add("When do I need to make payments")
        arrayList.add("How do I make payments for the bill")
        arrayList.add("Can I make a partial bill payment")
        arrayList.add("What happens if I don’t pay the bill")
        arrayList.add("What are the charges of using postpaid")
        arrayList.add("Are there any hidden charges")

        return arrayList;
    }

    private fun getCategoryThirdAnswer(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("Your Postpaid facility has a 15 days transactions period, so your statement will be presented on the 16th day and will be the sum of all the transactions and payments you have made during that period.")
        arrayList.add("We will send you a SMS on your registered mobile number and a PDF copy of your monthly statement to your registered email address. You can also view all your transactions in the Home section of the Postpaid section.")
        arrayList.add("Details of your transactions are available on the home screen of your postpaid account")
        arrayList.add("IDFC FIRST Postpaid facility gives you a grace period of 5 days from the bill generation date to pay the bill amount")
        arrayList.add("You can setup standing instructions from your savings account to facilitate bill payment. The amount will be auto-debited on the due date if you have enough balance in your account. You also have the option to make payments using net banking, debit card or other UPI handles.")
        arrayList.add("You can only make a full payment using the “Pay Now” option on the IDFC FIRST Postpaid dashboard. The bill will be considered paid only if you clear the entire amount.")
        arrayList.add("If you miss paying the bill by the due date, your account will be temporarily put on freeze till you repay the entire amount. Please note this is applicable only after your bill in generated. If you have only exhausted your limit within a 15 day cycle then you can continue using your account by making a payment. Your limit will be refreshed by the same amount.")
        arrayList.add("There are no fees or processing charges for using the postpaid account for your purchases. At this point we charge 3% interest per month on the outstanding amount and a late fees of Rs. 50. The outstanding amount is the balance amount for which the bill has not been paid by on the due date. See following table to understand the calculations..TABLE TO BE ATTACHED>")
        arrayList.add("There are no other charges apart from the interest amount and the late fee as mentioned above.")

        return arrayList;
    }

    private fun getCategoryFourthQuestion(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("My account is blocked and I am not able to make transactions")
        arrayList.add("How is my IDFC FIRST Postpaid limit calculated")
        arrayList.add("Can I request for increasing my limit")
        arrayList.add("Why was my Postpaid account limit reduced")
        arrayList.add("Can I manually control the limit on my postpaid account")
        arrayList.add("What should I do if my UPI ID creation has failed")
        arrayList.add("I want to change the UPI handle")
        arrayList.add("I want to change phone number")
        arrayList.add("Will my UPI ID change if I change my phone number")
        arrayList.add("What if I change my mobile device")
        arrayList.add("What if I change my phone number on the same device")
        arrayList.add("I am not able to find postpaid account on my internet banking website")
        arrayList.add("Do I get any reward points / loyalty points for using post paid account")
        arrayList.add("I want to stop using my postpaid limit")
        arrayList.add("How can I close / deactivate my postpaid account")

        return arrayList;
    }

    private fun getCategoryFourthAnswer(): List<String> {
        var arrayList = ArrayList<String>()
        arrayList.add("Your IDFC FIRST Postpaid account is frozen if you fail to repay the bill by the due date. You will be able to use the account as soon as the full payment is made.")
        arrayList.add("IDFC FIRST Postpaid limit is allocated based on your financial relationship and transactions history")
        arrayList.add("We periodically review limits allotted to all our customers. As you keep transacting and paying the bill on your postpaid account on time, we keep trying to provide you the best limit to meet your needs.")
        arrayList.add("We periodically review limits allotted to all our customers based on transaction and payment history. We hope you keep transacting and pay the bill on your postpaid account on time, while we keep working to provide you the best limit to meet your needs.")
        arrayList.add("Currently, there is no provision to manually control the limit of your postpaid account")
        arrayList.add("The next time you login you will receive a prompt to setup your UPI ID. Alternatively, you can access account setup from the menu on the mobile app.")
        arrayList.add("Your UPI handle is created during sign-up and cannot be changed at this point.")
        arrayList.add(
            "You can change your phone number using the 'Service Request' menu on the mobile app. However, please note that a change in phone number will temporarily de-activate your Postpaid account. \n" +
                    "If you have already changed your phone number kindly reach out to us at banker@idfcfirstbank.com or call us at 1800 419 4332."
        )
        arrayList.add(
            "If you change your phone number, you will not be able to use your Postpaid account. \n" +
                    "If you have already changed your phone number kindly reach out to us at banker@idfcfirstbank.com or call us at 1800 419 4332."
        )
        arrayList.add("If device is changed or SIM is changed then you will need to follow steps on the mobile app to re-link the UPI account to your new device or SIM")
        arrayList.add(
            "If you change your phone number, you will not be able to use your Postpaid account. \n" +
                    "If you have already changed your phone number kindly reach out to us at banker@idfcfirstbank.com or call us at 1800 419 4332."
        )
        arrayList.add("Currently the postpaid facility is available only on the IDFC FIRST Bank mobile app.")
        arrayList.add("Currently there is not loyalty program associated with the postpaid account, however we will bring your exciting offers from time to time as you keep using the account")
        arrayList.add("You can simply choose to not utilize the limit. However it is suggested you clear your bills to avoid incurring excess charges.")
        arrayList.add("You can simply choose to not utilize the limit. However if you want to close the account request you to walk into a branch and place the request.")
        return arrayList;
    }
}