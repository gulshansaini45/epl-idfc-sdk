package in.epaylater.eplpaymentsdk.extras;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import in.epaylater.eplpaymentsdk.R;

import java.text.NumberFormat;

/**
 * Created by ds on 22/11/15.
 */
public class CustomProgressDialog extends AlertDialog {

    public static final int STYLE_SPINNER = 0;

    public CustomProgressDialog(Context context) {
        super(context, R.style.CenterDialog);
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    public static CustomProgressDialog show(Context context) {
        return show(context, "", "", false);
    }

    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message) {
        return show(context, title, message, false);
    }

    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message, boolean indeterminate) {
        return show(context, title, message, indeterminate, false, null);
    }

    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message, boolean indeterminate, boolean cancelable) {
        return show(context, title, message, indeterminate, cancelable, null);
    }

    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message, boolean indeterminate,
                                            boolean cancelable, OnCancelListener cancelListener) {
        if (context == null || ((context instanceof Activity) && ((Activity) context).isFinishing()))
            return null;

        try {
            CustomProgressDialog dialog = new CustomProgressDialog(context);
            dialog.setTitle(null);
            dialog.setMessage(null);
            dialog.setCancelable(cancelable);
            dialog.setOnCancelListener(cancelListener);
            dialog.show();
            return dialog;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.layout_custom_progress_dialog, null);
        setView(view);
        super.onCreate(savedInstanceState);
    }

    public static void dismiss(CustomProgressDialog dialog) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

