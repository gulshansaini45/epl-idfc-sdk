package in.epaylater.eplpaymentsdk.extras;

public enum SwipeDirection {
    all, left, right, none ;
}
