package `in`.epaylater.eplpaymentsdk.managers

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.di.components.DaggerEPLAppComponent
import `in`.epaylater.eplpaymentsdk.di.components.EPLAppComponent
import `in`.epaylater.eplpaymentsdk.di.modules.EPLAppModule
import `in`.epaylater.eplpaymentsdk.di.modules.NetworkModule
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.fragments.EPLDashboardFragment
import `in`.epaylater.eplpaymentsdk.models.IDFCInitProfileModel
import `in`.epaylater.eplpaymentsdk.models.MerchantCredModel
import `in`.epaylater.eplpaymentsdk.utils.EPLCommonUtil
import `in`.epaylater.eplpaymentsdk.utils.RootUtil
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.widget.Toast
import com.scottyab.rootbeer.BuildConfig
import com.scottyab.rootbeer.RootBeer


object EPLManager {

    lateinit var eplAppComponent: EPLAppComponent
    fun startIntegration(
        application: Application,
        idfcInitProfileModel: IDFCInitProfileModel,
        fragmentManager: FragmentManager,
        containerId: Int
    ) {

        try {
            val rootBeer = RootBeer(application.applicationContext)
            if (RootUtil.isDeviceRooted() || (rootBeer.isRootedWithoutBusyBoxCheck && !BuildConfig.DEBUG) ) {
                Toast.makeText(application.applicationContext, "You are on a rooted device!", Toast.LENGTH_LONG).show()
                return
            }
            initAppComponent(
                application.applicationContext,
                idfcInitProfileModel.merchantSdkKey,
                EPLCommonUtil.INSTANCE.fetchSHACert(application.applicationContext)
            )
            var bundle = Bundle()
            var length = idfcInitProfileModel.custPhoneNumber.length - 10
            var phoneNo =
                idfcInitProfileModel.custPhoneNumber.substring(length, idfcInitProfileModel.custPhoneNumber.length)
            idfcInitProfileModel.custPhoneNumber = phoneNo
            bundle.putParcelable(EPLConstants.KEYS.USER_DATA, idfcInitProfileModel)
            val fragment = EPLDashboardFragment()
            fragment.setArguments(bundle)
            fragmentManager.beginTransaction().replace(containerId, fragment)
                .addToBackStack(EPLConstants.SCREEN_NAME.EPL_DASHBOARD).commit()
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e.message + " exception Appcomponent")
        }
    }

    fun initAppComponent(context: Context, merchantSDK: String, shaCert: String) {
        val model =
            MerchantCredModel(merchantSDK, shaCert)
        EPLManager.eplAppComponent = DaggerEPLAppComponent.builder()
            .ePLAppModule(EPLAppModule(context.applicationContext, model))
            .networkModule(NetworkModule()).build()
        EPLManager.eplAppComponent.inject(context.applicationContext)
    }
}