package `in`.epaylater.eplpaymentsdk.entities

import `in`.epaylater.eplpaymentsdk.models.responses.UPIProfileResponse

data class EplUPIProfile(
    val imei: String,
    val simSerial: String,
    val simSlotNo: Int,
    val mobile: String,
    val vpa: String,
    val disabled: Boolean,
    val simRebindingRequired: Boolean,
    val pendingApprovalCount: Int
) {

    var id: Long = 0

    fun isVPAExist(): Boolean {
        return vpa.isNotBlank()
    }

    fun canTransactwithUPI(): Boolean {
        return isVPAExist() && !disabled && !simRebindingRequired
    }

    companion object {
        fun mapEplUPIProfileFromUPIProfileResponse(it: UPIProfileResponse): EplUPIProfile {
            return EplUPIProfile(
                it.imei,
                it.simSerial,
                it.simSlotNo,
                it.mobile,
                it.vpa,
                it.disabled,
                it.simRebindingRequired,
                it.pendingApprovalCount
            )
        }
    }
}