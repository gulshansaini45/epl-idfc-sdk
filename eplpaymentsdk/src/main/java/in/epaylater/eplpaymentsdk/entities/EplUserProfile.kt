package `in`.epaylater.eplpaymentsdk.entities

import `in`.epaylater.eplpaymentsdk.models.responses.UserProfileResponse
import java.math.BigDecimal

data class EplUserProfile(
    val profileComplete: Boolean,
    val mobile: String,
    val email: String,
    val iciciCustomer: Boolean,
    val deviceEnabled: Boolean,
    val uuID: String?,
    val nachRegistered: Boolean,
    val mSetPin: Boolean,
    val unmaskedMobile: String?,
    val unmaskedEmail: String?,
    val features: Features,
    val userProperty: UserProperty

) {

    var id: Long = 0

    data class UserProperty(
        val upi: Int,
        val blacklisted: Int,
        val dmi: Int
    )

    data class Features(
        val upi: UPI,
        val loyalty: Loyalty,
        val referral: Referral,
        val dmi: DMI,
        val totp: TOTP,
        val creditEnhnc: CreditEnhnc
    ) {
        data class UPI(
            val upiEnabled: Boolean,
            val scanAndPayEnabled: Boolean,
            val dmiEnabled: Boolean,
            val mPinEnabled: Boolean
        )

        data class Loyalty(
            val enabled: Boolean,
            val totalPoints: BigDecimal?,
            val redeemablePoints: BigDecimal?,
            val repaymentEnabled: Boolean
        )

        data class Referral(
            val enabled: Boolean,
            val refUrl: String?
        )

        data class DMI(
            val ekycEnable: Boolean,
            val enachEnabled: Boolean,
            val ekycCompleted: Boolean,
            val enachCompleted: Boolean
        ) {
            fun isEkycRequired() = ekycEnable && !ekycCompleted

            fun isEnachRequired() = enachEnabled && !enachCompleted
        }

        data class TOTP(
            val isTotpEnabled: Boolean,
            val isTotpExpired: Boolean
        )

        data class CreditEnhnc(
            val isCreditEnhnc: Boolean
        )
    }

    companion object {
        fun mapFromDTO(userProfile: UserProfileResponse): EplUserProfile {
            return EplUserProfile(
                userProfile.profileComplete,
                userProfile.mobile,
                userProfile.email,
                userProfile.iciciCustomer,
                userProfile.deviceEnabled,
                userProfile.uuID,
                userProfile.nachRegistered,
                userProfile.mpinSet,
                userProfile.unmaskedMobile,
                userProfile.unmaskedEmail,
                Features(
                    Features.UPI(
                        userProfile.features.upi.upiEnabled,
                        userProfile.features.upi.scanAndPayEnabled,
                        userProfile.features.upi.dmiEnabled,
                        userProfile.features.upi.mPinEnabled
                    ),
                    Features.Loyalty(
                        userProfile.features.loyalty.enabled,
                        userProfile.features.loyalty.totalPoints,
                        userProfile.features.loyalty.redeemablePoints,
                        userProfile.features.loyalty.repaymentEnabled
                    ),
                    userProfile.features.referral.let {
                        Features.Referral(
                            it.enabled,
                            it.refUrl
                        )
                    },
                    userProfile.features.dmi.let {
                        Features.DMI(
                            it.ekycEnable,
                            it.enachEnabled,
                            it.ekycCompleted,
                            it.enachCompleted
                        )
                    },
                    Features.TOTP(
                        userProfile.features.totp.isTotpEnabled,
                        userProfile.features.totp.isTotpExpired
                    ),
                    Features.CreditEnhnc(
                        userProfile.features.creditEnhnc.isCreditEnchEnabled
                    )
                ),
                UserProperty(
                    userProfile.userProperties.upi,
                    userProfile.userProperties.blacklisted,
                    userProfile.userProperties.dmi
                )
            )
        }
    }
}
