package `in`.epaylater.eplpaymentsdk.di.modules

import `in`.epaylater.eplpaymentsdk.di.scope.PresenterScope
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.interfaces.EpayMerchantService
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import `in`.epaylater.eplpaymentsdk.repositories.*
import dagger.Module
import dagger.Provides

@Module
class PresenterModule {

    private var basePresenter: BasePresenter

    constructor(basePresenter: BasePresenter) {
        this.basePresenter = basePresenter
    }

    @Provides
    fun provideBasePresenter(): BasePresenter {
        return basePresenter
    }


    @Provides
    @PresenterScope
    fun provideDashboardRepo(
        epayLaterService: EpayLaterService,
        basePresenter: BasePresenter
    ): DashboardRepository {
        return DashboardRepository(
            epayLaterService = epayLaterService,
            presenter = basePresenter
        )
    }

    @Provides
    @PresenterScope
    fun provideSendMoneyRepo(
        epayLaterService: EpayLaterService,
        basePresenter: BasePresenter
    ): SendMoneyRepository {
        return SendMoneyRepository(
            epayLaterService = epayLaterService,
            presenter = basePresenter
        )
    }


    @Provides
    @PresenterScope
    fun provideConfirmPaymentRepo(
        epayLaterService: EpayLaterService,
        basePresenter: BasePresenter
    ): ConfirmPaymentRepository {
        return ConfirmPaymentRepository(
            epayLaterService = epayLaterService,
            presenter = basePresenter
        )
    }

    @Provides
    @PresenterScope
    fun provideHomeRepo(
        epayLaterService: EpayLaterService,
        basePresenter: BasePresenter
    ): HomeRepository {
        return HomeRepository(
            epayLaterService = epayLaterService,
            presenter = basePresenter
        )
    }

    @Provides
    @PresenterScope
    fun provideTransactionRepo(
        epayLaterService: EpayLaterService,
        basePresenter: BasePresenter
    ): TxnHistoryRepository {
        return TxnHistoryRepository(
            epayLaterService = epayLaterService,
            presenter = basePresenter
        )
    }

    @Provides
    @PresenterScope
    fun providePayNowRepo(
        epayLaterService: EpayLaterService,
        basePresenter: BasePresenter
    ): PayNowRepository {
        return PayNowRepository(
            epayLaterService = epayLaterService,
            presenter = basePresenter
        )
    }

    @Provides
    @PresenterScope
    fun providePendingCollectRepo(
        epayLaterService: EpayLaterService,
        basePresenter: BasePresenter
    ): PendingCollectRepository {
        return PendingCollectRepository(
            epayLaterService = epayLaterService,
            presenter = basePresenter
        )
    }

    @Provides
    @PresenterScope
    fun provideWhereToUseRepo(
        epayLaterMerchantService: EpayMerchantService,
        basePresenter: BasePresenter
    ): WhereToUseRepository {
        return WhereToUseRepository(
            epayLaterMerchantService = epayLaterMerchantService,
            presenter = basePresenter
        )
    }
}