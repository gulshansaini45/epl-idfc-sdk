package `in`.epaylater.eplpaymentsdk.di.components

import `in`.epaylater.eplpaymentsdk.di.modules.EPLAppModule
import `in`.epaylater.eplpaymentsdk.di.modules.NetworkModule
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.fragments.EPLBaseFragment
import android.app.Application
import android.content.Context
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(EPLAppModule::class, NetworkModule::class))
interface EPLAppComponent {
    fun inject(context: Context)
    fun inject(eplBaseFragment: EPLBaseFragment)
    fun presenterComponent(presenterModule: PresenterModule): PresenterComponent
}