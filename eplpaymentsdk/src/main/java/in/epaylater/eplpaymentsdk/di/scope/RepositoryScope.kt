package `in`.epaylater.eplpaymentsdk.di.scope

import javax.inject.Scope


@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class RepositoryScope