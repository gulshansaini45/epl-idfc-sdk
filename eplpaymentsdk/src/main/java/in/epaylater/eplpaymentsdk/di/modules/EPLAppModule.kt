package `in`.epaylater.eplpaymentsdk.di.modules

import `in`.epaylater.eplpaymentsdk.models.MerchantCredModel
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [NetworkModule::class])
class EPLAppModule {

    private var context: Context
    private var merchantCredModel: MerchantCredModel
    private lateinit var basePresenter: BasePresenter

    constructor(context: Context, merchantCredModel: MerchantCredModel) {
        this.context = context
        this.merchantCredModel = merchantCredModel
    }

    @Provides
    fun providesContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideMerchantCred(): MerchantCredModel {
        return merchantCredModel
    }

    @Provides
    @Singleton
    fun provideSharedPreference(): SharedPreferences {
        return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
    }


    companion object {
        const val PREF_FILE_NAME = "epl_sdk_pref_file"
    }
}