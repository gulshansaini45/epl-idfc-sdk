package `in`.epaylater.eplpaymentsdk.di.components

import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.di.scope.PresenterScope
import `in`.epaylater.eplpaymentsdk.presenters.*
import dagger.Subcomponent

@PresenterScope
@Subcomponent(modules = arrayOf(PresenterModule::class))
interface PresenterComponent {
    fun inject(presenter: DashboardPresenter)
    fun inject(presenter: SendMoneyPresenter)
    fun inject(presenter: ConfirmPaymentPresenter)
    fun inject(presenter: HomePresenter)
    fun inject(presenter: TxnPresenter)
    fun inject(presenter: EPLPayNowPresenter)
    fun inject(presenter: PendingCollectPresenter)
    fun inject(presenter: WhereToUsePresenter)
}