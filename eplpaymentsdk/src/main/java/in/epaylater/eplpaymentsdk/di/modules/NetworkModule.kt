package `in`.epaylater.eplpaymentsdk.di.modules

import `in`.epaylater.eplpaymentsdk.BuildConfig
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.extras.EPLSDKConfig
import `in`.epaylater.eplpaymentsdk.interceptor.HeaderInterceptor
import `in`.epaylater.eplpaymentsdk.interceptor.LiveNetworkInterceptor
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.interfaces.EpayMerchantService
import `in`.epaylater.eplpaymentsdk.models.MerchantCredModel
import android.content.Context
import android.os.Build
import android.util.Log
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
//import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.lang.reflect.Type
import java.security.KeyStore
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

@Module
public class NetworkModule {
    private val CACHE_SIZE = 10 * 1024 * 1024

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): EpayLaterService {
        return Retrofit.Builder()
            .baseUrl(EPLSDKConfig.EPL_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(nullOnEmptyConverterFactory)
            .addConverterFactory(GsonConverterFactory.create(gson))
            /*.addCallAdapterFactory(RxJava2CallAdapterFactory.create())*/
            .build()
            .create(EpayLaterService::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitMerchant(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        cookieJar: PersistentCookieJar,
        certPinner: CertificatePinner,
        cache: Cache,
        @Named("header") headerInterceptor: Interceptor,
        @Named("server") serverErrorInterceptor: Interceptor,
        @Named("network") liveNetworkInterceptor: Interceptor,
        //chuckInterceptor: ChuckInterceptor,
        gson: Gson
    ): EpayMerchantService {

        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder
            .writeTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
        httpClientBuilder.certificatePinner(certPinner)
        httpClientBuilder.cookieJar(cookieJar)
        httpClientBuilder.cache(cache)
        httpClientBuilder.addInterceptor(headerInterceptor)
        httpClientBuilder.addInterceptor(liveNetworkInterceptor)
        httpClientBuilder.addNetworkInterceptor(serverErrorInterceptor)
        httpClientBuilder.enableTls12()

        //Only for Debug Builds
        /*if (BuildConfig.DEBUG) {
            httpClientBuilder.addInterceptor(httpLoggingInterceptor)
            httpClientBuilder.addInterceptor(chuckInterceptor)
            //httpClientBuilder.addNetworkInterceptor(stethoInterceptor)
        }*/
        val okHttpClient = httpClientBuilder.build()


        return Retrofit.Builder()
            .baseUrl(EPLSDKConfig.EPL_MERCHANT_URL)
            .client(okHttpClient)
            .addConverterFactory(nullOnEmptyConverterFactory)
            .addConverterFactory(GsonConverterFactory.create(gson))
            /*.addCallAdapterFactory(RxJava2CallAdapterFactory.create())*/
            .build()
            .create(EpayMerchantService::class.java)
    }



    @Provides
    @Singleton
    fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        cookieJar: PersistentCookieJar,
        certPinner: CertificatePinner,
        cache: Cache,
        @Named("header") headerInterceptor: Interceptor,
        @Named("server") serverErrorInterceptor: Interceptor,
        @Named("network") liveNetworkInterceptor: Interceptor
       // chuckInterceptor: ChuckInterceptor
    ): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder
            .writeTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
        httpClientBuilder.certificatePinner(certPinner)
        httpClientBuilder.cookieJar(cookieJar)
        httpClientBuilder.cache(cache)
        httpClientBuilder.addInterceptor(headerInterceptor)
        httpClientBuilder.addInterceptor(liveNetworkInterceptor)
        httpClientBuilder.addNetworkInterceptor(serverErrorInterceptor)
        httpClientBuilder.enableTls12()

        //Only for Debug Builds
       /* if (BuildConfig.DEBUG) {
            httpClientBuilder.addInterceptor(httpLoggingInterceptor)
            httpClientBuilder.addInterceptor(chuckInterceptor)
            //httpClientBuilder.addNetworkInterceptor(stethoInterceptor)
        }*/
        return httpClientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideCache(context: Context): Cache {
        return Cache(File(context.cacheDir, "okhttpCache"), CACHE_SIZE.toLong())
    }

    @Provides
    @Singleton
    fun provideCookieJar(context: Context): PersistentCookieJar {
        return PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context))
    }

    @Provides
    @Singleton
    fun provideCertPinner(): CertificatePinner {
        return CertificatePinner.Builder()
            .add(EPLSDKConfig.EPL_SSL_URL, EPLSDKConfig.EPL_SSL_CERT)
            .build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

    @Provides
    @Singleton
    @Named("header")
    fun provideHeaderInterceptor(merchantCredModel: MerchantCredModel): Interceptor {
        return HeaderInterceptor(merchantCredModel.sdkKey, merchantCredModel.sha256)
    }

    @Provides
    @Singleton
    @Named("network")
    fun provideNetworkInterceptor(context: Context): Interceptor {
        return LiveNetworkInterceptor(context)
    }

    @Provides
    @Singleton
    @Named("server")
    fun provideServerErrorInterceptor(): Interceptor {
        return ServerErrorInterceptor()
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor { message -> Log.d("OKHTTP", message) }
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }


   /* @Provides
    @Singleton
    fun provideChuckInterceptor(context: Context): ChuckInterceptor {
        return ChuckInterceptor(context)
    }*/


    companion object {

        /**
         * @return [X509TrustManager] from [TrustManagerFactory]
         *
         * @throws [NoSuchElementException] if not found. According to the Android docs for [TrustManagerFactory], this
         * should never happen because PKIX is the only supported algorithm
         */
        private val trustManager by lazy {
            val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(null as KeyStore?)
            trustManagerFactory.trustManagers
                .first { it is X509TrustManager } as X509TrustManager
        }

        /**
         * If on [Build.VERSION_CODES.LOLLIPOP] or lower, sets [OkHttpClient.Builder.sslSocketFactory] to an instance of
         * [Tls12SocketFactory] that wraps the default [SSLContext.getSocketFactory] for [TlsVersion.TLS_1_2].
         *
         * Does nothing when called on [Build.VERSION_CODES.LOLLIPOP_MR1] or higher.
         *
         * For some reason, Android supports TLS v1.2 from [Build.VERSION_CODES.JELLY_BEAN], but the spec only has it
         * enabled by default from API [Build.VERSION_CODES.KITKAT]. Furthermore, some devices on
         * [Build.VERSION_CODES.LOLLIPOP] don't have it enabled, despite the spec saying they should.
         *
         * @return the (potentially modified) [OkHttpClient.Builder]
         */
        @JvmStatic
        fun OkHttpClient.Builder.enableTls12() = apply {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                try {
                    val sslContext = SSLContext.getInstance(TlsVersion.TLS_1_2.javaName())
                    sslContext.init(null, arrayOf(trustManager), null)

                    sslSocketFactory(Tls12SocketFactory(sslContext.socketFactory), trustManager)
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }

    }

    val nullOnEmptyConverterFactory = object : Converter.Factory() {
        fun converterFactory() = this
        override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) =
            object : Converter<ResponseBody, Any?> {
                val nextResponseBodyConverter =
                    retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)

                override fun convert(value: ResponseBody) =
                    if (value.contentLength() != 0L) nextResponseBodyConverter.convert(value) else null
            }
    }
}