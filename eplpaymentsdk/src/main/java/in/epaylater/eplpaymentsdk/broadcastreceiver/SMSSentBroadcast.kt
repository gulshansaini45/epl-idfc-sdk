package `in`.epaylater.eplpaymentsdk.broadcastreceiver

import `in`.epaylater.eplpaymentsdk.interfaces.EPLSMSSentInterface
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class SMSSentBroadcast(val eplsmsSentInterface : EPLSMSSentInterface) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                 eplsmsSentInterface.onSentSMS(true)
            }
            Activity.RESULT_CANCELED -> {
                eplsmsSentInterface.onSentSMS(false)
            }
        }
    }
}