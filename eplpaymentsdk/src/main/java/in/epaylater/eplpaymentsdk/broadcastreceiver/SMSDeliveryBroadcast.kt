package `in`.epaylater.eplpaymentsdk.broadcastreceiver

import `in`.epaylater.eplpaymentsdk.interfaces.EPLSMSDeliveryInterface
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class SMSDeliveryBroadcast(val eplsmsDeliveryInterface: EPLSMSDeliveryInterface) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                eplsmsDeliveryInterface.onDeliveredSMS(true)
            }
            Activity.RESULT_CANCELED -> {
                eplsmsDeliveryInterface.onDeliveredSMS(false)
            }
        }
    }
}