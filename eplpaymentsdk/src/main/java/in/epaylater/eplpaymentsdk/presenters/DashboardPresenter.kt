package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.DashboardContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.entities.EplUPIProfile
import `in`.epaylater.eplpaymentsdk.entities.EplUserProfile
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import `in`.epaylater.eplpaymentsdk.models.IDFCInitProfileModel
import `in`.epaylater.eplpaymentsdk.models.requests.UPIProfileReq
import `in`.epaylater.eplpaymentsdk.models.responses.EncMessageResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UPIProfileResponse
import `in`.epaylater.eplpaymentsdk.repositories.DashboardRepository
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import javax.inject.Inject

class DashboardPresenter : DashboardContracter.Presenter {

    @Inject
    lateinit var repository: DashboardRepository
    @Inject
    lateinit var epayLaterService: EpayLaterService
    @Inject
    lateinit var sharedPref: EPLSharedPref


    private val view: DashboardContracter.View

    constructor(view: DashboardContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }

    override fun saveUserProfile(profile: IDFCInitProfileModel) {
        sharedPref.putString(EPLSharedPref.PREF_USER_FIRST_NAME, profile.custfirstName)
        sharedPref.putString(EPLSharedPref.PREF_USER_LAST_NAME, profile.custlastName)
        sharedPref.putString(EPLSharedPref.PREF_USER_MIDDLE_NAME, profile.custMiddleName)
        sharedPref.putString(EPLSharedPref.PREF_UPI_PHONE_NO, profile.custPhoneNumber)
        sharedPref.putString(EPLSharedPref.PREF_USER_EMAIL, profile.custEmailAddress)
        sharedPref.putString(EPLSharedPref.PREF_MERCHANT_SDK_KEY, profile.merchantSdkKey)
        sharedPref.putString(EPLSharedPref.PREF_OD_ACCOUNT_NO, profile.odAccountNumber)
        sharedPref.putString(EPLSharedPref.PREF_SAVING_ACCOUNT_NAME, profile.savingAccountName)
        sharedPref.putString(EPLSharedPref.PREF_SAVING_ACCOUNT_NO, profile.savingAccountNumber)
        sharedPref.putString(EPLSharedPref.PREF_NAME_LINKED_SAVING_ACC, profile.custNameLinkedSavingAcc)
        sharedPref.putString(EPLSharedPref.PREF_FCM_TOKEN, profile.fcmToken)
    }

    override fun getToken(data: IDFCInitProfileModel) {
        repository.fetchToken(data)
    }

    override fun getUserProfileFromServer() {
        repository.getUserProfileFromServer()
    }

    override fun sendDeviceData(deviceDataModel: DeviceDataModel) {
        repository.sendDeviceData(deviceDataModel)
    }

    override fun fetchUPIProfileFromServer() {
        repository.fetchUPIProfileFromServer()
    }

    override fun startSimBinding(simSerialNo: String, imeiNo: String) {
        repository.fetchEncSmsMessage(simSerialNo, imeiNo)
    }

    override fun createUPIProfile(upiProfileReq: UPIProfileReq) {
        repository.createUPIProfile(upiProfileReq)
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_LOGIN_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_LOGIN_API)
                    } else {
                        view.sendDeviceData()
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            type == EPLConstants.TYPES.TYPE_DEVICE_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_DEVICE_API)
                    } else {
                        getUserProfileFromServer()
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            type == EPLConstants.TYPES.TYPE_USER_PROFILE_API -> {
                try {
                    if (result is ErrorResponse) {
                        if (result.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_USED_IS_NOT_SAME)) {
                            view.getSimDetails()
                        } else if (result.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)) {
                            view.getSimDetails()
                        } else {
                            onError(result, EPLConstants.TYPES.TYPE_USER_PROFILE_API)
                        }
                    } else if (result is EplUserProfile) {
                        if (result.features.upi.upiEnabled) {
                            fetchUPIProfileFromServer()
                        } else {
                            errorResult()
                        }
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            type == EPLConstants.TYPES.TYPE_UPI_PROFILE_API -> {
                try {
                    if (result is ErrorResponse) {
                        if (result.eplErrorCode.equals(ServerErrorInterceptor.UPI_PROFILE_NOT_FOUND)) {
                            view.setUpUpi()
                        } else if (result.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)) {
                            view.getSimDetails()
                        } else {
                            onError(result, EPLConstants.TYPES.TYPE_UPI_PROFILE_API)
                        }
                    } else if (result is EplUPIProfile) {
                        when {
                            result.simRebindingRequired -> {
                                view.getSimDetails()
                            }
                            else -> {
                                saveEPLUPIProfile(result)
                                view.goToEPLHomeScreen()
                            }
                        }
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            type == EPLConstants.TYPES.TYPE_ENCRYPT_MESSAGE_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_ENCRYPT_MESSAGE_API)
                    } else if (result is EncMessageResponse) {
                        view.sendSilentSMS(result)
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            type == EPLConstants.TYPES.TYPE_CREAT_UPI_PROFILE_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_CREAT_UPI_PROFILE_API)
                    } else if (result is UPIProfileResponse) {
                        saveUPIProfile(result)
                        view.goToEPLHomeScreen()
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }

        }
    }

    private fun saveUPIProfile(result: UPIProfileResponse) {
        try {
            sharedPref.putString(EPLSharedPref.PREF_IMEI, result.imei)
            sharedPref.putString(EPLSharedPref.PREF_SIM_SERIAL_NUMBER, result.simSerial)
            sharedPref.putString(EPLSharedPref.PREF_UPI_VPA_ID, result.vpa)
            sharedPref.putInt(EPLSharedPref.PREF_PENDING_APPROVAL_COUNT, result.pendingApprovalCount)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun saveEPLUPIProfile(result: EplUPIProfile) {
        try {
            sharedPref.putString(EPLSharedPref.PREF_IMEI, result.imei)
            sharedPref.putString(EPLSharedPref.PREF_SIM_SERIAL_NUMBER, result.simSerial)
            sharedPref.putString(EPLSharedPref.PREF_UPI_VPA_ID, result.vpa)
            sharedPref.putInt(EPLSharedPref.PREF_PENDING_APPROVAL_COUNT, result.pendingApprovalCount)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        try {
            view.onError(errorResponse, type)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun errorResult() {
        var errorResponse = ErrorResponse(
            type = "Error",
            reason = "OOPs something went wrong!!",
            code = 500,
            message = "OOPs something went wrong!!",
            eplErrorCode = ServerErrorInterceptor.DEFAULT_ERROR_CODE
        )
        onError(errorResponse, EPLConstants.TYPES.DEFAULT)
    }
}