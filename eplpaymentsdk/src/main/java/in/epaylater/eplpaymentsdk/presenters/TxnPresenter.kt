package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.TransactionContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.fragments.EPLDashboardFragment
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UnSettledTxnResonse
import `in`.epaylater.eplpaymentsdk.models.responses.SettledTxnResponse
import `in`.epaylater.eplpaymentsdk.repositories.TxnHistoryRepository
import android.view.View
import kotlinx.android.synthetic.main.fragment_txn_history.*
import kotlinx.android.synthetic.main.layout_empty_default_view.*
import javax.inject.Inject

class TxnPresenter : TransactionContracter.Presenter {

    @Inject
    lateinit var repository: TxnHistoryRepository
    private val view: TransactionContracter.View

    constructor(view: TransactionContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }

    override fun getSettledTransactionList() {
        repository.txnSettledHistory()
    }

    override fun getUnSettledTransactionList() {
        repository.txnUnSettledHistory()
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_SETTLED_TXN -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_SETTLED_TXN)
                    } else if (result is SettledTxnResponse) {
                        if (result.settledTransactionList.isEmpty()) {
                            view.setEmptyTransactionList()
                        } else {
                            view.setSettledTransactionList(result)
                        }
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            type == EPLConstants.TYPES.TYPE_UNSETTLED_TXN -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_UNSETTLED_TXN)
                    } else if (result is UnSettledTxnResonse) {
                        if (result.unsettledTransactionList.isEmpty()) {
                            view.setEmptyTransactionList()
                        } else {
                            view.setUnSettledTransactionList(result)
                        }
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        if(type == EPLConstants.TYPES.TYPE_SETTLED_TXN){
            try {
                if (!errorResponse.eplErrorCode.isNullOrEmpty()
                    && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
                ) {
                    view.hideProgressLoader()
                    view.getFragmentListner().changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
                } else if (errorResponse.code == 401) {
                    view.hideProgressLoader()
                    view.getFragmentListner().changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
                } else {
                    try {
                        view.onError(errorResponse, type)
                    } catch (e: Exception) {
                        EPLLogger.INSTANCE.e(e)
                    }
                }
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }else{
            try {
                view.onError(errorResponse, type)
            } catch (e: Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }
}