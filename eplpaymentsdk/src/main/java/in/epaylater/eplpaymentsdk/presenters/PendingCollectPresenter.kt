package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.PendingCollectContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.fragments.EPLDashboardFragment
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.PendingCollect
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.repositories.PendingCollectRepository
import javax.inject.Inject

class PendingCollectPresenter : PendingCollectContracter.Presenter {

    @Inject
    lateinit var repository: PendingCollectRepository
    private val view: PendingCollectContracter.View

    constructor(view: PendingCollectContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }

    override fun fetchPendingCollectData(simSerialNo: String, imeiNo: String) {
        repository.fetchPendingCollectData(simSerialNo = simSerialNo, imeiNo = imeiNo)
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_PENDING_COLLECT_APPROVAL_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_LOGIN_API)
                    } else {
                        if (result is ArrayList<*>) {
                            view.updateNotificationBadge(result.size)
                            if (result.isNotEmpty()) {
                                view.showPendingCollect(result as ArrayList<PendingCollect>)
                            } else {
                                view.showEmptyList()
                            }
                        }
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        try {
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                view.hideProgressLoader()
                view.getFragmentListner().changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
            } else if (errorResponse.code == 401) {
                view.hideProgressLoader()
                view.getFragmentListner()
                    .changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
            } else {
                view.onError(errorResponse, type)
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }
}