package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.HomeContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.responses.CreditSummaryResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.repositories.HomeRepository
import javax.inject.Inject

class HomePresenter : HomeContracter.Presenter {

    @Inject
    lateinit var repository: HomeRepository
    private val view: HomeContracter.View

    constructor(view: HomeContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }

    fun getCreditSummary() {
        repository.getCreditSummary()
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_CREDIT_SUMMARY_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_CREDIT_SUMMARY_API)
                    } else if (result is CreditSummaryResponse) {
                        view.setCurrentStatus(result)
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        try {
            view.onError(errorResponse, type)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

}