package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.WhereToUseContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.fragments.EPLDashboardFragment
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.WhereToUseResponse
import `in`.epaylater.eplpaymentsdk.repositories.WhereToUseRepository
import android.content.Context
import javax.inject.Inject

class WhereToUsePresenter : WhereToUseContracter.Presenter {

    @Inject
    lateinit var context: Context
    @Inject
    lateinit var repository: WhereToUseRepository
    private val view: WhereToUseContracter.View

    constructor(view: WhereToUseContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }

    override fun fetchWhereToUse() {
        repository.fetchWhereToUse()
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_PROMOTION_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_PROMOTION_API)
                    } else if (result is WhereToUseResponse) {
                        showWhereToUseList(result)
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        try {
            view.hideProgressLoader()
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                view.hideProgressLoader()
                view.getFragmentListner().changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
            } else if (errorResponse.code == 401) {
                view.hideProgressLoader()
                view.getFragmentListner()
                    .changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
            } else {
                view.onError(errorResponse, type)
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    fun showWhereToUseList(list: WhereToUseResponse) {
        try {
            if (list.merchantList.isNotEmpty()) {
                view.showWhereToUseList(list)
            } else {
                view.showDefaultView(context.getString(R.string.empty_where_to_use), false)
            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }
}