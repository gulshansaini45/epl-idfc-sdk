package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.ConfirmPaymentContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.fragments.EPLConfirmPaymentFragment
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.requests.ConfirmCollectReq
import `in`.epaylater.eplpaymentsdk.models.responses.ConfirmPaymentApproveResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ConfirmPaymentDeclineResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.repositories.ConfirmPaymentRepository
import javax.inject.Inject

class ConfirmPaymentPresenter : ConfirmPaymentContracter.Presenter {

    @Inject
    lateinit var repository: ConfirmPaymentRepository

    private val view: ConfirmPaymentContracter.View

    constructor(view: ConfirmPaymentContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }


    override fun approvePayment(confirmCollectReq: ConfirmCollectReq, simSerial: String, imeiNo: String,isScanAndPay : Boolean) {
        repository.approvePayment(
            confirmCollectReq = confirmCollectReq,
            imeiNo = imeiNo,
            simSerialNo = simSerial,
            isScanAndPay = isScanAndPay
        )
    }

    override fun declinePayment(confirmCollectReq: ConfirmCollectReq, simSerial: String, imeiNo: String,isScanAndPay : Boolean) {
        repository.declinePayment(
            confirmCollectReq = confirmCollectReq,
            imeiNo = imeiNo,
            simSerialNo = simSerial,
            isScanAndPay = isScanAndPay
        )
    }
    override fun resendOtp(txnId: String) {
        repository.resendOTP(txnId)
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_CONFIRM_PAYMENT_APPROVE_API -> {
                try {
                    if(result is ErrorResponse){
                      onError(result, EPLConstants.TYPES.TYPE_CONFIRM_PAYMENT_APPROVE_API)
                    }else if (result is ConfirmPaymentApproveResponse) {
                        view.switchScreen(EPLConfirmPaymentFragment.TRANSACTION_APPROVED)
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }

            }
            type == EPLConstants.TYPES.TYPE_CONFIRM_PAYMENT_DECLINE_API -> {
                try {
                   view.switchScreen(EPLConfirmPaymentFragment.TRANSACTION_CANCELLED)
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }

            }
            type == EPLConstants.TYPES.TYPE_RESEND_OTP -> {
                try {
                    view.resendOTPStatus(true)
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }
    }

    override fun onError(errorResponse : ErrorResponse, type: Int) {
        try {
            view.onError(errorResponse, type)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }
}