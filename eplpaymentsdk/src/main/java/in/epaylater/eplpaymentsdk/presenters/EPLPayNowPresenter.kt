package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.EPLPayNowContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.requests.PaymentReq
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.PayOutstandingResponse
import `in`.epaylater.eplpaymentsdk.models.responses.PaymentMethodResponse
import `in`.epaylater.eplpaymentsdk.repositories.PayNowRepository
import javax.inject.Inject

class EPLPayNowPresenter : EPLPayNowContracter.Presenter {


    @Inject
    lateinit var repository: PayNowRepository
    private val view: EPLPayNowContracter.View

    constructor(view: EPLPayNowContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }

    override fun getPaymentMethods() {
        repository.getPaymentMethods()
    }

    override fun payOutstandingAmount(paymentReq: PaymentReq) {
        repository.getPayOutstandingAmount(paymentReq = paymentReq)
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_PAYMENT_METHOD -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_PAYMENT_METHOD)
                    } else if (result is PaymentMethodResponse) {
                        view.showPaymentMethod(result)
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            type == EPLConstants.TYPES.TYPE_PAY_QUTSTANDING_AMOUNT -> {
                try {
                    if (result is ErrorResponse) {
                       onError(result, EPLConstants.TYPES.TYPE_PAY_QUTSTANDING_AMOUNT)
                    } else if (result is PayOutstandingResponse) {
                        view.showPaymentResult(result)
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        try {
            view.onError(errorResponse, type)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }
}