package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.SendMoneyContracter
import `in`.epaylater.eplpaymentsdk.di.modules.PresenterModule
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.fragments.EPLConfirmPaymentFragment
import `in`.epaylater.eplpaymentsdk.fragments.EPLDashboardFragment
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.models.UPIDataModel
import `in`.epaylater.eplpaymentsdk.models.requests.SendMoneyReq
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ScanAndPayResponse
import `in`.epaylater.eplpaymentsdk.repositories.SendMoneyRepository
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import android.content.Context
import android.os.Bundle
import java.util.*
import javax.inject.Inject

class SendMoneyPresenter : SendMoneyContracter.Presenter {

    @Inject
    lateinit var context: Context
    @Inject
    lateinit var sharedPref: EPLSharedPref

    @Inject
    lateinit var repository: SendMoneyRepository
    private val view: SendMoneyContracter.View

    constructor(view: SendMoneyContracter.View) {
        this.view = view
        EPLManager.eplAppComponent.presenterComponent(PresenterModule(this)).inject(this)
    }

    override fun proceedToPay(sendMoneyReq: SendMoneyReq, imeiNo: String, serialNo: String) {
        repository.scanAndPay(sendMoneyReq = sendMoneyReq, imeiNo = imeiNo, simSerialNo = serialNo)
    }

    override fun onSuccess(result: Any?, type: Int) {
        when {
            type == EPLConstants.TYPES.TYPE_SCAN_N_PAY_API -> {
                try {
                    if (result is ErrorResponse) {
                        onError(result, EPLConstants.TYPES.TYPE_SCAN_N_PAY_API)
                    } else if (result is ScanAndPayResponse) {
                        view.switchScreen(result)
                    }
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        }
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        try {
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                view.hideProgressLoader()
                view.getFragmentListner().changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
            } else if (errorResponse.code == 401) {
                view.hideProgressLoader()
                view.getFragmentListner()
                    .changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
            } else {
                view.onError(errorResponse, type)
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun validatePayData(amount: String, source: String?, upiPayload: UPIDataModel?) {
        if (amount.isNullOrEmpty()) {
            view.setErrorMessage(context.getString(R.string.enter_amount))
        } else if (!(amount.matches(EPLConstants.REGULAR_EXPRESS.regxAmount.toRegex())) || amount.toString().toFloat() < 1) {
            view.setErrorMessage(context.getString(R.string.enter_correct_amount))
        } else {
            var sourceType = SendMoneyReq.Source.QRCODE
            source?.let {
                if (it.equals(SendMoneyReq.Source.INTENT)) {
                    sourceType = SendMoneyReq.Source.INTENT
                }
            }

            try {
                val simSerial = sharedPref.getString(EPLSharedPref.PREF_SIM_SERIAL_NUMBER)
                val simImei = sharedPref.getString(EPLSharedPref.PREF_IMEI)
                upiPayload?.let {
                    var payeeVpa = it.payeeAddress
                    var payeeCode = it.merchantCode
                    var payeeTransaferType = it.transferType.type
                    var isBharatQR = it.isBharatQR
                    var remarks = it.transactionNote
                    if (remarks.isNullOrBlank() || remarks.isNullOrEmpty()) {
                        remarks = "NA"
                    }
                    var isValidData = checkRequestValidity(it)
                    if (isValidData) {
                        var sendMoneyReq = SendMoneyReq(
                            amount = amount,
                            payeeVpa = payeeVpa,
                            payeeCode = payeeCode!!,
                            payeeTransferType = payeeTransaferType,
                            isBharatQr = isBharatQR,
                            source = sourceType,
                            orgTxnId = generateOrgTxnId(),
                            remarks = remarks
                        )
                        view.showProgressLoader()
                        proceedToPay(sendMoneyReq = sendMoneyReq, imeiNo = simImei, serialNo = simSerial)
                    } else {
                        inValidQRCodeDetails()
                    }
                }
            } catch (e: Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }

    private fun checkRequestValidity(upiPayload: UPIDataModel): Boolean {
        try {
            if (!upiPayload.payeeAddress.isNullOrEmpty()) {
                return true
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        return false
    }

    private fun inValidQRCodeDetails() {
        var errorResponse = ErrorResponse(
            type = "Error",
            reason = "Invalid QR Code",
            code = 500,
            message = "Invalid QR Code",
            eplErrorCode = ServerErrorInterceptor.DEFAULT_ERROR_CODE
        )
        onError(errorResponse, EPLConstants.TYPES.DEFAULT)
    }

    private fun generateOrgTxnId(): String {
        return UUID.randomUUID().toString()
    }

    override fun switchScreen(amount: String, txnId: String?, payeename: String?) {
        try {
            var bundle = Bundle()
            bundle.putString(EPLConfirmPaymentFragment.TRANSACTION_ID, txnId)
            bundle.putString(EPLConfirmPaymentFragment.TRANSACTION_AMOUNT, amount)
            bundle.putString(EPLConfirmPaymentFragment.TIME_AGO, context.getString(R.string.default_time_ago))
            bundle.putString(EPLConfirmPaymentFragment.SCREEN_NAME, context.getString(R.string.title_scan_n_pay))
            bundle.putBoolean(EPLConfirmPaymentFragment.IS_SCAN_N_PAY_SCREEN, true)
            bundle.putString(EPLConfirmPaymentFragment.PAYEE_NAME, payeename)
            var fragment = EPLConfirmPaymentFragment()
            fragment.arguments = bundle
            view.getFragmentListner().changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }
}