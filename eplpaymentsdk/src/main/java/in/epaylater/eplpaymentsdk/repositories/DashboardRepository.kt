package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import `in`.epaylater.eplpaymentsdk.models.IDFCInitProfileModel
import `in`.epaylater.eplpaymentsdk.models.requests.UPIProfileReq
import `in`.epaylater.eplpaymentsdk.models.responses.EncMessageResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UPIProfileResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UserProfileResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import `in`.epaylater.eplpaymentsdk.utils.EPLCommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DashboardRepository(
    private val epayLaterService: EpayLaterService,
    private val presenter: BasePresenter
) : BaseRepository() {

    fun fetchToken(data: IDFCInitProfileModel) {
        var call = epayLaterService.fetchCustomerLogin(EPLCommonUtil.INSTANCE.convertRequest(data))
        call.enqueue(object : Callback<Any> {
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_LOGIN_API, t)
            }

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_LOGIN_API, response)
            }

        });
    }

    fun sendDeviceData(deviceDataModel: DeviceDataModel) {
        val call = epayLaterService.postDeviceData(deviceDataModel)
        call.enqueue(object : Callback<Any> {
            override fun onFailure(call: Call<Any>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_DEVICE_API, t)
            }

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_DEVICE_API, response)
            }

        });
    }

    fun fetchUPIProfileFromServer() {
        val call = epayLaterService.fetchUPIProfile()
        call.enqueue(object : Callback<UPIProfileResponse> {
            override fun onFailure(call: Call<UPIProfileResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_UPI_PROFILE_API, t)
            }

            override fun onResponse(call: Call<UPIProfileResponse>, response: Response<UPIProfileResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_UPI_PROFILE_API, response)
            }
        });
    }

    fun getUserProfileFromServer() {
        val call = epayLaterService.fetchUserProfile()
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_USER_PROFILE_API, t)
            }

            override fun onResponse(call: Call<UserProfileResponse>, response: Response<UserProfileResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_USER_PROFILE_API, response)
            }
        });

    }

    fun fetchEncSmsMessage(simSerialNo: String, imeiNo: String) {
        var call = epayLaterService.getEncryptedSMSMessage(imeiNo, simSerialNo)
        call.enqueue(object : Callback<EncMessageResponse> {
            override fun onFailure(call: Call<EncMessageResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_ENCRYPT_MESSAGE_API, t)
            }

            override fun onResponse(call: Call<EncMessageResponse>, response: Response<EncMessageResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_ENCRYPT_MESSAGE_API, response)
            }

        });
    }

    fun createUPIProfile(upiProfile: UPIProfileReq) {
        val call = epayLaterService.createUpiProfile(upiProfile)
        call.enqueue(object : Callback<UPIProfileResponse> {
            override fun onFailure(call: Call<UPIProfileResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_CREAT_UPI_PROFILE_API, t)
            }

            override fun onResponse(call: Call<UPIProfileResponse>, response: Response<UPIProfileResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_CREAT_UPI_PROFILE_API, response)
            }
        });
    }
}