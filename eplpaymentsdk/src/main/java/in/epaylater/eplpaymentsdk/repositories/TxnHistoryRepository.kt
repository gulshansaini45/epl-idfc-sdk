package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.models.responses.SettledTxnResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UnSettledTxnResonse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TxnHistoryRepository(
    private val epayLaterService: EpayLaterService,
    private val presenter: BasePresenter
) : BaseRepository() {

    fun txnUnSettledHistory() {
        var call = epayLaterService.fetchUnSettledTxn()
        call.enqueue(object : Callback<UnSettledTxnResonse> {
            override fun onFailure(call: Call<UnSettledTxnResonse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_UNSETTLED_TXN, t)
            }

            override fun onResponse(call: Call<UnSettledTxnResonse>, response: Response<UnSettledTxnResonse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_UNSETTLED_TXN, response)
            }
        });
    }


    fun txnSettledHistory() {
        var call = epayLaterService.fetchSettledTxn()
        call.enqueue(object : Callback<SettledTxnResponse> {
            override fun onFailure(call: Call<SettledTxnResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_SETTLED_TXN, t)
            }

            override fun onResponse(call: Call<SettledTxnResponse>, response: Response<SettledTxnResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_SETTLED_TXN, response)
            }
        });
    }
}