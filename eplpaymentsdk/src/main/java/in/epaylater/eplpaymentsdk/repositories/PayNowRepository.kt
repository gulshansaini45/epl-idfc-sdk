package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.models.requests.PaymentReq
import `in`.epaylater.eplpaymentsdk.models.responses.PayOutstandingResponse
import `in`.epaylater.eplpaymentsdk.models.responses.PaymentMethodResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayNowRepository(
    private val epayLaterService: EpayLaterService,
    private val presenter: BasePresenter
) : BaseRepository() {

    fun getPaymentMethods() {
        var call = epayLaterService.fetchPaymentMethod()
        call.enqueue(object : Callback<PaymentMethodResponse> {
            override fun onFailure(call: Call<PaymentMethodResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_PAYMENT_METHOD, t)
            }

            override fun onResponse(call: Call<PaymentMethodResponse>, response: Response<PaymentMethodResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_PAYMENT_METHOD, response)
            }
        });
    }

    fun getPayOutstandingAmount(paymentReq: PaymentReq) {
        var call = epayLaterService.payOutStandingAmount(paymentReq)
        call.enqueue(object : Callback<PayOutstandingResponse> {
            override fun onFailure(call: Call<PayOutstandingResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_PAY_QUTSTANDING_AMOUNT, t)
            }

            override fun onResponse(call: Call<PayOutstandingResponse>, response: Response<PayOutstandingResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_PAY_QUTSTANDING_AMOUNT, response)
            }
        });
    }
}