package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.models.responses.CreditSummaryResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeRepository(
    private val epayLaterService: EpayLaterService,
    private val presenter: BasePresenter
) : BaseRepository() {

    fun getCreditSummary() {
        var call = epayLaterService.fetchCreditSummary()
        call.enqueue(object : Callback<CreditSummaryResponse> {
            override fun onFailure(call: Call<CreditSummaryResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_CREDIT_SUMMARY_API, t)
            }

            override fun onResponse(call: Call<CreditSummaryResponse>, response: Response<CreditSummaryResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_CREDIT_SUMMARY_API, response)
            }
        });
    }
}