package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.models.requests.ConfirmCollectReq
import `in`.epaylater.eplpaymentsdk.models.responses.ConfirmPaymentApproveResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ConfirmPaymentDeclineResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ScanAndPayResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ConfirmPaymentRepository(
    private val epayLaterService: EpayLaterService,
    private val presenter: BasePresenter
) : BaseRepository() {


    fun approvePayment(
        confirmCollectReq: ConfirmCollectReq,
        imeiNo: String,
        simSerialNo: String,
        isScanAndPay: Boolean
    ) {
        var call: Call<ConfirmPaymentApproveResponse>
        if (isScanAndPay) {
            call = epayLaterService.confirmPaymentScanAndPayApprove(
                confirmCollectReq = confirmCollectReq,
                imeiNo = imeiNo,
                serialNo = simSerialNo
            )
        } else {
            call = epayLaterService.confirmCollectMoneyApprove(
                confirmCollectReq = confirmCollectReq,
                imeiNo = imeiNo,
                serialNo = simSerialNo
            )
        }
        call.enqueue(object : Callback<ConfirmPaymentApproveResponse> {
            override fun onFailure(call: Call<ConfirmPaymentApproveResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_CONFIRM_PAYMENT_APPROVE_API, t)
            }

            override fun onResponse(
                call: Call<ConfirmPaymentApproveResponse>,
                response: Response<ConfirmPaymentApproveResponse>
            ) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_CONFIRM_PAYMENT_APPROVE_API, response)
            }
        });
    }

    fun declinePayment(
        confirmCollectReq: ConfirmCollectReq,
        imeiNo: String,
        simSerialNo: String,
        isScanAndPay: Boolean
    ) {
        var call: Call<ConfirmPaymentDeclineResponse>
        if (isScanAndPay) {
            call = epayLaterService.confirmPaymentScanAndPayDecline(
                confirmCollectReq = confirmCollectReq,
                imeiNo = imeiNo,
                serialNo = simSerialNo
            )
        } else {
            call = epayLaterService.confirmCollectMoneyDecline(
                confirmCollectReq = confirmCollectReq,
                imeiNo = imeiNo,
                serialNo = simSerialNo
            )
        }
        call.enqueue(object : Callback<ConfirmPaymentDeclineResponse> {
            override fun onFailure(call: Call<ConfirmPaymentDeclineResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_CONFIRM_PAYMENT_DECLINE_API, t)
            }

            override fun onResponse(
                call: Call<ConfirmPaymentDeclineResponse>,
                response: Response<ConfirmPaymentDeclineResponse>
            ) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_CONFIRM_PAYMENT_DECLINE_API, response)
            }
        });
    }


    fun resendOTP(txnId: String) {
        var call = epayLaterService.resendOTP(txnId)
        call.enqueue(object : Callback<ScanAndPayResponse> {

            override fun onFailure(call: Call<ScanAndPayResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_RESEND_OTP, t)
            }

            override fun onResponse(call: Call<ScanAndPayResponse>, response: Response<ScanAndPayResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_RESEND_OTP, response)
            }

        });
    }

}