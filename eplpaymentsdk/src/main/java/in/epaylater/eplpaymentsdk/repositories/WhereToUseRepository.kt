package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.interfaces.EpayMerchantService
import `in`.epaylater.eplpaymentsdk.models.responses.WhereToUseResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WhereToUseRepository(
    private val epayLaterMerchantService: EpayMerchantService,
    private val presenter: BasePresenter
) : BaseRepository() {

    /*fun fetchWhereToUse() {
        var call = epayLaterService.fetchWhereToUse()
        call.enqueue(object : Callback<WhereToUserResponse> {
            override fun onFailure(call: Call<WhereToUserResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_PROMOTION_API, t)
            }

            override fun onResponse(call: Call<WhereToUserResponse>, response: Response<WhereToUserResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_PROMOTION_API, response)
            }
        });
    }*/

    fun fetchWhereToUse() {
        var call = epayLaterMerchantService.fetchWhereToUse()
        call.enqueue(object : Callback<WhereToUseResponse> {
            override fun onFailure(call: Call<WhereToUseResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_PROMOTION_API, t)
            }

            override fun onResponse(call: Call<WhereToUseResponse>, response: Response<WhereToUseResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_PROMOTION_API, response)
            }
        });
    }
}