package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.models.requests.SendMoneyReq
import `in`.epaylater.eplpaymentsdk.models.responses.ScanAndPayResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SendMoneyRepository(
    private val epayLaterService: EpayLaterService,
    private val presenter: BasePresenter
) : BaseRepository() {

    fun scanAndPay(sendMoneyReq: SendMoneyReq, imeiNo: String, simSerialNo: String) {
        var call = epayLaterService.scanAndPay(sendMoneyReq = sendMoneyReq, imeiNo = imeiNo, serialNo = simSerialNo)
        call.enqueue(object : Callback<ScanAndPayResponse> {
            override fun onFailure(call: Call<ScanAndPayResponse>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_SCAN_N_PAY_API, t)
            }

            override fun onResponse(call: Call<ScanAndPayResponse>, response: Response<ScanAndPayResponse>) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_SCAN_N_PAY_API, response)
            }
        });
    }
}