package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.models.requests.PendingCollectMoneyResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PendingCollectRepository(
    private val epayLaterService: EpayLaterService,
    private val presenter: BasePresenter
) : BaseRepository() {

    fun fetchPendingCollectData(simSerialNo: String, imeiNo: String) {
        val call = epayLaterService.fetchPendingApprovalCollectRequest(simSerial = simSerialNo, imei = imeiNo)
        call.enqueue(object : Callback<List<PendingCollectMoneyResponse>> {

            override fun onFailure(call: Call<List<PendingCollectMoneyResponse>>, t: Throwable) {
                onFailure(presenter, EPLConstants.TYPES.TYPE_PENDING_COLLECT_APPROVAL_API, t)
            }

            override fun onResponse(
                call: Call<List<PendingCollectMoneyResponse>>,
                response: Response<List<PendingCollectMoneyResponse>>
            ) {
                onResponse(presenter, EPLConstants.TYPES.TYPE_PENDING_COLLECT_APPROVAL_API, response)
            }
        });
    }
}