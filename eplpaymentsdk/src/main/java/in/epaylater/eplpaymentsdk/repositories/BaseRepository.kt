package `in`.epaylater.eplpaymentsdk.repositories

import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.entities.EplUPIProfile
import `in`.epaylater.eplpaymentsdk.entities.EplUserProfile
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.models.PendingCollect
import `in`.epaylater.eplpaymentsdk.models.requests.PendingCollectMoneyResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UPIProfileResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UserProfileResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import org.json.JSONObject
import retrofit2.Response
import java.net.UnknownHostException

abstract class BaseRepository() {

    private var DEFAULT_ERROR_CODE: Int = 500
    private var DEFAULT_ERROR_MESSAGE: String = "OOP's something went wrong!"
    private fun errorResponse(response: String): ErrorResponse {
        try {
            var jsonObject = JSONObject(response)
            var type = try {
                jsonObject.getString("type")
            } catch (e: Exception) {
                ""
            }

            if (type.isNullOrEmpty()) {
                var errors = jsonObject.getJSONArray("errors")
                if (!errors.isNull(0)) {
                    return getErrorResult(errors.get(0).toString(), DEFAULT_ERROR_CODE)
                }
            } else {
                var errorResponse = ErrorResponse(
                    jsonObject.getString("type"),
                    jsonObject.getString("reason"),
                    jsonObject.getString("code").toInt(),
                    jsonObject.getString("message"),
                    jsonObject.getString("eplErrorCode")
                )

                return errorResponse

            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        return getErrorResult("", DEFAULT_ERROR_CODE)
    }

    private fun defaultErrorResult(t: Throwable): ErrorResponse {
        var message = "OOP's something went wrong!"
        var errorCode = 400
        var eplErrorCode = ServerErrorInterceptor.DEFAULT_ERROR_CODE
        try {
            if (t is ServerException.DefaultErrorResponse) {
                message = t.message.toString()
            } else if (t is UnknownHostException) {
                message = "No internet connection"
            } else if (t is ServerException.IDFCErrorResponse) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIProfileNotFoundException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIInvalidMobileAppVersionException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPISimUsedIsNotSameException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
                eplErrorCode = ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED
            } else if (t is ServerException.UPIProfileAlreadyExistException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPISimBindingRequiredException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
                eplErrorCode = ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED
            } else if (t is ServerException.UPISimRebindingNotRequiredException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIFailedToCreateUpiProfileException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIApprovalIsNotPendingException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIInvalidOtpForTransactionException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIFailedToApproveUpiTransactionException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UserNotAuthorized) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
                eplErrorCode = ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED
            } else if (t is ServerException.UPITransactionExpired) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPITransactionInProgress) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIFailedToApproveUpiTransactionException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIInvalidOtpForTransactionException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIApprovalIsNotPendingException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            } else if (t is ServerException.UPIFailedToCreateUpiProfileException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
                eplErrorCode = ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED
            } else if (t is ServerException.UPIProfileNotFoundException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
                eplErrorCode = ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED
            } else if (t is ServerException.UPIProfileAlreadyExistException) {
                message = t.message?.let { it } ?: "OOP's something went wrong!"
            }else if (t is javax.net.ssl.SSLPeerUnverifiedException) {
                message = "Connection could not be established due to a technical error. Please contact us on 1800 419 4332 or banker@idfcbank.com for further assistance"
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        var errorResponse = ErrorResponse(
            type = "Error",
            reason = message,
            code = errorCode,
            message = message,
            eplErrorCode = eplErrorCode
        )
        return errorResponse
    }

    private fun getErrorResult(error: String, responseCode: Int): ErrorResponse {
        var message = error
        if (error.isNullOrEmpty()) {
            message = DEFAULT_ERROR_MESSAGE
        } else if (error.contains("No address associated with hostname")) {
            message = "No internet connection"
        }
        if (responseCode == 401) {
            message = "Session Expired!"
        }
        var errorResponse = ErrorResponse(
            type = "Error",
            reason = message,
            code = responseCode,
            message = message,
            eplErrorCode = ServerErrorInterceptor.DEFAULT_ERROR_CODE
        )
        return errorResponse
    }

    protected fun onFailure(presenter: BasePresenter, typeCode: Int, t: Throwable) {
        try {
            var errorResponse = defaultErrorResult(t)
            presenter.onError(errorResponse, typeCode)
        } catch (e: Exception) {
            var errorResponse = defaultErrorResult(Throwable(""))
            presenter.onError(errorResponse, typeCode)
        }
    }

    protected fun onResponse(presenter: BasePresenter, typeCode: Int, response: Response<*>) {
        var responseCode = if (typeCode == EPLConstants.TYPES.TYPE_CREAT_UPI_PROFILE_API) 201 else 200
        try {
            if (response.isSuccessful && response.code() == responseCode) {
                if (typeCode == EPLConstants.TYPES.TYPE_UPI_PROFILE_API) {
                    var upiProfile = response.body()
                    upiProfile?.let {
                        val eplupi =
                            EplUPIProfile.mapEplUPIProfileFromUPIProfileResponse(upiProfile as UPIProfileResponse)
                        presenter.onSuccess(eplupi, typeCode)
                    }
                } else if (typeCode == EPLConstants.TYPES.TYPE_USER_PROFILE_API) {
                    var eplUserProfile = response.body()?.let {
                        EplUserProfile.mapFromDTO(it as UserProfileResponse)
                    }
                    presenter.onSuccess(eplUserProfile, typeCode)
                }else if(typeCode == EPLConstants.TYPES.TYPE_PENDING_COLLECT_APPROVAL_API){
                    response.body()?.let {
                        var list = createPendingCollectList(it as List<PendingCollectMoneyResponse>)
                        presenter.onSuccess(list, EPLConstants.TYPES.TYPE_PENDING_COLLECT_APPROVAL_API)
                    }

                }else {
                    presenter.onSuccess(response.body(), typeCode)
                }
            } else if (response.code() == 401) {
                var errorResponse = getErrorResult(DEFAULT_ERROR_MESSAGE, 401)
                presenter.onError(errorResponse, typeCode);
            } else if (response.errorBody() != null) {
                var errorResponse = errorResponse(response.errorBody()?.string().toString())
                presenter.onSuccess(errorResponse, typeCode)
            } else if (response.code() in 500..599) {
                var errorResponse = getErrorResult(DEFAULT_ERROR_MESSAGE, 500)
                presenter.onError(errorResponse, typeCode);
            } else {
                var errorResponse = getErrorResult(response.message(), DEFAULT_ERROR_CODE)
                presenter.onError(errorResponse, typeCode);
            }
        } catch (e: Exception) {
            var errorResponse = defaultErrorResult(Throwable(""))
            presenter.onError(errorResponse, typeCode)
        }
    }

    private fun createPendingCollectList(listresponse: List<PendingCollectMoneyResponse>): List<PendingCollect> {
        val list: MutableList<PendingCollect> = ArrayList()
        for (i in listresponse) {
            list.add(PendingCollect.mapPendingCollectFromPendingCollectResponse(i))
        }
        return list
    }

}