package `in`.epaylater.eplpaymentsdk.interfaces

import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import `in`.epaylater.eplpaymentsdk.models.requests.*
import `in`.epaylater.eplpaymentsdk.models.responses.*
import `in`.epaylater.eplpaymentsdk.repositories.WhereToUseRepository
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface EpayLaterService {

    @POST("/api/v1/merchant/customer/login")
    fun fetchCustomerLogin(@Body loginReq: LoginReq): Call<Any>

    @POST("/api/v1/user/device")
    fun postDeviceData(@Body deviceDataModel: DeviceDataModel): Call<Any>

    @GET("/api/v2/user/profile")
    fun fetchUserProfile(): Call<UserProfileResponse>

    @GET("/api/v3/upi/customer/profile")
    fun fetchUPIProfile(): Call<UPIProfileResponse>

    @GET("/api/v3/upi/idfc/encrypted-sms/{imei}/{sim-serial}")
    fun getEncryptedSMSMessage(
        @Path("imei") imei: String,
        @Path("sim-serial") simSerial: String
    ): Call<EncMessageResponse>

    @POST("/api/v3/upi/idfc/profile")
    fun createUpiProfile(@Body upiProfileReq: UPIProfileReq): Call<UPIProfileResponse>

    @POST("/api/v3/upi/idfc/scan-and-pay/{imei}/{sim-serial}")
    fun scanAndPay(
        @Body sendMoneyReq: SendMoneyReq,
        @Path("imei") imeiNo: String?,
        @Path("sim-serial") serialNo: String?
    ): Call<ScanAndPayResponse>


    @POST("/api/v3/upi/idfc/confirm-scan-and-pay/{imei}/{sim-serial}")
    fun confirmPaymentScanAndPayApprove(
        @Body confirmCollectReq: ConfirmCollectReq,
        @Path("imei") imeiNo: String?,
        @Path("sim-serial") serialNo: String?
    ): Call<ConfirmPaymentApproveResponse>

    @POST("/api/v3/upi/idfc/confirm-scan-and-pay/{imei}/{sim-serial}")
    fun confirmPaymentScanAndPayDecline(
        @Body confirmCollectReq: ConfirmCollectReq,
        @Path("imei") imeiNo: String?,
        @Path("sim-serial") serialNo: String?
    ): Call<ConfirmPaymentDeclineResponse>

    @POST("/api/v3/upi/idfc/confirm-collect-money/{imei}/{sim-serial}")
    fun confirmCollectMoneyApprove(
        @Body confirmCollectReq: ConfirmCollectReq,
        @Path("imei") imeiNo: String?,
        @Path("sim-serial") serialNo: String?
    ): Call<ConfirmPaymentApproveResponse>

    @POST("/api/v3/upi/idfc/confirm-collect-money/{imei}/{sim-serial}")
    fun confirmCollectMoneyDecline(
        @Body confirmCollectReq: ConfirmCollectReq,
        @Path("imei") imeiNo: String?,
        @Path("sim-serial") serialNo: String?
    ): Call<ConfirmPaymentDeclineResponse>


    @POST("/api/v3/upi/idfc/txn/{txn-id}/resend-otp/mobile")
    fun resendOTP(
        @Path("txn-id") txnId: String
    ): Call<ScanAndPayResponse>


    @GET("/api/v3/upi/idfc/pending-approval-collect-requests/{imei}/{sim-serial}")
    fun fetchPendingApprovalCollectRequest(
        @Path("imei") imei: String,
        @Path("sim-serial") simSerial: String
    ): Call<List<PendingCollectMoneyResponse>>


    @GET("/api/v1/merchant/customer/getCreditSummary")
    fun fetchCreditSummary(): Call<CreditSummaryResponse>

    @GET("/api/v1/merchant/customer/transactions/settled")
    fun fetchSettledTxn(): Call<SettledTxnResponse>


    @GET("/api/v1/merchant/customer/transactions/unsettled")
    fun fetchUnSettledTxn(): Call<UnSettledTxnResonse>

    @GET("/api/v1/merchant/getPaymentMethods")
    fun fetchPaymentMethod(): Call<PaymentMethodResponse>

    @POST("/api/v1/merchant/customer/payOutstandingAmount")
    fun payOutStandingAmount(@Body req: PaymentReq): Call<PayOutstandingResponse>

   /* @GET("/api/v1/merchant/promotion")
    fun fetchWhereToUse(): Call<WhereToUserResponse>*/

    @GET("/api/v1/merchant/promotion")
    fun fetchWhereToUse(): Call<WhereToUseRepository>

}