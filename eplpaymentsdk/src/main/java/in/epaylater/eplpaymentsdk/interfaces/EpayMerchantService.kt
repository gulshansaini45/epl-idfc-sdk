package `in`.epaylater.eplpaymentsdk.interfaces

import `in`.epaylater.eplpaymentsdk.models.responses.WhereToUseResponse
import retrofit2.Call
import retrofit2.http.GET

interface EpayMerchantService {

    @GET("idfcPostpaid/idfcMerchantList.json")
    fun fetchWhereToUse(): Call<WhereToUseResponse>
}