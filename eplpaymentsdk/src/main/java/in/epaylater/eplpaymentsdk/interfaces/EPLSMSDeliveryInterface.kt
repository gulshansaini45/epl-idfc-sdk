package `in`.epaylater.eplpaymentsdk.interfaces

interface EPLSMSDeliveryInterface {
    fun onDeliveredSMS(result: Boolean)
}