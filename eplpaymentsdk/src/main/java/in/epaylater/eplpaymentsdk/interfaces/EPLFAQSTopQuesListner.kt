package `in`.epaylater.eplpaymentsdk.interfaces

interface EPLFAQSTopQuesListner {
    fun showTopQuestionDetails(position: Int)
}