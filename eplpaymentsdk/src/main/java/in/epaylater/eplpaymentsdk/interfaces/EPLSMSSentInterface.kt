package `in`.epaylater.eplpaymentsdk.interfaces

interface EPLSMSSentInterface {
    fun onSentSMS(result: Boolean)
}