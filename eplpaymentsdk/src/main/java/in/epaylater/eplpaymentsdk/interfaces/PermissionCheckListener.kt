package `in`.epaylater.eplpaymentsdk.interfaces

interface PermissionCheckListener {
    fun onPermissionGranted(isPermissionGranted: Boolean)
}