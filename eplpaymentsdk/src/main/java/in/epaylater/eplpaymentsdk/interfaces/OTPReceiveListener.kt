package `in`.epaylater.eplpaymentsdk.interfaces

interface OTPReceiveListener {
    fun onOTPReceived(otp: String)
    fun onOTPTimeOut()
}