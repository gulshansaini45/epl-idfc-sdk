package `in`.epaylater.eplpaymentsdk.interfaces

import android.support.v4.app.Fragment

interface EpayFragmentListner {
    fun changeEplFragment(fragment: Fragment,type:Int)
    fun setActionTitle(title: String,headerViewType:Int = 0,keyboardAdjustType:Int= 0)
}