package `in`.epaylater.eplpaymentsdk.adapters.viewholders

import `in`.epaylater.eplpaymentsdk.R
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class TxnViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    var ivTxnIcon = view.findViewById<ImageView>(R.id.ivTxnIcon)
    var tvMarketPlaceName = view.findViewById<TextView>(R.id.tvMarketPlaceName)
    var tvTxnAmount = view.findViewById<TextView>(R.id.tvTxnAmount)
    var tvTxndate = view.findViewById<TextView>(R.id.tvTxndate)
}