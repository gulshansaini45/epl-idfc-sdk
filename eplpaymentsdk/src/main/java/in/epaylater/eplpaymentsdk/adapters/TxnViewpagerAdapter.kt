package `in`.epaylater.eplpaymentsdk.adapters

import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.ViewGroup

class TxnViewpagerAdapter : FragmentPagerAdapter {

    private var mFragmentList: MutableList<Fragment>
    private var mFragmentTitle: MutableList<String>
    private var fragmentManager: FragmentManager

    constructor(fragmentManager: FragmentManager) : super(fragmentManager) {
        this.fragmentManager = fragmentManager
        this.mFragmentTitle = ArrayList()
        this.mFragmentList = ArrayList()
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitle.add(title)
    }

    override fun getItem(position: Int): Fragment {
        return mFragmentList.get(position)
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
    }
    override fun getCount(): Int {
        return mFragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitle.get(position)
    }

    fun popAllFragment() {
        try {
           mFragmentList.clear()
            notifyDataSetChanged()
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }
}