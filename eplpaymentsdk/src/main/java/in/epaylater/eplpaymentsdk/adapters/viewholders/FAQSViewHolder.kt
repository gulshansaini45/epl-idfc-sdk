package `in`.epaylater.eplpaymentsdk.adapters.viewholders

import `in`.epaylater.eplpaymentsdk.R
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView

class FAQSViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var tvTitle = view.findViewById<TextView>(R.id.tvTitle)
    var rlContainer = view.findViewById<RelativeLayout>(R.id.rlContainer)
}