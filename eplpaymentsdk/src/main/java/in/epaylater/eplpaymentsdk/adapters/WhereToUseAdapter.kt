package `in`.epaylater.eplpaymentsdk.adapters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.viewholders.WhereToUseViewHolder
import `in`.epaylater.eplpaymentsdk.models.responses.WhereToUseResponse
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout

class WhereToUseAdapter() : RecyclerView.Adapter<WhereToUseViewHolder>() {
    lateinit var whereToUserResponse: WhereToUseResponse
    lateinit var layoutManager: LinearLayoutManager
lateinit var context:Context
   constructor(context: Context, whereToUseResponse : WhereToUseResponse) : this() {
        this.whereToUserResponse = whereToUseResponse
        layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): WhereToUseViewHolder {
        context = viewGroup.context
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_item_where_to_use, viewGroup, false)

        return WhereToUseViewHolder(view)
    }

    override fun getItemCount(): Int {
        return whereToUserResponse.merchantList.size
    }

    override fun onBindViewHolder(viewHolder: WhereToUseViewHolder, position: Int) {
        val categoryList = whereToUserResponse.merchantList.get(position)
        viewHolder.tvCategoryName?.text = categoryList.sectionTitle
        val marketPlaceAdapter = MarketPlaceAdapter(categoryList.merchants)
        viewHolder.rvMarketPlaceList.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
        viewHolder.rvMarketPlaceList.adapter = marketPlaceAdapter
        viewHolder.rvMarketPlaceList.hasFixedSize()
    }

}