package `in`.epaylater.eplpaymentsdk.adapters.viewholders

import `in`.epaylater.eplpaymentsdk.R
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class MarketPlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var ivMarketPlaceIcon = itemView.findViewById<ImageView>(R.id.ivMarketPlaceIcon)
    var tvMarketplaceName = itemView.findViewById<TextView>(R.id.tvMarketplaceName)
}