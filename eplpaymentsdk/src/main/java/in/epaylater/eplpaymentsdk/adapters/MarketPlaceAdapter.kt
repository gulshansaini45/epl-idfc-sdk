package `in`.epaylater.eplpaymentsdk.adapters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.viewholders.MarketPlaceViewHolder
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.models.responses.WhereToUseResponse
import android.content.Context
import android.support.v4.graphics.drawable.RoundedBitmapDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions


class MarketPlaceAdapter(val marketplaces: List<WhereToUseResponse.Merchants>) :
    RecyclerView.Adapter<MarketPlaceViewHolder>() {
    /*val requestOption = RequestOptions()
        .fitCenter()
        .transforms(CenterCrop(), RoundedCorners(1000))*/
    lateinit var context: Context
    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): MarketPlaceViewHolder {
        this.context = viewGroup.context
        val view =
            LayoutInflater.from(viewGroup.context)
                .inflate(`in`.epaylater.eplpaymentsdk.R.layout.layout_item_market_place, viewGroup, false)
        return MarketPlaceViewHolder(view)
    }

    override fun getItemCount(): Int {
        return marketplaces.size
    }

    override fun onBindViewHolder(viewholder: MarketPlaceViewHolder, position: Int) {
        try {
            viewholder.ivMarketPlaceIcon?.setImageResource(R.drawable.bg_rc_rect_white)
            Glide.with(context)
                .load(marketplaces.get(position).logoURL)
                .into(viewholder.ivMarketPlaceIcon);
        }catch (e:Throwable){
        }
    }

}