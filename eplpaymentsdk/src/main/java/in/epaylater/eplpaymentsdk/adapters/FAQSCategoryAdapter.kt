package `in`.epaylater.eplpaymentsdk.adapters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.viewholders.FAQSViewHolder
import `in`.epaylater.eplpaymentsdk.interfaces.EPLFAQSTopQuesListner
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class FAQSCategoryAdapter(val list: List<String>, val eplfaqsTopQuesListner: EPLFAQSTopQuesListner) : RecyclerView.Adapter<FAQSViewHolder>() {

    private lateinit var context: Context
    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): FAQSViewHolder {
        this.context = viewGroup.context
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_faqs_item_category, viewGroup, false)
        return FAQSViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: FAQSViewHolder, position: Int) {
        var title = list.get(position)
        viewHolder.tvTitle?.text = title
        viewHolder.rlContainer?.setOnClickListener(onclick(position))

    }

    private fun onclick(position: Int): View.OnClickListener? {
        return object : View.OnClickListener {
            override fun onClick(v: View?) {
                eplfaqsTopQuesListner.showTopQuestionDetails(position)
            };
        }
    }
}