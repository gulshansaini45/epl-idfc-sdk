package `in`.epaylater.eplpaymentsdk.adapters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.viewholders.PendingCollectViewHolder
import `in`.epaylater.eplpaymentsdk.extras.BigDecimalConverter
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EPLAdapterClickListner
import `in`.epaylater.eplpaymentsdk.models.PendingCollect
import `in`.epaylater.eplpaymentsdk.utils.EPLCommonUtil
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView


class PendingCollectAdapter(val list: List<PendingCollect>,val eplOnItemClickListener: EPLAdapterClickListner) : RecyclerView.Adapter<PendingCollectViewHolder>() {

    private lateinit var context: Context
    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): PendingCollectViewHolder {
        this.context = viewGroup.context
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_pending_collect_item, viewGroup, false)
        return PendingCollectViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: PendingCollectViewHolder, position: Int) {
        var pendingCollect = list.get(position)
        viewHolder.tvPayeeName?.text = pendingCollect.payeeName
        viewHolder.tvTimeAgo?.text = pendingCollect.timeAgo
        viewHolder.tvPayNow?.setOnClickListener(onclick(position))
        try {
            var pendingAmount = BigDecimalConverter.toTwoDigitPlace(pendingCollect.amount)
            val text = "${context.getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(pendingAmount)}"
            val spannableString = SpannableString(text)
            spannableString.setSpan(RelativeSizeSpan(1f), 0, 1, 0) // set size
            spannableString.setSpan(RelativeSizeSpan(1f), 1, text.length, 0) // set size
            viewHolder.tvAmount?.text = spannableString
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun onclick(position: Int): View.OnClickListener? {
        return object : View.OnClickListener {
            override fun onClick(v: View?) {
                eplOnItemClickListener.onClickItem(position)
            };
        }
    }
}