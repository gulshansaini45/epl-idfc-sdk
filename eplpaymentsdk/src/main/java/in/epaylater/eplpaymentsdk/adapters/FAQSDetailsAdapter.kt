package `in`.epaylater.eplpaymentsdk.adapters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.viewholders.FAQSDetailViewHolder
import `in`.epaylater.eplpaymentsdk.models.FAQSModel
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class FAQSDetailsAdapter(val list: ArrayList<FAQSModel>) :
    RecyclerView.Adapter<FAQSDetailViewHolder>() {

    private lateinit var context: Context
    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): FAQSDetailViewHolder {
        this.context = viewGroup.context
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_faqs_item_details, viewGroup, false)
        return FAQSDetailViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: FAQSDetailViewHolder, position: Int) {
        var faqsModel = list.get(position)
        viewHolder.tvTitle?.text = faqsModel.title
        viewHolder.tvAnswer?.text = faqsModel.answer
        viewHolder.rlContainer?.setOnClickListener(onclick(position))

        if (faqsModel.isClicked!!) {
            viewHolder.tvTitle?.setTextColor(ContextCompat.getColor(context, R.color.maroon_100))
            viewHolder.ivIcon?.setImageResource(R.drawable.ic_arrow_up)
            viewHolder.tvAnswer?.visibility = View.VISIBLE
        } else {
            viewHolder.tvTitle?.setTextColor(ContextCompat.getColor(context, R.color.grey_400))
            viewHolder.ivIcon?.setImageResource(R.drawable.ic_arrow_down)
            viewHolder.tvAnswer?.visibility = View.GONE
        }
    }

    private fun onclick(position: Int): View.OnClickListener? {
        return object : View.OnClickListener {
            override fun onClick(v: View?) {
                for (i in 0..list.size - 1) {
                    var faqsModel = list.get(i)
                    if (position == i) {
                        if(faqsModel.isClicked!!){
                            faqsModel.isClicked = false
                        }else{
                            faqsModel.isClicked = true
                        }
                    } else {
                        faqsModel.isClicked = false
                    }
                    list.set(i, faqsModel)
                }
                notifyDataSetChanged()
            };
        }
    }
}