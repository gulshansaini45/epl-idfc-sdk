package `in`.epaylater.eplpaymentsdk.adapters

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.viewholders.TxnViewHolder
import `in`.epaylater.eplpaymentsdk.extras.BigDecimalConverter
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.models.responses.UnSettledTxnResonse
import `in`.epaylater.eplpaymentsdk.utils.DateTimeUtil
import `in`.epaylater.eplpaymentsdk.utils.EPLCommonUtil
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.ViewGroup

class UnSettledTxnAdapter(val list: List<UnSettledTxnResonse.UnSettledTxnList>) : RecyclerView.Adapter<TxnViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(viewGroup: ViewGroup, itemType: Int): TxnViewHolder {
        this.context = viewGroup.context
        val view =
            LayoutInflater.from(context).inflate(R.layout.layout_item_txn_history, viewGroup, false)
        return TxnViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: TxnViewHolder, position: Int) {
        var txnRecent = list.get(position)
        viewHolder.tvMarketPlaceName?.text = txnRecent.marketplaceName
        viewHolder.tvTxndate?.text = DateTimeUtil.getDateTimeUserFriendlyFormat(txnRecent.txnDate)

        try {
            var amount = BigDecimalConverter.toTwoDigitPlace(txnRecent.netPayableAmount)
            viewHolder.tvTxnAmount?.text = "${context.getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(amount)}"
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

}