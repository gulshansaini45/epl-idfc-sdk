package `in`.epaylater.eplpaymentsdk.adapters.viewholders

import `in`.epaylater.eplpaymentsdk.R
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

class FAQSDetailViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var tvTitle = view.findViewById<TextView>(R.id.tvTitle)
    var tvAnswer = view.findViewById<TextView>(R.id.tvAnswer)
    var ivIcon = view.findViewById<ImageView>(R.id.ivIcon)
    var rlContainer = view.findViewById<RelativeLayout>(R.id.rlContainer)
}