package `in`.epaylater.eplpaymentsdk.adapters.viewholders

import `in`.epaylater.eplpaymentsdk.R
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class WhereToUseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var tvCategoryName = itemView.findViewById<TextView>(R.id.tvCategoryName)
    var rvMarketPlaceList = itemView.findViewById<RecyclerView>(R.id.rvMarketPlaceList)
}