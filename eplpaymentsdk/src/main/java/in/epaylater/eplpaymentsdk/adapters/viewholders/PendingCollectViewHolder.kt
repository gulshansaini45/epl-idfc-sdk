package `in`.epaylater.eplpaymentsdk.adapters.viewholders

import `in`.epaylater.eplpaymentsdk.R
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class PendingCollectViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var tvPayeeName = view.findViewById<TextView>(R.id.tvPayeeName)
    var tvAmount = view.findViewById<TextView>(R.id.tvAmount)
    var tvTimeAgo = view.findViewById<TextView>(R.id.tvTimeAgo)
    var tvPayNow = view.findViewById<TextView>(R.id.tvPayNow)
}