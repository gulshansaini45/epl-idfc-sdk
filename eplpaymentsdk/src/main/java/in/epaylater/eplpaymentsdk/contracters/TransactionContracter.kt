package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.responses.UnSettledTxnResonse
import `in`.epaylater.eplpaymentsdk.models.responses.SettledTxnResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter

interface TransactionContracter {
    interface View : BaseView {
        fun setSettledTransactionList(settledTxnResponse: SettledTxnResponse)
        fun setUnSettledTransactionList(unSettledTxnResonse: UnSettledTxnResonse)
        fun setEmptyTransactionList()
        fun getFragmentListner() : EpayFragmentListner
    }

    interface Presenter : BasePresenter {
        fun getUnSettledTransactionList()
        fun getSettledTransactionList()
    }
}