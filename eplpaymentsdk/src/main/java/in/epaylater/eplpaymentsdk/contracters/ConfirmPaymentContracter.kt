package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.models.requests.ConfirmCollectReq
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter

interface ConfirmPaymentContracter {

    interface View : BaseView {
        fun switchScreen(status: String)
        fun resendOTPStatus(status: Boolean)
    }

    interface Presenter : BasePresenter {
        fun approvePayment(
            confirmCollectReq: ConfirmCollectReq,
            simSerial: String,
            imeiNo: String,
            isScanAndPay: Boolean
        )

        fun declinePayment(
            confirmCollectReq: ConfirmCollectReq,
            simSerial: String,
            imeiNo: String,
            isScanAndPay: Boolean
        )

        fun resendOtp(txnId: String)
    }
}