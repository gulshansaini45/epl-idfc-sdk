package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.UPIDataModel
import `in`.epaylater.eplpaymentsdk.models.requests.SendMoneyReq
import `in`.epaylater.eplpaymentsdk.models.responses.ScanAndPayResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter

interface SendMoneyContracter {
    interface View : BaseView {
        fun switchScreen(result : ScanAndPayResponse)
        fun getFragmentListner() : EpayFragmentListner
        fun setErrorMessage(message:String)

    }

    interface Presenter : BasePresenter {
        fun proceedToPay(sendMoneyReq: SendMoneyReq, imeiNo: String, serialNo: String)
        fun validatePayData(amount : String,source: String?,upiPayload: UPIDataModel?)
        fun switchScreen(amount : String,txnId: String?,payeename: String?)
    }
}