package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse

interface BaseView {
    fun showProgressLoader()
    fun hideProgressLoader()
    fun onError(errorResponse : ErrorResponse, type: Int)
}