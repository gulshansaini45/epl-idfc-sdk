package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.responses.WhereToUseResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter

interface WhereToUseContracter {
    interface View : BaseView {
        fun showWhereToUseList(list: WhereToUseResponse)
        fun showDefaultView(value:String,showRetry:Boolean = true)
        fun getFragmentListner() : EpayFragmentListner
    }

    interface Presenter : BasePresenter {
        fun fetchWhereToUse()
    }
}