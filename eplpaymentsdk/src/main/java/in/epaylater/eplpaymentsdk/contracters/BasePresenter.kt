package `in`.epaylater.eplpaymentsdk.presenters

import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse

interface BasePresenter {
    fun onSuccess(result: Any?, type: Int)
    fun onError(errorResponse: ErrorResponse, type: Int)
}