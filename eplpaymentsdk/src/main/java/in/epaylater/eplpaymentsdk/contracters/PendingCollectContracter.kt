package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.PendingCollect
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter

interface PendingCollectContracter {

    interface View : BaseView {
        fun showPendingCollect(list: ArrayList<PendingCollect>)
        fun getFragmentListner(): EpayFragmentListner
        fun showEmptyList()
        fun updateNotificationBadge(count: Int)
    }

    interface Presenter : BasePresenter {
        fun fetchPendingCollectData(simSerialNo: String, imeiNo: String)
    }
}