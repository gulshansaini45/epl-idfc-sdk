package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.requests.PaymentReq
import `in`.epaylater.eplpaymentsdk.models.responses.PayOutstandingResponse
import `in`.epaylater.eplpaymentsdk.models.responses.PaymentMethodResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter

interface EPLPayNowContracter {
    interface View : BaseView {
        fun showPaymentMethod(response: PaymentMethodResponse)
        fun showPaymentResult(response: PayOutstandingResponse)
        fun getFragmentListner() : EpayFragmentListner
    }

    interface Presenter : BasePresenter {
        fun getPaymentMethods()
        fun payOutstandingAmount(paymentReq: PaymentReq)
    }
}