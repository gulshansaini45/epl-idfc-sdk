package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import `in`.epaylater.eplpaymentsdk.models.IDFCInitProfileModel
import `in`.epaylater.eplpaymentsdk.models.requests.UPIProfileReq
import `in`.epaylater.eplpaymentsdk.models.responses.EncMessageResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter
import android.support.v4.app.Fragment

interface DashboardContracter {
    interface View : BaseView {
        fun switchScreen()
        fun sendDeviceData()
        fun setVPAFlowStep(pageNo: Int)
        fun startSimBinding(simSerialNo: String, imeiNo: String)
        fun sendSilentSMS(encMessageResponse: EncMessageResponse)
        fun goToEPLHomeScreen()
        fun getSimDetails()
        fun setUpUpi()
        fun switchFragment(fragment: Fragment, type: Int)
        fun reAuthUPIProfile()
    }

    interface Presenter : BasePresenter {
        fun getToken(data: IDFCInitProfileModel)
        fun getUserProfileFromServer()
        fun sendDeviceData(deviceDataModel: DeviceDataModel)
        fun fetchUPIProfileFromServer()
        fun startSimBinding(simSerialNo: String, imeiNo: String)
        fun createUPIProfile(upiProfileReq: UPIProfileReq)
        fun saveUserProfile(profile: IDFCInitProfileModel)
    }
}