package `in`.epaylater.eplpaymentsdk.contracters

import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.responses.CreditSummaryResponse
import `in`.epaylater.eplpaymentsdk.presenters.BasePresenter

interface HomeContracter {

    interface View : BaseView {
        fun setCurrentStatus(creditSummaryResponse: CreditSummaryResponse)
        fun onBackPressed()
        fun getFragmentListner() : EpayFragmentListner
    }

    interface Presenter : BasePresenter {
    }
}