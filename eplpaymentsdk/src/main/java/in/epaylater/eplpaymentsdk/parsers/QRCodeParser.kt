package `in`.epaylater.eplpaymentsdk.parsers

import `in`.epaylater.eplpaymentsdk.exception.MCCIncorrectException
import `in`.epaylater.eplpaymentsdk.exception.TRNotPresentException
import `in`.epaylater.eplpaymentsdk.exception.VPAAndTRNotPresentException
import `in`.epaylater.eplpaymentsdk.exception.VPANotPresentException
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.fragments.EPLScanNPayFragment
import `in`.epaylater.eplpaymentsdk.models.UPIDataModel
import android.net.Uri

object QRCodeParser {

    private lateinit var stringToParse: String
    private var lengthToOmit: Int = 0
    private var upiTag: UPIQRTag = UPIQRTag.getEmptyTags()

    private const val PAYER_TYPE_PERSON = "0000"

    private fun findNextTagToParse(): String = stringToParse.substring(0, 2)

    private fun isValidVPA(vpa: String): Boolean = vpa.contains('@')

    private fun getTagValue(tagNo: String, stringToParse: String): String {
        val tagString = stringToParse.substringAfter(tagNo)
        if (tagString == stringToParse) {
            return ""
        }
        val length = tagString.substring(0, 2)
        val tagValue = tagString.substring(2).substring(0, length.toInt())
        EPLLogger.INSTANCE.d(tagValue)
        return tagValue
    }

    private fun parseTag(tag: String, upiQrTag: UPIQRTag) {
        when (tag) {
            "01" -> {
                upiQrTag.tag01 = getTagValue("01", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag01.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "02" -> {
                upiQrTag.tag02 = getTagValue("02", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag02.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "04" -> {
                upiQrTag.tag04 = getTagValue("04", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag04.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "06" -> {
                upiQrTag.tag06 = getTagValue("06", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag06.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "08" -> {
                upiQrTag.tag08AccIfsc = getTagValue("08", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag08AccIfsc.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "26" -> {
                upiQrTag.tag26VPA = getTagValue("26", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag26VPA.length
                stringToParse = stringToParse.substring(lengthToOmit)

                var subtag26 = upiQrTag.tag26VPA

                upiQrTag.tag26subtag00 = getTagValue("00", subtag26)
                subtag26 = upiTag.tag26VPA.substring(4 + upiQrTag.tag26subtag00.length)

                if (subtag26.isNotBlank()) {
                    upiQrTag.tag26subtag01Vpa = getTagValue("01", subtag26)
                    if (upiTag.tag26subtag01Vpa.isNotEmpty()) {
                        subtag26 = subtag26.substring(4 + upiQrTag.tag26subtag01Vpa.length)
                    }
                }

                if (subtag26.isNotBlank())
                    upiQrTag.tag26subtag02 = getTagValue("02", subtag26)
            }
            "27" -> {
                upiQrTag.tag27 = getTagValue("27", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag27.length
                stringToParse = stringToParse.substring(lengthToOmit)

                var subtag27 = upiQrTag.tag27

                upiQrTag.tag27subtag2700 = getTagValue("00", subtag27)
                subtag27 = upiTag.tag27.substring(4 + upiQrTag.tag27subtag2700.length)

                if (subtag27.isNotBlank() && subtag27.substring(0, 2).contentEquals("01")) {
                    upiQrTag.tag27subtag2701Tr = getTagValue("01", subtag27)
                    if (upiQrTag.tag27subtag2701Tr.isNotEmpty()) {
                        subtag27 = subtag27.substring(4 + upiQrTag.tag27subtag2701Tr.length)
                    }
                }

                if (subtag27.isNotBlank()) {
                    upiQrTag.tag27subtag2702 = getTagValue("02", subtag27)
                }
            }
            "28" -> {
                upiQrTag.tag28 = getTagValue("28", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag28.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "52" -> {
                upiQrTag.tag52MCC = getTagValue("52", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag52MCC.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "53" -> {
                upiQrTag.tag53 = getTagValue("53", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag53.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "54" -> {
                upiQrTag.tag54 = getTagValue("54", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag54.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "58" -> {
                upiQrTag.tag58 = getTagValue("58", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag58.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "59" -> {
                upiQrTag.tag59 = getTagValue("59", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag59.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "60" -> {
                upiQrTag.tag60 = getTagValue("60", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag60.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "61" -> {
                upiQrTag.tag61 = getTagValue("61", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag61.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            "62" -> {
                upiQrTag.tag62 = getTagValue("62", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag62.length
                stringToParse = stringToParse.substring(lengthToOmit)

                var subtag62 = upiQrTag.tag62

                if (subtag62.isNotBlank() && subtag62.substring(0, 2).contentEquals("02")) {
                    upiQrTag.tag62subtag6202 = getTagValue("02", subtag62)
                    if (upiQrTag.tag62subtag6202.isNotEmpty()) {
                        subtag62 = upiQrTag.tag62.substring(4 + upiQrTag.tag62subtag6202.length)
                    }
                }

                if (subtag62.isNotBlank() && subtag62.substring(0, 2).contentEquals("04")) {
                    upiQrTag.tag62subtag6204 = getTagValue("04", subtag62)
                    if (upiQrTag.tag62subtag6204.isNotEmpty()) {
                        subtag62 = subtag62.substring(4 + upiQrTag.tag62subtag6204.length)
                    }
                }

                if (subtag62.isNotBlank() && subtag62.substring(0, 2).contentEquals("06")) {
                    upiQrTag.tag62subtag6206 = getTagValue("06", subtag62)
                    if (upiQrTag.tag62subtag6206.isNotEmpty()) {
                        subtag62 = subtag62.substring(4 + upiQrTag.tag62subtag6206.length)
                    }
                }

                if (subtag62.isNotBlank() && subtag62.substring(0, 2).contentEquals("07")) {
                    upiQrTag.tag62subtag6207 = getTagValue("07", subtag62)
                    if (upiQrTag.tag62subtag6207.isNotEmpty()) {
                        subtag62 = subtag62.substring(4 + upiQrTag.tag62subtag6207.length)
                    }
                }

                if (subtag62.isNotBlank() && subtag62.substring(0, 2).contentEquals("08")) {
                    upiQrTag.tag62subtag6208 = getTagValue("08", subtag62)
                }
            }
            "63" -> {
                upiQrTag.tag63 = getTagValue("63", stringToParse)
                lengthToOmit = 4 + upiQrTag.tag63.length
                stringToParse = stringToParse.substring(lengthToOmit)
            }
            else -> throw IllegalArgumentException("Invalid tag $tag")
        }
    }

    private fun mapBharatQrTags(bharatQRtext: String) {
        stringToParse = bharatQRtext.substring(6)
        lengthToOmit = 0
        upiTag = UPIQRTag.getEmptyTags()

        upiTag.tag01 = getTagValue("01", stringToParse)
        lengthToOmit = 4 + upiTag.tag01.length
        stringToParse = stringToParse.substring(lengthToOmit)

        while (stringToParse.isNotEmpty() && stringToParse.length > 4) {
            parseTag(findNextTagToParse(), upiTag)
        }
    }

    fun parseBharatQRDataForBankAccount(bharatQRtext: String): UPIDataModel? {
        mapBharatQrTags(bharatQRtext)
        return if (!upiTag.tag52MCC.contentEquals(PAYER_TYPE_PERSON) && upiTag.tag27subtag2701Tr.isNotEmpty()) {
            UPIDataModel(
                upiTag.tag59,
                "${upiTag.tag08AccIfsc.substring(11, upiTag.tag08AccIfsc.length)}@${upiTag.tag08AccIfsc.substring(
                    0,
                    11
                )}",
                upiTag.tag52MCC,
                "",
                null,
                null,
                upiTag.tag54.toBigDecimalOrNull(),
                null,
                UPIDataModel.DEFAULT_CURRENCY_CODE,
                null,
                UPIDataModel.TransferType.ACC
            )
        } else if (upiTag.tag27subtag2701Tr.isEmpty() && upiTag.tag26VPA.isEmpty()) {
            throw VPAAndTRNotPresentException("Transaction declined due to VPA and TR not present")
        } else if (upiTag.tag26VPA.isEmpty()) {
            throw VPANotPresentException("Transaction declined due to no VPA")
        } else if (upiTag.tag27subtag2701Tr.isEmpty()) {
            throw TRNotPresentException("Transaction declined due to no TR")
        } else {
            null
        }
    }

    fun parseBharatQRDataForVPA(bharatQRtext: String): UPIDataModel? {
        mapBharatQrTags(bharatQRtext)
        when {
            isValidVPA(upiTag.tag26subtag01Vpa) -> {
                return UPIDataModel(
                    upiTag.tag59,
                    upiTag.tag26subtag01Vpa,
                    upiTag.tag52MCC,
                    "",
                    upiTag.tag27subtag2701Tr,
                    upiTag.tag62subtag6208,
                    upiTag.tag54.toBigDecimalOrNull(),
                    if (upiTag.tag26subtag02.isNotBlank()) upiTag.tag26subtag02.toBigDecimalOrNull() else null,
                    UPIDataModel.DEFAULT_CURRENCY_CODE,
                    upiTag.tag27subtag2702,
                    UPIDataModel.TransferType.VIR
                )
            }
            !upiTag.tag52MCC.contentEquals(PAYER_TYPE_PERSON) && upiTag.tag27subtag2701Tr.isNotBlank() -> {
                return UPIDataModel(
                    upiTag.tag59,
                    "${upiTag.tag08AccIfsc.substring(11, upiTag.tag08AccIfsc.length)}@${upiTag.tag08AccIfsc.substring(
                        0,
                        11
                    )}",
                    upiTag.tag52MCC,
                    "",
                    upiTag.tag27subtag2701Tr,
                    upiTag.tag62subtag6208,
                    upiTag.tag54.toBigDecimalOrNull(),
                    if (upiTag.tag26subtag02.isNotBlank()) upiTag.tag26subtag02.toBigDecimalOrNull() else null,
                    UPIDataModel.DEFAULT_CURRENCY_CODE,
                    upiTag.tag27subtag2702,
                    UPIDataModel.TransferType.ACC
                )
            }
            upiTag.tag52MCC.contentEquals(PAYER_TYPE_PERSON) -> {
                throw MCCIncorrectException("MCC is incorrect ${upiTag.tag52MCC}")
            }
            else -> return null
        }
    }

    fun parseUPIQrData(upiUriString: String): UPIDataModel {
        val upiUri = Uri.parse(upiUriString)
        return UPIDataModel(
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.PAYEE_NAME),
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.PAYEE_ADDRESS) ?: "",
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.MERCHANT_CODE),
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.TRANSACTION_ID),
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.TRANSACTION_REFERENCE_ID),
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.TRANSACTION_NOTE),
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.AMOUNT)?.toBigDecimalOrNull(),
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.MINIMUM_AMOUNT)?.toBigDecimalOrNull(),
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.CURRENCY_CODE)
                ?: UPIDataModel.DEFAULT_CURRENCY_CODE,
            upiUri.getQueryParameter(EPLScanNPayFragment.UpiParams.REFERENCE_URL),
            UPIDataModel.TransferType.VIR
        )
    }

    private data class UPIQRTag(
        var tag01: String,
        var tag02: String,
        var tag04: String,
        var tag06: String,
        var tag08AccIfsc: String,
        var tag26VPA: String,
        var tag26subtag00: String,
        var tag26subtag01Vpa: String,
        var tag26subtag02: String,
        var tag27: String,
        var tag27subtag2700: String,
        var tag27subtag2701Tr: String,
        var tag27subtag2702: String,
        var tag28: String,
        var tag52MCC: String,
        var tag53: String,
        var tag54: String,
        var tag58: String,
        var tag59: String,
        var tag60: String,
        var tag61: String,
        var tag62: String,
        var tag62subtag6202: String,
        var tag62subtag6204: String,
        var tag62subtag6206: String,
        var tag62subtag6207: String,
        var tag62subtag6208: String,
        var tag63: String
    ) {
        companion object {
            fun getEmptyTags(): UPIQRTag {
                return UPIQRTag(
                    "", "", "", "", "",
                    "", "", "", "",
                    "", "", "", "",
                    "", "", "", "", "", "",
                    "", "", "", "", "",
                    "", "", "", ""
                )
            }
        }
    }
}