package `in`.epaylater.eplpaymentsdk.utils

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class EPLSharedPref @Inject constructor(private val preference: SharedPreferences) {

    fun getBoolean(key: String): Boolean {
        return preference.getBoolean(key, false)
    }

    fun putBoolean(key: String, value: Boolean) {
        preference.edit().putBoolean(key, value).apply()
    }

    fun getString(key: String): String {
        val result = preference.getString(key, "") ?: ""
        return EPLEncryption.decrypt(result);
    }

    fun putString(key: String, value: String) {
        val encryptedValue = EPLEncryption.encrypt(value);
        preference.edit().putString(key, encryptedValue).apply()
    }

    fun getFloat(key: String): Float {
        return preference.getFloat(key, -1f)
    }

    fun putFloat(key: String, value: Float) {
        preference.edit().putFloat(key, value).apply()
    }

    fun getInt(key: String): Int {
        return preference.getInt(key, 0)
    }

    fun putInt(key: String, value: Int) {
        preference.edit().putInt(key, value).apply()
    }

    fun getLong(key: String): Long {
        return preference.getLong(key, -1L)
    }

    fun putLong(key: String, value: Long) {
        preference.edit().putLong(key, value).apply()
    }


    fun clear() = preference.edit().clear().apply()

    companion object {
        const val PREF_DEVICE_DATA = "user_device_data"
        const val PREF_PROFILE_COMPLETE = "user_profile_complete"
        const val PREF_PROFILE_CACHED = "user_profile_cached"
        const val PREF_SMS = "user_sms"

        const val PREF_FCM_TOKEN_UPDATED = "fcm_token_updated"
        const val PREF_IMEI = "user_phone_imei"
        const val PREF_SIM_SERIAL_NUMBER = "user_sim_serial_number"
        const val PREF_SIM_SUBSCRIBER_ID = "user_sim_subscriber_id"
        const val PREF_UPI_VPA_CREATED = "upi_vpa_created"
        const val PREF_DEVICE_ID_SYNC = "user_device_id_sync"
        const val PREF_SYNC_PUB_KEY = "sync_pub_key"
        const val PREF_SYNC = "sync"
        const val PREF_SYNC_TIME = "sync_time"
        const val PREF_DEEPLINK_ENC_PATH = "deep_link_enc_path"
        const val PREF_TOTP_KEY = "totp_key"
        const val PREF_TOTP_EXPIRE_TIME = "totp_expire_time"
        const val PREF_TOTP_REFRESH_TIME = "totp_refresh_time"
        const val PREF_UTM_SOURCE = "utm_source"
        const val PREF_UTM_MEDIUM = "utm_medium"
        const val PREF_UTM_CAMPAIGN = "utm_campaign"
        const val PREF_DEEPLINK_URI = "deep_link_uri"
        const val PREF_DMI_AADHAAR = "dmi_aadhaar"
        const val PREF_CREDIT_ENHANCEMENT_OPT_OUT = "credit_enhancement_opt_out"
        const val PREF_IS_VALID_CLIENT = "is_valid_client"
        const val PREF_SHA256 = "sha_cert"
        const val PREF_PERMISSION_UPDATED = "is_permission_updated";

        const val PREF_UPI_VPA_ID = "upi_vpa_id"

        const val PREF_USER_FIRST_NAME = "user_first_name"
        const val PREF_USER_LAST_NAME = "user_last_name"
        const val PREF_USER_MIDDLE_NAME = "user_middle_name"
        const val PREF_USER_EMAIL = "user_email"
        const val PREF_OD_ACCOUNT_NO = "user_od_acc_no"
        const val PREF_SAVING_ACCOUNT_NO = "user_saving_acc_no"
        const val PREF_SAVING_ACCOUNT_NAME = "user_saving_acc_name"
        const val PREF_NAME_LINKED_SAVING_ACC = "user_name_linked_saving_acc"
        const val PREF_MERCHANT_SDK_KEY = "merchant_sdk_key";
        const val PREF_FCM_TOKEN = "fcm_token"
        const val PREF_PENDING_APPROVAL_COUNT = "pending_approval_count"
        const val PREF_UPI_PHONE_NO = "upi_phone_no"
        const val PREF_IS_FIRST_TIME_USER = "is_first_time_user"
        const val PREF_CREDIT_LIMIT = "credit_limit"
        const val PREF_CREDIT_AVAILABLE = "credit_available"
        const val PREF_OTP_RETRY_COUNT = "otp_retry_count"
        const val PREF_IS_LAUNCHED_REPEATED_USER_LAUNCH = "is_epl_idfc_sdk_launched_repeated"

    }
}