package in.epaylater.eplpaymentsdk.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import in.epaylater.eplpaymentsdk.R;
import in.epaylater.eplpaymentsdk.constant.EPLConstants;
import in.epaylater.eplpaymentsdk.extras.EPLLogger;
import in.epaylater.eplpaymentsdk.models.IDFCInitProfileModel;
import in.epaylater.eplpaymentsdk.models.requests.BankAccountDetails;
import in.epaylater.eplpaymentsdk.models.requests.CustomerLoginReq;
import in.epaylater.eplpaymentsdk.models.requests.LocationReq;
import in.epaylater.eplpaymentsdk.models.requests.LoginReq;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.List;

public enum EPLCommonUtil {
    INSTANCE;


    public boolean objectValidator(Object obj) {
        if (null == obj || obj.equals(null))
            return false;
        if (obj instanceof String) {
            return obj.toString().trim().length() > 0 && !obj.toString().trim().equalsIgnoreCase("null");
        } else if (obj instanceof List<?>) {
            return ((List<?>) obj).size() > 0;
        }
        return true;
    }

    public LoginReq convertRequest(IDFCInitProfileModel userProfileModel) {
        LocationReq locationReq = new LocationReq(
                userProfileModel.latitude,
                userProfileModel.longitude,
                userProfileModel.accuracy);

        CustomerLoginReq customerLoginReq = new CustomerLoginReq(
                userProfileModel.custfirstName,
                userProfileModel.custlastName,
                userProfileModel.custMiddleName,
                userProfileModel.custPhoneNumber,
                userProfileModel.custEmailAddress,
                userProfileModel.custGender);

        BankAccountDetails bankAccountDetails = new BankAccountDetails(
                userProfileModel.odAccountNumber,
                userProfileModel.savingAccountNumber,
                userProfileModel.savingAccountName,
                "",
                userProfileModel.bankName,
                userProfileModel.custNameLinkedSavingAcc
        );

        return new LoginReq(customerLoginReq, locationReq, bankAccountDetails);
    }

    public boolean validateSignature(Context context, EPLSharedPref sharedPref) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature[] signs = packageInfo.signatures;
            Signature sign = signs[0];
            String result = encryptionMD5(sign.toByteArray());
            if (EPLConstants.getShaCert().contains(result)) {
                sharedPref.putString(EPLSharedPref.PREF_SHA256, result);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public String fetchSHACert(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature[] signs = packageInfo.signatures;
            Signature sign = signs[0];
            return encryptionMD5(sign.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private String encryptionMD5(byte[] byteStr) {
        MessageDigest messageDigest = null;
        StringBuffer md5StrBuff = new StringBuffer();
        try {
            messageDigest = MessageDigest.getInstance("SHA256");
            messageDigest.reset();
            messageDigest.update(byteStr);
            byte[] byteArray = messageDigest.digest();
            for (int i = 0; i < byteArray.length; i++) {
                if (Integer.toHexString(0xFF & byteArray[i]).length() == 1) {
                    md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
                } else {
                    md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return md5StrBuff.toString().toUpperCase();
    }

    public String getPermissionName(String name, Context context) {
        if (name.equals(Manifest.permission.CAMERA)) {
            return context.getString(R.string.perm_open_camera);
        } else if (name.equals(Manifest.permission.READ_PHONE_STATE)) {
            return context.getString(R.string.perm_read_device);
        }
        return "";
    }

    public boolean isConnectedToMobileNetwork(Context context) {
        try {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (objectValidator(activeNetwork)) {
                return activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE && activeNetwork.isConnected();
            }
        } catch (Exception e) {
            EPLLogger.INSTANCE.e(e);
        }
        return false;
    }

    public boolean isAirplaneModeOn(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        } catch (Exception e) {
            EPLLogger.INSTANCE.e(e);
        }
        return true;
    }

    public boolean isSimExists(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int SIM_STATE = telephonyManager.getSimState();
            if (SIM_STATE == TelephonyManager.SIM_STATE_READY)
                return true;
        } catch (Exception e) {
            EPLLogger.INSTANCE.e(e);
        }
        return false;
    }
}
