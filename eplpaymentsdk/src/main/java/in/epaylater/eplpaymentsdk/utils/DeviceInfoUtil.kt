package `in`.epaylater.eplpaymentsdk.utils

import `in`.epaylater.eplpaymentsdk.extras.EPLSDKConfig
import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import android.content.Context
import android.os.Build

class DeviceInfoUtil(
    private val context: Context,
    private val token: String,
    val simInfoUtil: SimInfoUtil
) {

    fun getDeviceInfo(): DeviceDataModel {
        return DeviceDataModel(
            fcm = token,
            make = Build.MANUFACTURER,
            model = Build.MODEL,
            osVersion = Build.VERSION.RELEASE,
            deviceId = simInfoUtil.getDeviceUniqueSerialNo(),
            simData = simInfoUtil.getSimInfo(),
            appVersion = EPLSDKConfig.EPL_SDK_VERSION
        )
    }
}