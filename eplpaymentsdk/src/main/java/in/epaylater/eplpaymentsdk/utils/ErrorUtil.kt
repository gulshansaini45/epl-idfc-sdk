package `in`.epaylater.eplpaymentsdk.utils

import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import okhttp3.Response
import java.io.IOException

object ErrorUtil {
    fun checkErrorCode(response: Response): Pair<String?, String> {
        val errorBody = response.body()
        try {
            errorBody?.let {
                val gson = Gson()
                val errorRes = gson.fromJson(errorBody.string(), ErrorResponse::class.java)
                errorRes?.let {
                    return Pair(it.eplErrorCode, it.message)
                }
            }
        } catch (e: JsonSyntaxException) {
            EPLLogger.INSTANCE.e(e)
            e.message?.let {
                return Pair(response.code().toString(), it)
            }
        } catch (e: IOException) {
            EPLLogger.INSTANCE.e(e)
            e.message?.let {
                return Pair(response.code().toString(), it)
            }
        }
        return Pair(response.code().toString(), "")
    }
}
