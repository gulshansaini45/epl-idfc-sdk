package `in`.epaylater.eplpaymentsdk.utils

import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import android.text.format.DateUtils
import android.util.Log
import java.lang.Exception
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object DateTimeUtil {
    private const val USER_FRIENDLY_FORMAT = "dd MMM yyyy"
    private const val DATE_FORMAT_IN_MILLI_SECOND = "yyyy-MM-dd'T'HH:mm:ssZ"
    private const val DATE_FORMAT_IN_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    private const val DATE_FORMAT_ISO_WITH_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    private const val DATE_FORMAT_ISO_WITH_MONTH = "MMMMM dd yyyy"


    fun getDateTimeInMilliSeconds(dateTime: String): Long {
        return SimpleDateFormat(DATE_FORMAT_IN_MILLI_SECOND, Locale.ENGLISH).parse(dateTime).time
    }

    fun getISODateInMilli(dateTime: String): Long {
        return SimpleDateFormat(DATE_FORMAT_IN_ISO, Locale.ENGLISH).parse(dateTime).time
    }

    fun getNowInMilliSeconds() = Date().time

    private fun getDateInUserFriendlyFormat(dateTime: String): String {
        val date = SimpleDateFormat(DATE_FORMAT_IN_ISO, Locale.ENGLISH).parse(dateTime)
        return SimpleDateFormat(USER_FRIENDLY_FORMAT, Locale.ENGLISH).format(date)
    }

    fun getDateFromISOString(isoDateString: String): String {
        val date = SimpleDateFormat(DATE_FORMAT_ISO_WITH_TIMEZONE, Locale.ENGLISH).parse(isoDateString)
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.time = date
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        return "$month/$year"
    }

    fun toISODateFromMonthAndYearString(month: Int, year: Int): String {
        val calender = Calendar.getInstance(Locale.ENGLISH)
        calender.set(year, month, 1)
        calender.timeZone = TimeZone.getTimeZone("UTC")
        calender.time
        return SimpleDateFormat(DATE_FORMAT_ISO_WITH_TIMEZONE, Locale.ENGLISH).format(calender.time)
    }

    fun getTimeAgo(timeStamp: String): String {
        return try {
            val date = getDateTimeInMilliSeconds(dateTime = timeStamp)
            val timeAgo = DateUtils.getRelativeTimeSpanString(
                date,
                Calendar.getInstance().timeInMillis,
                DateUtils.MINUTE_IN_MILLIS
            )
            //DateTimeUtil.getDateInUserFriendlyFormat(timeStamp)
            timeAgo.toString()
        } catch (e: ParseException) {
            timeStamp
        }
    }

    fun getDateTimeUserFriendlyFormat(dateTime: String): String {
        try {
            if (!dateTime.isNullOrBlank()) {
                if (dateTime.length > 10) {
                    var date = dateTime.substring(0, 10)
                    val initDate = SimpleDateFormat("yyyy-MM-dd").parse(date)
                    val formatter = SimpleDateFormat("MMMM dd yyyy")
                    return formatter.format(initDate)
                }
            }
        }catch (e:Exception){
            EPLLogger.INSTANCE.e(e)
        }
        return dateTime
    }
}
