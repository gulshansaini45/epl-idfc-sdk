package `in`.epaylater.eplpaymentsdk.utils

import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import `in`.epaylater.eplpaymentsdk.models.SimDataModel
import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.util.*


@Suppress("DEPRECATION")
class SimInfoUtil
constructor(private val context: Context) {

    companion object {
        const val IMEI_NOT_FOUND = "imei-not-found"
        /*  fun getDeviceUniqueSerialNo(): String {
            return Build.SERIAL
          }*/


    }

    private val simDataList: ArrayList<SimDataModel> = ArrayList(2)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    fun isDualSim(): Boolean {
        val activeSubscriptionInfoCountMax = SubscriptionManager.from(context).activeSubscriptionInfoCountMax
        return activeSubscriptionInfoCountMax > 1
    }

    @SuppressLint("HardwareIds")
    private fun checkMultiSim() {
        val permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1 && isDualSim()) {
                simDataList.add(SimDataModel(simSlot = 0))
                simDataList.add(SimDataModel(simSlot = 1))
            } else {
                simDataList.add(SimDataModel(simSlot = 0))
            }
        } else {
            showPermissionScreen()
        }
    }

    fun getSimInfo(): List<DeviceDataModel.SimInfo> {
        getSimInfoDetails()
        return simDataList
            .asSequence()
            .filter { it.imei.isNotBlank() && it.simSerial.isNotBlank() }
            .map { DeviceDataModel.SimInfo(it.imei, it.simSerial, it.simSlot, it.simName,it.simSubId.toString()) }
            .toList()
    }

    @SuppressLint("HardwareIds")
    fun getSimInfoDetails(): List<SimDataModel> {
        checkMultiSim()
        val androidId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        val permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                simDataList[0].imei = androidId+"-"+0
                getSimDetails(simDataList[0],0)
                if (isDualSim()) {
                    simDataList[1].imei =  androidId+"-"+1
                    getSimDetails(simDataList[1],1)
                }
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                simDataList[0].imei = tm.getDeviceId(0) ?: androidId+"-"+0
                getSimDetails(simDataList[0],0)
                if (isDualSim()) {
                    simDataList[1].imei = tm.getDeviceId(1) ?: androidId+"-"+1
                    getSimDetails(simDataList[1],1)
                }
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                simDataList[0].imei = tm.deviceId ?: ""
                getSimDetails(simDataList[0],0)
                if (isDualSim()) {
                    var tmpImei = getDeviceIdBySlot("getDeviceIdDs", 1)
                    if (tmpImei.contentEquals(IMEI_NOT_FOUND)) {
                        tmpImei = getDeviceIdBySlot("getDeviceIdGemini", 1)
                    }
                    if (!tmpImei.contentEquals(IMEI_NOT_FOUND)) {
                        simDataList[1].imei = tmpImei
                        getSimDetails(simDataList[1],1)
                    } else {
                        try {
                            simDataList.removeAt(1)
                        } catch (e: IndexOutOfBoundsException) {
                            e.printStackTrace()
                        }
                    }
                }
            } else {
                simDataList[0].imei = tm.deviceId ?:androidId+"-"+0
                simDataList[0].phoneNo = tm.line1Number
                simDataList[0].simName = tm.simOperatorName
                simDataList[0].simSerial = tm.simSerialNumber ?: ""
                simDataList[0].simSubId = tm.subscriberId ?: ""
            }
        } else {
            showPermissionScreen()
        }
        return simDataList
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    private fun getSimDetails(simData: SimDataModel,slotID: Int) {
        val activeSubscriptionInfoList = SubscriptionManager.from(context).activeSubscriptionInfoList
        activeSubscriptionInfoList?.let { subInfo ->
            subInfo.asSequence()
                .filter { it.simSlotIndex == simData.simSlot }
                .forEach { subscriptionInfo ->
                    subscriptionInfo?.let {
                        simData.simSerial = it.iccId ?: ""
                        simData.phoneNo = it.number
                        simData.simName = it.displayName.toString()
                        simData.simSubId = it.subscriptionId.toString()
                    }
                }
        }
    }

    private fun getDeviceIdBySlot(predictedMethodName: String, slotID: Int): String {
        var imei = "null"
        val telephony = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        try {
            val telephonyClass = Class.forName(telephony.javaClass.name)
            val parameter = arrayOfNulls<Class<*>>(1)
            parameter[0] = Int::class.javaPrimitiveType
            val getSimID = telephonyClass.getMethod(predictedMethodName, *parameter)
            val obParameter = arrayOfNulls<Any>(1)
            obParameter[0] = slotID
            val obPhone = getSimID.invoke(telephony, obParameter)
            if (obPhone != null) {
                imei = obPhone.toString()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return IMEI_NOT_FOUND
        }
        return imei
    }

    private fun showPermissionScreen() {

    }

    fun getDeviceUniqueSerialNo(): String {
        var c: Class<*>? = null
        try {
            c = Class.forName("android.os.SystemProperties")
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
        var get: Method? = null
        try {
            get = c?.getMethod("get", String::class.java, String::class.java)
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        }
        var deviceId = ""
        try {
            deviceId = get?.invoke(c, "sys.serialnumber", "Error") as String
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
        if (deviceId == "Error") {
            try {
                deviceId = get?.invoke(c, "ril.serialnumber", "Error") as String
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
            }
        }
        if (deviceId == "Error") {
            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                    val androidId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
                    deviceId = androidId
                } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    deviceId = Build.getSerial()
                } else {
                    deviceId = Build.SERIAL
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                val androidId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
                deviceId = androidId
            }

        }
        return deviceId
        //return "42009202d005a28d";
    }
}
