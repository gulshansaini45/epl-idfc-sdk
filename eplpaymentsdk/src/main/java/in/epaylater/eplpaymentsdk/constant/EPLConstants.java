package in.epaylater.eplpaymentsdk.constant;

import java.util.ArrayList;
import java.util.List;

public class EPLConstants {

    public interface KEYS {
        String USER_DATA = "user_data";
        String CONTAINER_KEY = "container_key";
        String EXTRA_REBIND = "extra_rebind";
        String EXTRA_FAQS_QUESTIONS_ANS = "extra_faqs_questions_ans";
        String EXTRA_FAQS_QUESTION = "extra_faqs_question";
        String EXTRA_OUTSTANDING_AMOUNT= "extra_outstanding_amount";
        String SUCCESS = "success";
    }

    public static List<String> getShaCert() {
        List<String> shaCert = new ArrayList<>();
        shaCert.add("17CBD1B9D21D88CE40F59E3B3515F97A2BC3BD70AA92CD8508B731E1A9FE7B91");
        shaCert.add("167770970CD4272F76D2FA4D524732ECEA8845C9C8DF9CDD82F16CE4F432A827");
        shaCert.add("31B9D3618EFD699CC2D62A038C3C800A83F7B6615C56E51C2D0C6E3CD961E059");
        return shaCert;
    }

    public interface SCREEN_NAME {
        String EPL_AUTH_FRAGMENT = "epl_auth";
        String EPL_HOME_FRAGMENT = "epl_home";
        String EPL_DASHBOARD = "epl_dashboard";
        String EPL_WHERE_TO_USE = "epl_where_to_use";
        String EPL_FAQS = "epl_faqs";
        String EPL_SCAN_N_PAY = "epl_scan_n_pay";
        String EPL_SEND_MONEY = "epl_send_money";
        String EPL_CONFIRM_PAYMENT= "epl_confirm_payment";
        String EPL_PAYMENT_STATUS= "epl_payment_status";
        String EPL_PENDING_COLLECT = "epl_pending_collect";
        String EPL_PAYNOW = "epl_pay_now";
        String EPL_RECENT_TRANSACTION = "Recent Transactions";
        String EPL_SETTLED_TRANSACTION = "Settled Transactions";
        String EPL_FAQS_DETAIL = "epl_faqs_details";
        String EPL_CONTACT_US = "epl_contact_us";

    }

    public interface PERMISSION_REQUESTCODES {
        int ACCESS_FINE_LOCATION = 100;
        int CAMERA = 101;
        int READ_PHONE_STATE = 102;
        int GO_TO_SETTING_LOCATION = 103;
        int PHONE_CALL = 104;
        int SMS = 105;
    }

    public interface TYPES {
        int DEFAULT = 0;
        int TYPE_LOGIN_API = 1;
        int TYPE_DEVICE_API = 2;
        int TYPE_USER_PROFILE_API = 3;
        int TYPE_UPI_PROFILE_API = 4;
        int TYPE_ENCRYPT_MESSAGE_API = 5;
        int TYPE_CREAT_UPI_PROFILE_API = 6;
        int TYPE_SCAN_N_PAY_API = 7;
        int TYPE_CONFIRM_PAYMENT_APPROVE_API = 8;
        int TYPE_RESEND_OTP = 9;
        int TYPE_PENDING_COLLECT_APPROVAL_API = 10;
        int TYPE_PENDING_COLLECT_CONFIRM_API = 11;
        int TYPE_CREDIT_SUMMARY_API = 12;
        int TYPE_UNSETTLED_TXN = 13;
        int TYPE_SETTLED_TXN = 14;
        int TYPE_PAYMENT_METHOD = 15;
        int TYPE_PAY_QUTSTANDING_AMOUNT = 16;
        int TYPE_CONFIRM_PAYMENT_DECLINE_API = 17;
        int TYPE_PROMOTION_API = 18;
        int TYPE_SWITCH_FRAGMENT =51;
        int TYPE_REAUTH_UPI = 52;
        int TYPE_SESSION_EXPIRE = 53;

    }

    public interface REQUEST_CODE {
        int REQUEST_CODE_PERMISSION = 1000;
        int EPL_REQUEST_SEND_EMAIL = 1001;
        int EPL_REQUEST_SETTING = 1002;
        int EPL_REQUEST_SEND_SMS = 1003;
    }
    public interface REGULAR_EXPRESS{
        String regxNumberFormat = "^[0-9]*$";
        String regxAmount = "^\\d+(\\.\\d{1,2})?$";
    }
}
