package `in`.epaylater.eplpaymentsdk.interceptor

import `in`.epaylater.eplpaymentsdk.extras.EPLSDKConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class HeaderInterceptor(val merchantKey: String, val sha256: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalUrl = originalRequest.url().url().toString()
        val newRequestBuilder = originalRequest.newBuilder()

        //when {
        if (!(originalUrl.endsWith(EPLSDKConfig.EPL_USER_PROFILE)
                    || originalUrl.endsWith(EPLSDKConfig.EPL_DEVICE))) {
            newRequestBuilder.addHeader("Content-Type", "application/json")
            newRequestBuilder.addHeader("X-Merchant-Key", merchantKey)
        }

        if (originalUrl.endsWith(EPLSDKConfig.EPL_LOGIN)) {
            newRequestBuilder.addHeader("X-Merchant-App-Fingerprint",sha256)
        }
        //}
        return chain.proceed(newRequestBuilder.build())
    }
}
