package `in`.epaylater.eplpaymentsdk.interceptor

import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.extras.EPLSDKConfig
import `in`.epaylater.eplpaymentsdk.utils.ErrorUtil
import android.support.annotation.StringDef
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ServerErrorInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalResponse = chain.proceed(originalRequest)
        val contentTypeHeader = originalResponse.header(HEADER_CONTENT_TYPE)

        try {
            if (originalResponse.code() == 401) throw ServerException.UserNotAuthorized("Session expired")

            if (originalRequest.url().url().toString().startsWith(EPLSDKConfig.EPL_BASE_URL) && contentTypeHeader != null && contentTypeHeader.equals(
                    APPLICATION_JSON,
                    ignoreCase = true
                )
            ) {
                if (originalResponse.code() in 400..599) {
                    val code = ErrorUtil.checkErrorCode(originalResponse)
                    code.let {
                        if (code.first == null) throw ServerException.DefaultErrorResponse(it.second)
                        when (code.first) {
                            UPI_INVALID_MOBILE_APP_VERSION -> throw ServerException.UPIInvalidMobileAppVersionException(
                                it.second
                            )
                            UPI_MOBILE_APP_VERSION_NOT_SUPPORTED -> throw ServerException.UPIMobileAppVersionNotSupportedException(
                                it.second
                            )
                            UPI_SIM_USED_IS_NOT_SAME -> throw ServerException.UPISimUsedIsNotSameException(it.second)
                            UPI_PROFILE_ALREADY_EXIST -> throw ServerException.UPIProfileAlreadyExistException(it.second)
                            UPI_PROFILE_NOT_FOUND -> throw ServerException.UPIProfileNotFoundException(it.second)
                            UPI_SIM_REBINDING_REQUIRED -> throw ServerException.UPISimBindingRequiredException(it.second)
                            UPI_SIM_REBINDING_NOT_REQUIRED -> throw ServerException.UPISimRebindingNotRequiredException(
                                it.second
                            )
                            UPI_FAILED_TO_CREATE_UPI_PROFILE -> throw ServerException.UPIFailedToCreateUpiProfileException(
                                it.second
                            )
                            UPI_APPROVAL_IS_NOT_PENDING -> throw ServerException.UPIApprovalIsNotPendingException(it.second)
                            UPI_INVALID_OTP_FOR_TRANSACTION -> throw ServerException.UPIInvalidOtpForTransactionException(
                                it.second
                            )
                            UPI_FAILED_TO_APPROVE_UPI_TRANSACTION -> throw ServerException.UPIFailedToApproveUpiTransactionException(
                                it.second
                            )
                            UPI_TRANSACTION_EXPIRED -> throw ServerException.UPITransactionExpired(it.second)
                            UPI_TRANSACTION_IN_PROGRESS -> throw ServerException.UPITransactionInProgress(it.second)
                            USER_NOT_SIGNED_UP -> throw ServerException.UserNotSignedUp(it.second)
                            USER_NOT_FOUND_FOR_MOBILE -> throw ServerException.UserNotAuthorized(it.second)
                            ICICI_CUSTOMER_LOGIN_DISABLE -> throw ServerException.ICICICustomerLoginDisable(it.second)
                            else -> throw ServerException.DefaultErrorResponse(it.second)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        return originalResponse
    }

    companion object {
        private const val HEADER_CONTENT_TYPE = "Content-Type"
        private const val APPLICATION_JSON = "application/json"

        @StringDef(
            USER_NOT_SIGNED_UP,
            USER_NOT_FOUND_FOR_MOBILE,
            UPI_INVALID_MOBILE_APP_VERSION,
            UPI_MOBILE_APP_VERSION_NOT_SUPPORTED,
            UPI_SIM_USED_IS_NOT_SAME,
            UPI_PROFILE_ALREADY_EXIST,
            UPI_PROFILE_NOT_FOUND,
            UPI_SIM_REBINDING_REQUIRED,
            UPI_SIM_REBINDING_NOT_REQUIRED,
            UPI_FAILED_TO_CREATE_UPI_PROFILE,
            UPI_APPROVAL_IS_NOT_PENDING,
            UPI_INVALID_OTP_FOR_TRANSACTION,
            UPI_FAILED_TO_APPROVE_UPI_TRANSACTION,
            UPI_TRANSACTION_IN_PROGRESS
        )
        @Retention(AnnotationRetention.SOURCE)
        internal annotation class Error

        const val USER_NOT_SIGNED_UP = "EPL2000"
        const val ICICI_CUSTOMER_LOGIN_DISABLE = "EPL2002"
        const val USER_NOT_FOUND_FOR_MOBILE = "EPL1204"
        const val UPI_INVALID_MOBILE_APP_VERSION = "EPL6001"
        const val UPI_MOBILE_APP_VERSION_NOT_SUPPORTED = "EPL6002"
        const val UPI_SIM_USED_IS_NOT_SAME = "EPL6003"
        const val UPI_PROFILE_ALREADY_EXIST = "EPL6004"
        const val UPI_PROFILE_NOT_FOUND = "EPL6005"
        const val UPI_SIM_REBINDING_REQUIRED = "EPL6006"
        const val UPI_SIM_REBINDING_NOT_REQUIRED = "EPL6007"
        const val UPI_FAILED_TO_CREATE_UPI_PROFILE = "EPL6008"
        const val UPI_APPROVAL_IS_NOT_PENDING = "EPL6009"
        const val UPI_INVALID_OTP_FOR_TRANSACTION = "EPL6010"
        const val UPI_FAILED_TO_APPROVE_UPI_TRANSACTION = "EPL6011"
        const val UPI_TRANSACTION_EXPIRED = "EPL6012"
        const val UPI_TRANSACTION_IN_PROGRESS = "EPL6023"
        const val DEFAULT_ERROR_CODE = "EPL0000"
    }
}
