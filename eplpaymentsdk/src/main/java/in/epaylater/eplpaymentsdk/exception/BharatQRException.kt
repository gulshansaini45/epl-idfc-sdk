package `in`.epaylater.eplpaymentsdk.exception

sealed class BharatQRException(message: String) : RuntimeException(message)
class MCCIncorrectException(message: String) : BharatQRException(message)
class VPANotPresentException(message: String) : BharatQRException(message)
class TRNotPresentException(message: String) : BharatQRException(message)
class VPAAndTRNotPresentException(message: String) : BharatQRException(message)
