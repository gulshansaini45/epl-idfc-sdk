package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.FAQSCategoryAdapter
import `in`.epaylater.eplpaymentsdk.adapters.FAQSDetailsAdapter
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.extras.FAQSData
import `in`.epaylater.eplpaymentsdk.interfaces.EPLFAQSTopQuesListner
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_faqs_questions.*

class EPLFAQSQuestionsFragment : EPLBaseFragment(), EPLFAQSTopQuesListner {

    private lateinit var epayFragmentListner: EpayFragmentListner
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_faqs_questions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            var faqsTopQuestion = FAQSData.getTopQuestionData()
            if (faqsTopQuestion.isNotEmpty()) {
                var faqOtherQuestionAdapter = FAQSDetailsAdapter(faqsTopQuestion)
                rlFaqsTopQuestions?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
                rlFaqsTopQuestions?.hasFixedSize()
                rlFaqsTopQuestions?.adapter = faqOtherQuestionAdapter
            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }

        try {
            var faqsCategoryList = FAQSData.getCategoryList()
            if (faqsCategoryList.isNotEmpty()) {
                var faqsCategoryAdapter = FAQSCategoryAdapter(faqsCategoryList, this)
                rlCategoryList?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
                rlCategoryList?.hasFixedSize()
                rlCategoryList?.adapter = faqsCategoryAdapter
            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }


    override fun showTopQuestionDetails(position: Int) {
        var faqsTopQuestion = FAQSData.getCategoryList()
        when {
            position == 0 -> {
                var data = FAQSData.getFirstCategoryData()
                var bundle = Bundle()
                bundle.putParcelableArrayList(EPLConstants.KEYS.EXTRA_FAQS_QUESTIONS_ANS, data)
                bundle.putString(EPLConstants.KEYS.EXTRA_FAQS_QUESTION, faqsTopQuestion.get(0))
                var fragment = EPLFaqsDetailFragment()
                fragment.arguments = bundle
                epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
            }
            position == 1 -> {
                var data = FAQSData.getSecondCategoryData()
                var bundle = Bundle()
                bundle.putParcelableArrayList(EPLConstants.KEYS.EXTRA_FAQS_QUESTIONS_ANS, data)
                bundle.putString(EPLConstants.KEYS.EXTRA_FAQS_QUESTION, faqsTopQuestion.get(1))
                var fragment = EPLFaqsDetailFragment()
                fragment.arguments = bundle
                epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
            }
            position == 2 -> {
                var data = FAQSData.getThirdCategoryData()
                var bundle = Bundle()
                bundle.putParcelableArrayList(EPLConstants.KEYS.EXTRA_FAQS_QUESTIONS_ANS, data)
                bundle.putString(EPLConstants.KEYS.EXTRA_FAQS_QUESTION, faqsTopQuestion.get(2))
                var fragment = EPLFaqsDetailFragment()
                fragment.arguments = bundle
                epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
            }
            position == 3 -> {
                var data = FAQSData.getFourthCategoryData()
                var bundle = Bundle()
                bundle.putParcelableArrayList(EPLConstants.KEYS.EXTRA_FAQS_QUESTIONS_ANS, data)
                bundle.putString(EPLConstants.KEYS.EXTRA_FAQS_QUESTION, faqsTopQuestion.get(3))
                var fragment = EPLFaqsDetailFragment()
                fragment.arguments = bundle
                epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
            }
        }
    }
}