package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.WhereToUseAdapter
import `in`.epaylater.eplpaymentsdk.contracters.WhereToUseContracter
import `in`.epaylater.eplpaymentsdk.extras.CustomProgressDialog
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.WhereToUseResponse
import `in`.epaylater.eplpaymentsdk.presenters.WhereToUsePresenter
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_pending_collect.*
import kotlinx.android.synthetic.main.fragment_where_to_use.*
import kotlinx.android.synthetic.main.layout_empty_default_view.*

class EPLWhereToUseFragment : EPLBaseFragment(), WhereToUseContracter.View, View.OnClickListener {

    private lateinit var epayFragmentListner: EpayFragmentListner
    private lateinit var presenter: WhereToUsePresenter
    private lateinit var whereToUseAdapter: WhereToUseAdapter
    private lateinit var customProgressDialog: CustomProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = WhereToUsePresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_where_to_use, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btnRetry?.setOnClickListener(this)
        fetchWhereToUse()
    }

    private fun fetchWhereToUse() {
        layoutEmptyPendingHistory?.visibility = View.GONE
        showProgressLoader()
        presenter.fetchWhereToUse()
    }

    override fun showWhereToUseList(list: WhereToUseResponse) {
        hideProgressLoader()
        whereToUseAdapter = WhereToUseAdapter(context!!, list)
        rvWhereToUse.layoutManager =
            LinearLayoutManager(context, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager
        rvWhereToUse.adapter = whereToUseAdapter
        llWhereToUse?.visibility = View.VISIBLE
        layoutEmpty?.visibility = View.GONE
    }

    override fun showDefaultView(value: String, showRetry: Boolean) {
        hideProgressLoader()
        llWhereToUse?.visibility = View.GONE
        layoutEmpty?.visibility = View.VISIBLE
        tvEmptyText?.text = value
        if (showRetry) btnRetry?.visibility = View.VISIBLE else btnRetry?.visibility = View.GONE
    }

    override fun showProgressLoader() {
        customProgressDialog = CustomProgressDialog.show(context)
    }

    override fun hideProgressLoader() {
        CustomProgressDialog.dismiss(customProgressDialog)
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        hideProgressLoader()
        try {
            llWhereToUse?.visibility = View.GONE
            layoutEmpty?.visibility = View.VISIBLE
            tvEmptyText?.text = errorResponse.message
            btnRetry?.visibility = View.VISIBLE
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        /*   try {
               if (!errorResponse.eplErrorCode.isNullOrEmpty()
                   && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
               ) {
                   showToast(errorResponse.message)
                   epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
               } else if (errorResponse.code == 401) {
                   epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
               } else {
                   try {
                       llWhereToUse?.visibility = View.GONE
                       layoutEmpty?.visibility = View.VISIBLE
                       tvEmptyText?.text = errorResponse.message
                       btnRetry?.visibility = View.VISIBLE
                   } catch (e: java.lang.Exception) {
                       EPLLogger.INSTANCE.e(e)
                   }
               }
           } catch (e: java.lang.Exception) {
               EPLLogger.INSTANCE.e(e)
           }*/
    }

    override fun getFragmentListner(): EpayFragmentListner {
        return epayFragmentListner
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnRetry -> {
                fetchWhereToUse()
            }
        }
    }
}