package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.interfaces.PermissionCheckListener
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_contact_us.*


class EPLContactUsFragment : EPLBaseFragment(), View.OnClickListener {

    private lateinit var epayFragmentListner: EpayFragmentListner
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_us, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tvCallCenterNo?.setOnClickListener(this)
        tvEmailId?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvCallCenterNo -> {
                callNow()
            }
            R.id.tvEmailId -> {
                emailNow()
            }
        }
    }

    private fun emailNow() {
        try {
            var emailId = tvEmailId.text.toString()
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.putExtra(android.content.Intent.EXTRA_EMAIL, arrayOf<String>(emailId))
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "")
            intent.putExtra(android.content.Intent.EXTRA_TEXT, "")
            intent.setData(Uri.parse("mailto:"));

            context?.let {
                if (intent.resolveActivity(it.getPackageManager()) != null) {
                    startActivityForResult(
                        Intent.createChooser(intent, "Send email"),
                        EPLConstants.REQUEST_CODE.EPL_REQUEST_SEND_EMAIL
                    )
                } else {
                    // If there are no email client installed in this device
                    Toast.makeText(it, "No email client installed in this device.", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (e: java.lang.Exception) {
            Toast.makeText(context, "No email client!", Toast.LENGTH_LONG).show()
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun callNow() {
        try {
            checkPermissions(object : PermissionCheckListener {
                override fun onPermissionGranted(isPermissionGranted: Boolean) {
                    if (isPermissionGranted) {
                        makeCall()
                    } else {
                        context?.let {
                            Toast.makeText(it, getString(R.string.permission_denied), Toast.LENGTH_LONG).show()
                        }

                    }
                }
            }, true, EPLConstants.PERMISSION_REQUESTCODES.PHONE_CALL)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun makeCall() {
        var no = tvCallCenterNo?.text!!.trim().toString()
        var dialNo = no.replace("\\s", "");
        var intent = Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + dialNo));
        context?.let {
            if (intent.resolveActivity(it.getPackageManager()) != null) {
                startActivity(intent);
            } else {
                // If there are no phone client installed in this device
                Toast.makeText(it, "No phone client installed in this device.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == EPLConstants.REQUEST_CODE.REQUEST_CODE_PERMISSION) {
            if (permissions.get(0).equals(android.Manifest.permission.CALL_PHONE)) {
                makeCall()
            }
        }
    }

}