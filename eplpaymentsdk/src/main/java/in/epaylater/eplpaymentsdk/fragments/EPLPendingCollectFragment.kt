package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.PendingCollectAdapter
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.PendingCollectContracter
import `in`.epaylater.eplpaymentsdk.extras.CustomProgressDialog
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EPLAdapterClickListner
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.PendingCollect
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.presenters.PendingCollectPresenter
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_pending_collect.*
import kotlinx.android.synthetic.main.layout_empty_default_view.*

class EPLPendingCollectFragment : EPLBaseFragment(), EPLAdapterClickListner, PendingCollectContracter.View,
    View.OnClickListener {

    private var payeeVPAId: String? = ""
    private var payeeAmount: Int? = 0
    private var transactionId: String? = ""
    private lateinit var epayFragmentListner: EpayFragmentListner
    private lateinit var pendingCollectAdapter: PendingCollectAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    var listPendingCollect = arrayListOf<PendingCollect>()
    private lateinit var presenter: PendingCollectPresenter
    private lateinit var eplDashboardFragment: EPLDashboardFragment
    private lateinit var customProgressDialog : CustomProgressDialog

    companion object {
        public const val EXTRA_PENDING_COLLECT = "extra_pending_collect"
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = PendingCollectPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pending_collect, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btnRetry?.setOnClickListener(this)
        tvVPAId?.text = sharedPref.getString(EPLSharedPref.PREF_UPI_VPA_ID)
        fetchPendingCollect()
    }

    private fun fetchPendingCollect() {
        layoutEmptyPendingHistory?.visibility = View.GONE
        var simSerial = sharedPref.getString(EPLSharedPref.PREF_SIM_SERIAL_NUMBER)
        var imeiNo = sharedPref.getString(EPLSharedPref.PREF_IMEI)
        showProgressLoader()
        presenter.fetchPendingCollectData(simSerialNo = simSerial, imeiNo = imeiNo)
    }

    override fun showPendingCollect(list: ArrayList<PendingCollect>) {
        hideProgressLoader()
        try {
            listPendingCollect = list
            pendingCollectAdapter = PendingCollectAdapter(listPendingCollect, this)
            rvPendingCollect.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            rvPendingCollect.adapter = pendingCollectAdapter
            rlPendingCollect?.visibility = View.VISIBLE
            layoutEmptyPendingHistory?.visibility = View.GONE
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun showEmptyList() {
        hideProgressLoader()
        rlPendingCollect?.visibility = View.GONE
        layoutEmptyPendingHistory?.visibility = View.VISIBLE
        tvEmptyText?.text = getString(R.string.empty_pending_collect)
        btnRetry?.visibility = View.VISIBLE
    }

    override fun updateNotificationBadge(count: Int) {
        try {
            sharedPref.putInt(EPLSharedPref.PREF_PENDING_APPROVAL_COUNT, count)
            eplDashboardFragment = parentFragment as EPLDashboardFragment
            eplDashboardFragment.let { it.addBadgeView() }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun showProgressLoader() {
        customProgressDialog = CustomProgressDialog.show(context)
    }

    override fun hideProgressLoader() {
        CustomProgressDialog.dismiss(customProgressDialog)
    }


    override fun onError(errorResponse: ErrorResponse, type: Int) {
        hideProgressLoader()
        try {
            rlPendingCollect?.visibility = View.GONE
            layoutEmptyPendingHistory?.visibility = View.VISIBLE
            tvEmptyText?.text = errorResponse.message
            btnRetry?.visibility = View.VISIBLE
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onClickItem(position: Int) {
        try {
            var bundle = Bundle()
            bundle.putString(EPLConfirmPaymentFragment.TRANSACTION_ID, listPendingCollect.get(position).txnId)
            bundle.putString(
                EPLConfirmPaymentFragment.TRANSACTION_AMOUNT,
                listPendingCollect.get(position).amount.toString()
            )
            bundle.putString(EPLConfirmPaymentFragment.PAYEE_NAME, listPendingCollect.get(position).payeeName)
            bundle.putString(EPLConfirmPaymentFragment.TIME_AGO, listPendingCollect.get(position).timeAgo)
            bundle.putString(EPLConfirmPaymentFragment.SCREEN_NAME, getString(R.string.title_upi_payments))
            bundle.putBoolean(EPLConfirmPaymentFragment.IS_SCAN_N_PAY_SCREEN, false)
            var fragment = EPLConfirmPaymentFragment()
            fragment.arguments = bundle
            epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun getFragmentListner(): EpayFragmentListner {
        return epayFragmentListner
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnRetry -> {
                fetchPendingCollect()
            }
        }
    }

}