package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.SettledTxnAdapter
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.TransactionContracter
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.SettledTxnResponse
import `in`.epaylater.eplpaymentsdk.models.responses.UnSettledTxnResonse
import `in`.epaylater.eplpaymentsdk.presenters.TxnPresenter
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_txn_history.*
import kotlinx.android.synthetic.main.layout_empty_default_view.*

class EPLSettledTxnFragment : EPLBaseFragment(), TransactionContracter.View, View.OnClickListener {

    private lateinit var epayFragmentListner: EpayFragmentListner
    lateinit var presenter: TxnPresenter
    private var settledTransactionList: MutableList<SettledTxnResponse.SettledTxnList> = ArrayList()
    private val adapter = SettledTxnAdapter(settledTransactionList)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
        presenter = TxnPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_txn_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnRetry?.setOnClickListener(this)
        try {
            rvTxn?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            rvTxn?.hasFixedSize()
            rvTxn?.adapter = adapter
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fetchSettledTxn()
    }

    fun fetchSettledTxn() {
        layoutEmptyTxnHistory?.visibility = View.GONE
        showProgressLoader()
        presenter.getSettledTransactionList()
    }

    override fun setSettledTransactionList(settledTxnResponse: SettledTxnResponse) {
        hideProgressLoader()
        layoutEmptyTxnHistory?.visibility = View.GONE
        try {
            settledTransactionList.clear()
            settledTransactionList.addAll(settledTxnResponse.settledTransactionList)
            adapter.notifyDataSetChanged()
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun setEmptyTransactionList() {
        hideProgressLoader()
        layoutEmptyTxnHistory?.visibility = View.VISIBLE
        tvEmptyText?.text = getString(R.string.txt_empty_transaction_history)
        btnRetry?.visibility = View.GONE
    }

    override fun setUnSettledTransactionList(unSettledTxnResonse: UnSettledTxnResonse) {
        hideProgressLoader()
    }

    override fun showProgressLoader() {
        progressBarLayout?.visibility = View.VISIBLE
    }

    override fun hideProgressLoader() {
        progressBarLayout?.visibility = View.GONE
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        hideProgressLoader()
        try {
            val message = handleNetworkError(ServerException.DefaultErrorResponse(errorResponse.message))
            context?.let {
                layoutEmptyTxnHistory?.visibility = View.VISIBLE
                tvEmptyText?.text = message
                btnRetry?.visibility = View.VISIBLE
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        /*try {
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                showToast(errorResponse.message)
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
            } else if (errorResponse.code == 401) {
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
            } else {
                try {
                    val message = handleNetworkError(ServerException.DefaultErrorResponse(errorResponse.message))
                    context?.let {
                        layoutEmptyTxnHistory?.visibility = View.VISIBLE
                        tvEmptyText?.text = message
                        btnRetry?.visibility = View.VISIBLE
                    }
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }*/
    }

    override fun getFragmentListner(): EpayFragmentListner {
        return epayFragmentListner
    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnRetry -> {
                fetchSettledTxn()
            }
        }
    }
}