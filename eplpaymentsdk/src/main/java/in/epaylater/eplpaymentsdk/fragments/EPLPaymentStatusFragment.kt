package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.TxnViewpagerAdapter
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.extras.BigDecimalConverter
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_payment_status.*
import kotlinx.android.synthetic.main.layout_available_credit_details.*
import kotlinx.android.synthetic.main.layout_transaction_bottom.*

class EPLPaymentStatusFragment : EPLBaseFragment(), View.OnClickListener {

    private var transactionId: String? = ""
    private var transactionAmount: String? = ""
    private var transactionStatus: String? = ""
    private var payeeName: String? = ""
    private lateinit var epayFragmentListner: EpayFragmentListner
    private lateinit var eplDashboardFragment: EPLDashboardFragment
    private var txnAdapter: TxnViewpagerAdapter? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var bundle = arguments
        bundle?.let {
            transactionAmount = it.getString(EPLConfirmPaymentFragment.TRANSACTION_AMOUNT)
            transactionId = it.getString(EPLConfirmPaymentFragment.TRANSACTION_ID)
            transactionStatus = it.getString(EPLConfirmPaymentFragment.TRANSACTION_STATUS)
            payeeName = it.getString(EPLConfirmPaymentFragment.PAYEE_NAME)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_payment_status, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rlTransactionHistory?.setOnClickListener(this)
        setStatus()
        updateNotificationCount()
        showCreditDetails()
    }

    private fun showCreditDetails() {

        val creditLimit = sharedPref.getString(EPLSharedPref.PREF_CREDIT_LIMIT)
        transactionAmount?.let {
            val creditAvailable = sharedPref.getString(EPLSharedPref.PREF_CREDIT_AVAILABLE)
            var totalCreditAvailable: Double
            if (transactionStatus!!.equals(EPLConfirmPaymentFragment.TRANSACTION_APPROVED)) {
                totalCreditAvailable =
                    (creditAvailable.toDouble()) - it.toDouble()
            } else {
                totalCreditAvailable =
                    creditAvailable.toDouble()
            }
            sharedPref.putString(EPLSharedPref.PREF_CREDIT_AVAILABLE, totalCreditAvailable.toString())

            var finalCreditLimit = BigDecimalConverter.toTwoDigitPlace(creditLimit.toBigDecimal())
            var finalCreditAvail = BigDecimalConverter.toTwoDigitPlace(totalCreditAvailable.toBigDecimal())

            tvCreditDetailAvlAmount?.text =
                "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(finalCreditAvail)}"

            tvCreditDetailLimitAmount?.text =
                "${getString(`in`.epaylater.eplpaymentsdk.R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(
                    finalCreditLimit
                )}"

            try {
                var percent =
                    ((finalCreditAvail / finalCreditLimit).toDouble() * 100).toInt()
                pbCreditDetailStatus?.progress = percent
            }catch (e:Exception){
                EPLLogger.INSTANCE.e(e)
            }
        }
    }


    private fun setStatus() {
        tvPayeeName?.text = payeeName
        tvTxnId?.text = "Txn Id: $transactionId"
        if (transactionStatus!!.equals(EPLConfirmPaymentFragment.TRANSACTION_APPROVED)) {
            ivIcon?.setImageResource(R.drawable.ic_transaction_success)
            tvPaymentStatus?.text = getString(R.string.payment_status_success)
        } else  if (transactionStatus!!.equals(EPLConfirmPaymentFragment.TRANSACTION_CANCELLED)) {
            ivIcon?.setImageResource(R.drawable.ic_transaction_failed)
            tvPaymentStatus?.text = getString(R.string.payment_status_cancelled)
        }else {
            ivIcon?.setImageResource(R.drawable.ic_transaction_failed)
            tvPaymentStatus?.text = getString(R.string.payment_status_failed)
        }


        try {
            transactionAmount?.let {
                var amount = BigDecimalConverter.toTwoDigitPlace(it.toBigDecimal())
                val text = "${getString(R.string.indian_rupee)} ${BigDecimalConverter.numberFormatterUS(amount)}"
                tvPaidAmount?.text = text
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun updateNotificationCount() {
        try {
            eplDashboardFragment = parentFragment as EPLDashboardFragment
            val pendingCount = sharedPref.getInt(EPLSharedPref.PREF_PENDING_APPROVAL_COUNT)
            if (pendingCount > 0) {
                sharedPref.putInt(EPLSharedPref.PREF_PENDING_APPROVAL_COUNT, pendingCount - 1)
            }
            eplDashboardFragment.let { it.addBadgeView() }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun openCloseTransactionHistory() {
        if (viewTabLayout?.visibility == View.VISIBLE) {
            viewTabLayout?.visibility = View.GONE
            viewpagerTransactions?.visibility = View.GONE
            ivUpArrow?.setImageResource(R.drawable.ic_arrow_up)
            txnAdapter?.popAllFragment()
        } else {
            context?.let {
                viewTabLayout?.visibility = View.VISIBLE
                viewpagerTransactions?.visibility = View.VISIBLE
                ivUpArrow?.setImageResource(R.drawable.ic_arrow_down)
                viewpagerTransactions.isSaveEnabled = false
                setupViewPager()
            }
        }
    }

    private fun setupViewPager() {
        try {
            txnAdapter = TxnViewpagerAdapter(childFragmentManager)
            txnAdapter?.let {
                var transactionRecentFragment = EPLUnSettledTxnFragment()
                var transactionSettledFragment = EPLSettledTxnFragment()

                it.addFragment(transactionRecentFragment, EPLConstants.SCREEN_NAME.EPL_RECENT_TRANSACTION)
                it.addFragment(transactionSettledFragment, EPLConstants.SCREEN_NAME.EPL_SETTLED_TRANSACTION)

                viewpagerTransactions?.adapter = it
                tabsTransaction?.setupWithViewPager(viewpagerTransactions)
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        setTabDividerLine()
    }

    private fun setTabDividerLine() {
        try {
            val root = tabsTransaction.getChildAt(0)
            if (root is LinearLayout) {
                context?.let {
                    (root).showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
                    val drawable = GradientDrawable()
                    drawable.setColor(ContextCompat.getColor(it, R.color.grey_400))
                    drawable.setSize(1, 1)
                    (root).dividerPadding = 20
                    (root).dividerDrawable = drawable
                }

            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlTransactionHistory -> {
                openCloseTransactionHistory()
            }
        }
    }
}