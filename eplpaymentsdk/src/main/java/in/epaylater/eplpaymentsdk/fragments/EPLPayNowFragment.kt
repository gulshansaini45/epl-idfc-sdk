package `in`.epaylater.eplpaymentsdk.fragments


import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.EPLPayNowContracter
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.BigDecimalConverter
import `in`.epaylater.eplpaymentsdk.extras.CustomProgressDialog
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.requests.PaymentReq
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.PayOutstandingResponse
import `in`.epaylater.eplpaymentsdk.models.responses.PaymentMethodResponse
import `in`.epaylater.eplpaymentsdk.presenters.EPLPayNowPresenter
import `in`.epaylater.eplpaymentsdk.utils.EPLCommonUtil
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_pay_now.*
import java.math.BigDecimal

class EPLPayNowFragment : EPLBaseFragment(), EPLPayNowContracter.View, View.OnClickListener {

    private lateinit var epayFragmentListner: EpayFragmentListner
    private lateinit var presenter: EPLPayNowPresenter
    private lateinit var paymentMethodResponse: PaymentMethodResponse
    private var snakBar: Snackbar? = null
    private var amountOutstanding: Double? = 0.0
    private lateinit var customProgressDialog : CustomProgressDialog


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = EPLPayNowPresenter(this)
        var bundle = arguments
        bundle?.let {
            amountOutstanding = it.getDouble(EPLConstants.KEYS.EXTRA_OUTSTANDING_AMOUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pay_now, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnPayNow?.setOnClickListener(this)
        amountOutstanding?.let {
            var displayAmount = BigDecimalConverter.toTwoDigitPlace(it.toBigDecimal())
            tvPayAmount?.text = "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(displayAmount)}"
            btnPayNow?.text = "${getString(R.string.pay_amount)} ${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(displayAmount)}"

            if (displayAmount > BigDecimal.ZERO) {
                btnPayNow.isEnabled = true
                btnPayNow.setBackgroundResource(R.drawable.bg_rc_rect_solid_maroon)
            } else {
                btnPayNow.isEnabled = false
                btnPayNow.setBackgroundResource(R.drawable.bg_rc_rect_solid_grey)
            }
        }

        getPaymentMethods()
    }


    override fun showPaymentResult(response: PayOutstandingResponse) {
        hideProgressLoader()
        context?.let {
            if (response.status.equals(EPLConstants.KEYS.SUCCESS)) {
                var amountAvailable = sharedPref.getString(EPLSharedPref.PREF_CREDIT_AVAILABLE)
                amountOutstanding?.let {
                    var totalAvailable = amountAvailable + it.toFloat()
                    sharedPref.putString(EPLSharedPref.PREF_CREDIT_AVAILABLE, totalAvailable)
                }

            }
            showDialog(response.msg, it)
            //showSnakBar(response.msg, it)
        }
    }

    private fun getPaymentMethods() {
        showProgressLoader()
        presenter.getPaymentMethods()

    }

    override fun showPaymentMethod(response: PaymentMethodResponse) {
        hideProgressLoader()
        if (!response.methods.isNullOrEmpty()) {
            rlPaymentMode?.visibility = View.VISIBLE
            btnPayNow?.visibility = View.VISIBLE
            this.paymentMethodResponse = response
            tvPaymentOptionType?.text = response.methods.get(0).method
            tvPaymentOptionDetail?.text = response.methods.get(0).description
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnPayNow -> {
                payNowValidation()
            }
        }
    }

    private fun payNowValidation() {
        if (!radioButton.isChecked) {
            context?.let {
                showSnakBar(getString(R.string.select_payment_option), it)
            }
        } else {
            try {
                amountOutstanding?.let {
                    var paymentReq = PaymentReq(it.toDouble(), paymentMethodResponse.methods.get(0).methodCode)
                    showProgressLoader()
                    presenter.payOutstandingAmount(paymentReq = paymentReq)
                }
            } catch (e: Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }

    override fun showProgressLoader() {
        customProgressDialog = CustomProgressDialog.show(context)
    }

    override fun hideProgressLoader() {
        CustomProgressDialog.dismiss(customProgressDialog)
    }



    override fun onError(errorResponse: ErrorResponse, type: Int) {
        hideProgressLoader()
        try {
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                showToast(errorResponse.message)
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
            } else if (errorResponse.code == 401) {
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
            } else {
                val message = handleNetworkError(ServerException.DefaultErrorResponse(errorResponse.message))
                context?.let { showSnakBar(message, it) }
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }


    private fun showSnakBar(message: String, context: Context) {
        try {
            view?.let {
                snakBar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                snakBar?.let {
                    it.view.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_400))
                    it.view.setPadding(0, 20, 0, 20)
                    it.show()
                }
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun showDialog(message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.layout_permission_dialog, null)

        val tvTitle = dialogView.findViewById<TextView>(R.id.tvTitle)
        val tvmessage = dialogView.findViewById<TextView>(R.id.tvMessage)

        tvTitle.setText("Payment")
        tvmessage.setText(message)
        builder.setPositiveButton(getString(R.string.ok)) { dialog, which ->
            dialog.cancel()
            epayFragmentListner.changeEplFragment(EPLHomeFragment(), EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
        }

        builder.setView(dialogView)
        builder.setCancelable(false)
        builder.show()
    }

    override fun getFragmentListner(): EpayFragmentListner {
        return epayFragmentListner
    }
}