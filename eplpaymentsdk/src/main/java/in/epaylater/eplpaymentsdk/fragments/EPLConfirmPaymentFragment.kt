package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.ConfirmPaymentContracter
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.BigDecimalConverter
import `in`.epaylater.eplpaymentsdk.extras.CustomProgressDialog
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.interfaces.TextChangeListner
import `in`.epaylater.eplpaymentsdk.models.requests.ConfirmCollectReq
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.presenters.ConfirmPaymentPresenter
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.*
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_confirm_payment.*
import java.util.concurrent.TimeUnit


class EPLConfirmPaymentFragment : EPLBaseFragment(), View.OnClickListener, TextChangeListner,
    ConfirmPaymentContracter.View {

    var transactionId: String? = ""
    var transactionAmount: String? = ""
    var payeeName: String? = ""
    var timeAgo: String? = ""
    var screenName: String? = ""
    var isScanAndPayScreen: Boolean = false
    lateinit var countDownTimer: CountDownTimer
    private lateinit var epayFragmentListner: EpayFragmentListner
    private val FORMAT = "%02dsec"
    private var snakBar: Snackbar? = null
    lateinit var presenter: ConfirmPaymentPresenter
    private lateinit var customProgressDialog: CustomProgressDialog

    companion object {
        public val TRANSACTION_ID = "txnId"
        public const val TRANSACTION_AMOUNT = "txnAmount"
        public const val PAYEE_NAME = "payeeName"
        public const val TIME_AGO = "timeAgo"
        public const val TRANSACTION_STATUS = "status"
        public const val TRANSACTION_APPROVED = "Approve"
        public const val TRANSACTION_DECLINED = "Declined"
        public const val TRANSACTION_CANCELLED = "Cancelled"
        public const val SCREEN_NAME = "screen_name"
        public const val IS_SCAN_N_PAY_SCREEN = "isScanAndPayScreen"
        public const val REMARKS = "remarks"
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
        presenter = ConfirmPaymentPresenter(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var bundle = arguments
        bundle?.let {
            transactionAmount = it.getString(TRANSACTION_AMOUNT)
            transactionId = it.getString(TRANSACTION_ID)
            payeeName = it.getString(PAYEE_NAME)
            timeAgo = it.getString(TIME_AGO)
            screenName = it.getString(SCREEN_NAME)
            isScanAndPayScreen = it.getBoolean(IS_SCAN_N_PAY_SCREEN)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_confirm_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnDecline?.setOnClickListener(this)
        btnApprove?.setOnClickListener(this)
        tvResendOTP?.setOnClickListener(this)
        rootContainer?.setOnClickListener(this)
       // etOTP?.disableCopyPaste()
        sharedPref.putInt(EPLSharedPref.PREF_OTP_RETRY_COUNT, 0)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        screenName?.let {
            activity?.title = it
        }
        payeeName?.let {
            tvPayeeName?.text = it
        }
        tvTimeAgo?.text = timeAgo
        try {
            var amount = BigDecimalConverter.toTwoDigitPlace(transactionAmount?.toBigDecimal())
            val text = "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(amount)}"
            val spannableString = SpannableString(text)
            spannableString.setSpan(RelativeSizeSpan(1.5f), 0, 1, 0) // set size
            spannableString.setSpan(RelativeSizeSpan(2f), 1, text.length, 0) // set size
            tvTransAmount?.text = spannableString
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }

        startTimer()
    }

    private fun stopTimer() {
        try {
            tvResendOTP?.visibility = View.VISIBLE
            tvOTPValidTime?.visibility = View.GONE
            tvOTPValidTitle?.visibility = View.GONE
            countDownTimer.cancel()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onStop() {
        super.onStop()
        onTextChange()
    }

    override fun onTextChange() {
        etOTP?.setError(null)
    }

    override fun onClick(v: View?) {
        hideKeyboard()
        when (v?.id) {
            R.id.btnDecline -> {
                declinePayment()
            }
            R.id.btnApprove -> {
                approvePayment()
            }
            R.id.tvResendOTP -> {
                transactionId?.let {
                    showProgressLoader()
                    presenter.resendOtp(it)
                }
            }
            R.id.rootContainer->{
                etOTP?.clearFocus()
                hideKeyboard()
            }
        }
    }

    private fun declinePayment() {
        showProgressLoader()
        var otp = etOTP?.text.toString()
        stopTimer()
        tvResendOTP?.visibility = View.VISIBLE
        try {
            transactionId?.let {
                var confirmCollectReq = ConfirmCollectReq(txnId = it, userAction = "DECLINE", otp = otp)
                var simSerial = sharedPref.getString(EPLSharedPref.PREF_SIM_SERIAL_NUMBER)
                var simIMEI = sharedPref.getString(EPLSharedPref.PREF_IMEI)
                presenter.declinePayment(
                    confirmCollectReq = confirmCollectReq,
                    imeiNo = simIMEI,
                    simSerial = simSerial,
                    isScanAndPay = isScanAndPayScreen
                )
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun approvePayment() {
        var otp = etOTP?.text.toString()
        if (otp.isNullOrEmpty()) {
            etOTP?.setError(getString(R.string.enter_otp))
            etOTP?.requestFocus()
        } else {
            showProgressLoader()
            stopTimer()
            tvResendOTP?.visibility = View.VISIBLE
            try {
                transactionId?.let {
                    var confirmCollectReq = ConfirmCollectReq(txnId = it, userAction = "APPROVED", otp = otp)
                    var simSerial = sharedPref.getString(EPLSharedPref.PREF_SIM_SERIAL_NUMBER)
                    var simIMEI = sharedPref.getString(EPLSharedPref.PREF_IMEI)
                    presenter.approvePayment(
                        confirmCollectReq = confirmCollectReq,
                        imeiNo = simIMEI,
                        simSerial = simSerial,
                        isScanAndPay = isScanAndPayScreen
                    )
                }
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }

    private fun startTimer() {
        val time = 30000
        tvOTPValidTitle?.visibility = View.VISIBLE
        tvOTPValidTime?.visibility = View.VISIBLE
        tvResendOTP?.visibility = View.GONE
        try {
            countDownTimer = object : CountDownTimer(time.toLong(), 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    tvOTPValidTime?.setText(
                        "" + String.format(
                            FORMAT,
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                            )
                        )
                    );
                }

                override fun onFinish() {
                    stopTimer()
                }
            }.start()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }


    override fun switchScreen(status: String) {
        hideProgressLoader()
        try {
            var bundle = Bundle()
            bundle.putString(EPLConfirmPaymentFragment.TRANSACTION_ID, transactionId)
            bundle.putString(EPLConfirmPaymentFragment.TRANSACTION_AMOUNT, transactionAmount)
            bundle.putString(EPLConfirmPaymentFragment.PAYEE_NAME, payeeName)
            bundle.putString(EPLConfirmPaymentFragment.TRANSACTION_STATUS, status)
            var fragment = EPLPaymentStatusFragment()
            fragment.arguments = bundle
            epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun showProgressLoader() {
        customProgressDialog = CustomProgressDialog.show(context)
    }

    override fun hideProgressLoader() {
        CustomProgressDialog.dismiss(customProgressDialog)
    }


    override fun resendOTPStatus(status: Boolean) {
        hideProgressLoader()
        startTimer()
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        hideProgressLoader()
        try {
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                showToast(errorResponse.message)
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
            } else if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_INVALID_OTP_FOR_TRANSACTION)
            ) {
                var count = getRetryCount()
                showToast(errorResponse.message)
                if (count >= 2) {
                    //switchScreen(EPLConfirmPaymentFragment.TRANSACTION_DECLINED)
                    declinePayment()
                }
                setRetryCount()
            } else if (errorResponse.code == 401) {
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
            } else {
                val message = handleNetworkError(ServerException.DefaultErrorResponse(errorResponse.message))
                context?.let { showSnakBar(message, it) }
                switchScreen(EPLConfirmPaymentFragment.TRANSACTION_DECLINED)
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun showSnakBar(message: String, context: Context) {
        try {
            view?.let {
                snakBar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                snakBar?.let {
                    it.view.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_400))
                    it.show()
                }
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopTimer()
    }

    fun setRetryCount() {
        try {
            var count = getRetryCount() + 1
            if (count >= 3) {
                count = 0
            }
            sharedPref.putInt(EPLSharedPref.PREF_OTP_RETRY_COUNT, count)

        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    fun getRetryCount(): Int {
        try {
            return sharedPref.getInt(EPLSharedPref.PREF_OTP_RETRY_COUNT)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        return 2
    }
    fun TextView.disableCopyPaste() {
        isLongClickable = false
        setTextIsSelectable(false)
        customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu): Boolean {
                return false
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem): Boolean {
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode?) {}
        }
    }
}