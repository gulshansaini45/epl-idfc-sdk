package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.di.modules.EPLAppModule
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.LiveNetworkInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.interfaces.EpayLaterService
import `in`.epaylater.eplpaymentsdk.interfaces.PermissionCheckListener
import `in`.epaylater.eplpaymentsdk.managers.EPLManager
import `in`.epaylater.eplpaymentsdk.utils.EPLCommonUtil
import `in`.epaylater.eplpaymentsdk.utils.EPLEncryption
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import java.net.UnknownHostException
import java.util.*
import javax.inject.Inject

open class EPLBaseFragment : Fragment() {

    @Inject
    lateinit var sharedPref: EPLSharedPref
    @Inject
    lateinit var epayLaterService: EpayLaterService
    private lateinit var epayFragmentListner: EpayFragmentListner

    private var permissionCheckListener: PermissionCheckListener? = null
    protected var isAPPInForground: Boolean = true
    private val TYPE_HAMBURGER_ICON = 0
    private val TYPE_BACK_BUTTON = 1
    private val TYPE_KEYBOARD_ADJUST_PAN = 1

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
        try {
            EPLManager.eplAppComponent.inject(this)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
            reInitAppComponent()
            EPLManager.eplAppComponent.inject(this)
        }

    }

    override fun onResume() {
        super.onResume()
        isAPPInForground = true
        setTitle(this)
    }

    protected fun setTitle(currentFragment: Fragment) {
        try {
            when {
                currentFragment is EPLContactUsFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_contact_us),
                        headerViewType = TYPE_BACK_BUTTON
                    )
                }
                currentFragment is EPLFaqsDetailFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_faqs),
                        headerViewType = TYPE_BACK_BUTTON
                    )
                }
                currentFragment is EPLPayNowFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_make_payment),
                        headerViewType = TYPE_BACK_BUTTON
                    )
                }
                currentFragment is EPLPendingCollectFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_upi_payments),
                        headerViewType = TYPE_BACK_BUTTON
                    )
                }
                currentFragment is EPLConfirmPaymentFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_upi_payments),
                        headerViewType = TYPE_BACK_BUTTON,
                        keyboardAdjustType = TYPE_KEYBOARD_ADJUST_PAN
                    )
                }
                currentFragment is EPLScanNPayFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_scan_n_pay),
                        headerViewType = TYPE_BACK_BUTTON
                    )
                }
                currentFragment is EPLSendMoneyFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_scan_n_pay),
                        headerViewType = TYPE_BACK_BUTTON,
                        keyboardAdjustType = TYPE_KEYBOARD_ADJUST_PAN
                    )
                }
                currentFragment is EPLHomeFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = "",
                        headerViewType = TYPE_HAMBURGER_ICON
                    )
                }
                currentFragment is EPLWhereToUseFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = "",
                        headerViewType = TYPE_HAMBURGER_ICON
                    )
                }
                currentFragment is EPLPaymentStatusFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = "",
                        headerViewType = TYPE_HAMBURGER_ICON
                    )
                }
                currentFragment is EPLDashboardFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = "",
                        headerViewType = TYPE_HAMBURGER_ICON
                    )
                }
                currentFragment is EPLFAQSQuestionsFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = getString(R.string.title_faqs),
                        headerViewType = TYPE_HAMBURGER_ICON
                    )
                }
                currentFragment is EPLUnSettledTxnFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = "",
                        headerViewType = TYPE_HAMBURGER_ICON
                    )
                }
                currentFragment is EPLSettledTxnFragment -> {
                    epayFragmentListner.setActionTitle(
                        title = "",
                        headerViewType = TYPE_HAMBURGER_ICON
                    )
                }
                else -> {
                    epayFragmentListner.setActionTitle(title = "", headerViewType = TYPE_BACK_BUTTON)
                }
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onStop() {
        super.onStop()
        isAPPInForground = false
    }

    fun Fragment.hideKeyboard() {
        try {
            view?.let { activity?.hideKeyboard(it) }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    fun Context.hideKeyboard(view: View) {
        try {
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    protected fun replaceFragment(fragment: Fragment) {
        var result = popAllFragment(fragment)
        try {
            if (result) {
                childFragmentManager.beginTransaction()
                    .replace(R.id.eplContainer, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit()
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }


    protected fun replaceFragment(fragment: Fragment, tag: String) {
        var result = popAllFragment(fragment)
        try {
            if (result) {
                childFragmentManager.beginTransaction()
                    .replace(R.id.eplContainer, fragment, tag)
                    .addToBackStack(tag)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit()
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    protected fun addFragment(fragment: Fragment) {
        try {
            childFragmentManager.beginTransaction()
                .add(R.id.eplContainer, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    protected fun addFragment(fragment: Fragment, tag: String) {
        try {
            childFragmentManager.beginTransaction()
                .add(R.id.eplContainer, fragment, tag)
                .addToBackStack(tag)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    fun popAllFragment(fragment: Fragment): Boolean {
        if (fragment is EPLHomeFragment) {
            try {
                while (childFragmentManager.backStackEntryCount > 0) {
                    childFragmentManager.popBackStackImmediate()
                }
            } catch (e: Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
        return true
    }

    fun popTopFragment(fragment: Fragment) {
        if (fragment is EPLDashboardFragment) {
            try {
                fragment.getChildFragmentManager().popBackStackImmediate()
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }

    fun popAllTopFragment(fragment: Fragment) {
        if (fragment is EPLDashboardFragment) {
            try {
                while (fragment.getChildFragmentManager().backStackEntryCount > 0) {
                    fragment.getChildFragmentManager().popBackStackImmediate()
                }
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }

    fun popAllBackStackFragment(fragment: Fragment) {
        if (fragment is EPLDashboardFragment) {
            try {
                while (fragment.getChildFragmentManager().backStackEntryCount > 1) {
                    fragment.getChildFragmentManager().popBackStackImmediate()
                }
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }

    fun popAllTopFragmentWithList(fragment: Fragment): Boolean {
        if (fragment is EPLDashboardFragment) {
            try {
                while (fragment.getChildFragmentManager().backStackEntryCount > 0) {
                    fragment.getChildFragmentManager()
                        .popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                }
                fragment.getChildFragmentManager().let {
                    it.beginTransaction().remove(it.findFragmentById(R.id.eplContainer)!!).commit();
                }
                return true
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
        return false
    }

    protected fun checkPermissions(
        permissionCheckListener: PermissionCheckListener,
        isShowDialog: Boolean,
        vararg requestCode: Int
    ) {
        this.permissionCheckListener = permissionCheckListener
        val listPermissions = ArrayList<String>()

        for (i in requestCode.indices) {
            when (requestCode[i]) {
                EPLConstants.PERMISSION_REQUESTCODES.ACCESS_FINE_LOCATION -> isPermissionNeeded(
                    listPermissions,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                EPLConstants.PERMISSION_REQUESTCODES.CAMERA -> isPermissionNeeded(
                    listPermissions,
                    Manifest.permission.CAMERA
                )
                EPLConstants.PERMISSION_REQUESTCODES.READ_PHONE_STATE -> isPermissionNeeded(
                    listPermissions,
                    Manifest.permission.READ_PHONE_STATE
                )
                EPLConstants.PERMISSION_REQUESTCODES.PHONE_CALL -> isPermissionNeeded(
                    listPermissions,
                    Manifest.permission.CALL_PHONE
                )
                /*EPLConstants.PERMISSION_REQUESTCODES.SMS -> isPermissionNeeded(
                    listPermissions,
                    Manifest.permission.SEND_SMS
                )*/
                else -> {
                }
            }
        }
        startPermissionDialog(listPermissions, isShowDialog)
    }

    private fun isPermissionNeeded(permissionsList: MutableList<String>, permission: String) {
        context?.let {
            if (ContextCompat.checkSelfPermission(it, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
            }
        }
    }

    private fun startPermissionDialog(permissionsList: List<String>, isShowDialog: Boolean) {
        if (permissionsList.isEmpty()) {
            permissionCheckListener?.let {
                it.onPermissionGranted(true)
            }
        } else if (isShowDialog) {
            context?.let {
                try {
                    showPermissionInfo(permissionsList, false, it)
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            }
            return
        } else {
            ActivityCompat.requestPermissions(
                context as Activity,
                permissionsList.toTypedArray(), EPLConstants.REQUEST_CODE.REQUEST_CODE_PERMISSION
            )
        }
    }


    private fun showPermissionInfo(permissionsList: List<String>, isShowSetting: Boolean, context: Context) {
        val builder = AlertDialog.Builder(context)
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.layout_permission_dialog, null)

        val tvTitle = dialogView.findViewById<TextView>(R.id.tvTitle)
        val tvmessage = dialogView.findViewById<TextView>(R.id.tvMessage)
        tvTitle.text = getString(R.string.permission_required)

        if (isShowSetting) {
            var permission = ""
            for (i in permissionsList.indices) {
                permission = permission + EPLCommonUtil.INSTANCE.getPermissionName(permissionsList[i], context)
            }
            tvmessage.text = getString(R.string.def_perm_req_txt, permission)
            builder.setPositiveButton(getString(R.string.settings)) { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                if (EPLCommonUtil.INSTANCE.objectValidator(context.getPackageName())) {
                    val uri = Uri.fromParts("package", context.getPackageName(), null)
                    intent.data = uri
                    (context as Activity).startActivityForResult(intent, EPLConstants.REQUEST_CODE.EPL_REQUEST_SETTING)
                    dialog.cancel()
                }
            }
            builder.setNegativeButton(
                getString(R.string.cancel)
            ) { dialog, which ->
                dialog.cancel()
                (context as Activity).onBackPressed()
            }
        } else {
            tvmessage.text = Html.fromHtml(getAlertBoxTitle(permissionsList))
            builder.setPositiveButton(getString(R.string.allow)) { dialog, which ->
                ActivityCompat.requestPermissions(
                    context as Activity,
                    permissionsList.toTypedArray(),
                    EPLConstants.REQUEST_CODE.REQUEST_CODE_PERMISSION
                )
                dialog.cancel()
            }
            builder.setNegativeButton(
                getString(R.string.cancel)
            ) { dialog, which ->
                dialog.cancel()
                (context as Activity).onBackPressed()
            }
        }
        builder.setView(dialogView)
        builder.setCancelable(false)
        builder.show()
    }

    private fun getAlertBoxTitle(permissionsList: List<String>): String {
        var title = StringBuilder()
        for (i in permissionsList.indices) {
            when (permissionsList[i]) {
                Manifest.permission.ACCESS_FINE_LOCATION -> {
                    if (title.length != 0) {
                        title = title.append("<br><br>")
                    }
                    title = title.append("<b>").append(getString(R.string.perm_location))
                        .append("</b><br><font color='#54565b'>").append(getString(R.string.perm_location_msg))
                        .append("</font>")
                }

                Manifest.permission.CAMERA -> {
                    if (title.length != 0) {
                        title = title.append("<br><br>")
                    }
                    title = title.append("<b>").append(getString(R.string.perm_open_camera))
                        .append("</b><br><font color='#54565b'>").append("</b>")
                        .append(getString(R.string.perm_camera_msg))
                        .append("</font>")
                }
                Manifest.permission.READ_PHONE_STATE -> {
                    if (title.length != 0) {
                        title = title.append("<br><br>")
                    }
                    title = title.append("<b>").append(getString(R.string.perm_read_device))
                        .append("</b><br><font color='#54565b'>").append("</b>")
                        .append(getString(R.string.perm_phone_msg))
                        .append("</font>")
                }
                Manifest.permission.CALL_PHONE -> {
                    if (title.length != 0) {
                        title = title.append("<br><br>")
                    }
                    title = title.append("<b>").append(getString(R.string.perm_read_device))
                        .append("</b><br><font color='#54565b'>").append("</b>")
                        .append(getString(R.string.perm_call_msg))
                        .append("</font>")
                }
                Manifest.permission.SEND_SMS -> {
                    if (title.length != 0) {
                        title = title.append("<br><br>")
                    }
                    title = title.append("<b>").append(getString(R.string.perm_receive_sms))
                        .append("</b><br><font color='#54565b'>").append("</b>")
                        .append(getString(R.string.perm_sms_msg))
                        .append("</font>")
                }
                else -> {
                }
            }
        }

        return /*getString(R.string.permission_sub_title) +"<br><br>" + */ title.toString().trim { it <= ' ' }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        try {
            checkPermissionGranted(requestCode, permissions, grantResults)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        try {
            trackPermissionResponce(permissions, grantResults)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun checkPermissionGranted(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (permissionCheckListener != null && EPLCommonUtil.INSTANCE.objectValidator(permissions) && EPLCommonUtil.INSTANCE.objectValidator(
                grantResults
            )
        ) {
            when (requestCode) {
                EPLConstants.REQUEST_CODE.REQUEST_CODE_PERMISSION -> {
                    var i: Int
                    try {
                        i = 0
                        while (i < permissions.size) {
                            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                                if (!ActivityCompat.shouldShowRequestPermissionRationale(
                                        context as Activity,
                                        permissions[i]
                                    )
                                ) {
                                    context?.let {
                                        try {
                                            showPermissionInfo(Arrays.asList(permissions[i]), true, it)
                                        } catch (e: java.lang.Exception) {
                                            EPLLogger.INSTANCE.e(e)
                                        }
                                    }
                                } else {
                                    permissionCheckListener?.let {
                                        it.onPermissionGranted(false)
                                    }
                                }
                                break
                            }
                            i++
                        }
                        if (permissions.size > 0 && i == permissions.size) {
                            permissionCheckListener?.let {
                                it.onPermissionGranted(true)
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

                else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    private fun trackPermissionResponce(permission: Array<String>, result: IntArray) {
        for (i in permission.indices) {
            sharedPref.putString(permission[i], if (result[i] == PackageManager.PERMISSION_GRANTED) "Yes" else "No")
            sharedPref.putBoolean(EPLSharedPref.PREF_PERMISSION_UPDATED, true)
        }
    }

    protected fun handleNetworkError(t: Throwable): String {
        if (t is LiveNetworkInterceptor.NoNetworkException) {
            return getString(R.string.error_no_internet)
        } else if (t is ServerException.UserNotAuthorized) {
            Toast.makeText(context, getString(R.string.text_session_expired), Toast.LENGTH_SHORT).show()
        } else if (t is ServerException.IDFCErrorResponse) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPIProfileNotFoundException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPIInvalidMobileAppVersionException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPISimUsedIsNotSameException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPIProfileAlreadyExistException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPISimBindingRequiredException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPISimRebindingNotRequiredException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPIFailedToCreateUpiProfileException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPIApprovalIsNotPendingException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPIInvalidOtpForTransactionException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.UPIFailedToApproveUpiTransactionException) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is ServerException.DefaultErrorResponse) {
            return t.message?.let { it } ?: getString(R.string.error_default_network)
        } else if (t is UnknownHostException) {
            return getString(R.string.error_no_internet)
        }
        return getString(R.string.error_default_network)
    }

    protected fun showToast(message: String) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun reInitAppComponent() {
        try {
            context?.let {
                var shared =
                    it.applicationContext.getSharedPreferences(EPLAppModule.PREF_FILE_NAME, Context.MODE_PRIVATE)
                var merchantSDK = shared.getString(EPLSharedPref.PREF_MERCHANT_SDK_KEY, "")
                val decryptedmerchantKey = EPLEncryption.decrypt(merchantSDK);
                var shaCert = EPLCommonUtil.INSTANCE.fetchSHACert(it.applicationContext)
                EPLManager.initAppComponent(it, decryptedmerchantKey!!, shaCert)
            }
        }catch (e:Exception){
            context?.let {
                ( it as AppCompatActivity).onBackPressed()
            }
        }
    }
}