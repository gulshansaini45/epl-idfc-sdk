package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.SendMoneyContracter
import `in`.epaylater.eplpaymentsdk.customviews.CustomTextWatcher
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.CustomProgressDialog
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.interfaces.TextChangeListner
import `in`.epaylater.eplpaymentsdk.models.UPIDataModel
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ScanAndPayResponse
import `in`.epaylater.eplpaymentsdk.presenters.SendMoneyPresenter
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import kotlinx.android.synthetic.main.fragment_send_money.*
import android.support.v4.widget.TextViewCompat.setCustomSelectionActionModeCallback
import android.view.*
import android.widget.TextView


class EPLSendMoneyFragment : EPLBaseFragment(), SendMoneyContracter.View, View.OnClickListener, TextChangeListner {
    private lateinit var epayFragmentListner: EpayFragmentListner
    var upiPayload: UPIDataModel? = null
    var source: String? = null
    lateinit var presenter: SendMoneyPresenter
    private var snakBar: Snackbar? = null
    private lateinit var customProgressDialog : CustomProgressDialog

    companion object {
        public const val EXTRA_UPI_PAYLOAD = "extra_upi_payload"
        public const val EXTRA_SOURCE = "extra_upi_source"
        public const val EXTRA_UUID = "extra_uuid"
        public const val PAYER_CODE_PERSON = "0000"
        public const val NOTE_PROMPT = "***"
        var minimumAmount = 0.toBigDecimal()
        var maximumAmount = 99999.toBigDecimal()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
        presenter = SendMoneyPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var bundle = arguments
        bundle?.let {
            upiPayload = it.getParcelable(EXTRA_UPI_PAYLOAD)
            source = it.getString(EXTRA_SOURCE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_send_money, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnProceedToPay?.setOnClickListener(this)
        rootContainer?.setOnClickListener(this)
        etAmount?.addTextChangedListener(CustomTextWatcher(this))
        setDisplayAmount()
       // etAmount?.disableCopyPaste()
    }

    private fun setDisplayAmount() {
        upiPayload?.let {
            tvPayeeName?.text = it.payeeName
            var amount = it.payeeAmount?.toDouble()
            amount?.let {
                if (it > 0) {
                    etAmount?.setText(amount.toString())
                    etAmount?.isEnabled = false
                }
            }
        }
    }

    override fun switchScreen(result: ScanAndPayResponse) {
        hideProgressLoader()
        presenter.switchScreen(etAmount.text.toString(),result.txnId,upiPayload?.payeeName)
    }

    override fun showProgressLoader() {
        customProgressDialog = CustomProgressDialog.show(context)
    }

    override fun hideProgressLoader() {
        CustomProgressDialog.dismiss(customProgressDialog)
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        hideProgressLoader()
        try {
            val message = handleNetworkError(ServerException.DefaultErrorResponse(errorResponse.message))
            context?.let { showSnakBar(message, it) }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun setErrorMessage(message: String) {
        etAmount?.setError(message)
        etAmount?.requestFocus()
    }

    private fun showSnakBar(message: String, context: Context) {
        try {
            view?.let {
                snakBar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                snakBar?.let {
                    it.view.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_400))
                    it.show()
                }
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onClick(v: View?) {
        hideKeyboard()
        when (v?.id) {
            R.id.btnProceedToPay -> {
                presenter.validatePayData(etAmount?.text.toString(), source, upiPayload)
            }
            R.id.rootContainer->{
                etAmount?.clearFocus()
                hideKeyboard()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        onTextChange()
    }

    override fun onTextChange() {
        etAmount?.setError(null)
    }

    override fun getFragmentListner(): EpayFragmentListner {
        return epayFragmentListner
    }

    fun TextView.disableCopyPaste() {
        isLongClickable = false
        setTextIsSelectable(false)
        customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu): Boolean {
                return false
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem): Boolean {
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode?) {}
        }
    }
}