package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.exception.MCCIncorrectException
import `in`.epaylater.eplpaymentsdk.exception.TRNotPresentException
import `in`.epaylater.eplpaymentsdk.exception.VPANotPresentException
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.UPIDataModel
import `in`.epaylater.eplpaymentsdk.models.requests.SendMoneyReq
import `in`.epaylater.eplpaymentsdk.parsers.QRCodeParser
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.journeyapps.barcodescanner.Size
import kotlinx.android.synthetic.main.custom_barcode_scanner.*
import kotlinx.android.synthetic.main.fragment_scan_n_pay.*
import java.util.*


class EPLScanNPayFragment : EPLBaseFragment(), View.OnClickListener {

    private lateinit var epayFragmentListner: EpayFragmentListner
    private var IS_FLASH_ON = false

    companion object {
        private const val TYPE_UPI_QR = "upi"
        private const val TYPE_BHARAT_QR = "000201"
    }

    object UpiParams {
        // Mandatory fields
        const val PAYEE_ADDRESS = "pa"
        const val PAYEE_NAME = "pn"
        // Conditional
        const val TRANSACTION_REFERENCE_ID = "tr"
        // Optional
        const val AMOUNT = "am"
        const val MINIMUM_AMOUNT = "mam"
        const val MERCHANT_CODE = "mc"
        const val TRANSACTION_ID = "tid"
        const val TRANSACTION_NOTE = "tn"
        const val CURRENCY_CODE = "cu"
        const val REFERENCE_URL = "url"
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scan_n_pay, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        llFlashIcon?.setOnClickListener(this)
        rlContainer?.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        zxingBarcodeScanner.resume()
        val formats = Arrays.asList(BarcodeFormat.QR_CODE)
        zxingBarcodeScanner.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        zxingBarcodeScanner.decodeSingle(callback)
        zxing_barcode_surface.framingRectSize = Size(getScreenWidth(), getScreenHeight());
        rlFrame.layoutParams.width = getScreenWidth()
        rlFrame.layoutParams.height = getScreenHeight()
        EPLLogger.INSTANCE.e(getScreenWidth().toString()+"  height  "+getScreenHeight().toString())
    }

    override fun onPause() {
        zxingBarcodeScanner.pause()
        super.onPause()
    }

    private fun handleQrResult(qrText: String) {
        when {
            qrText.startsWith(TYPE_UPI_QR) -> parseUPIQRData(qrText)
            qrText.startsWith(TYPE_BHARAT_QR) -> try {
                parseBharatQRDataForVPA(qrText)
            } catch (e: Exception) {
                if (e is MCCIncorrectException || e is VPANotPresentException) {
                    e.message?.let {
                        showToast(it)
                    }
                }
                try {
                    parseBharatQRDataForBankAccount(qrText)
                } catch (e: Exception) {
                    when (e) {
                        is TRNotPresentException -> showToast(e.message.toString())
                        is VPANotPresentException -> showToast(e.message.toString())
                        else -> showToast(getString(R.string.text_error_invalid_bharat_qr))
                    }
                    onbackPressed()
                }
            }
            else -> {
                showToast(getString(R.string.text_error_qr_not_supported))
                onbackPressed()

            }
        }
    }

    private fun onbackPressed() {
        try {
            activity?.onBackPressed()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }

    }

    private fun parseBharatQRDataForBankAccount(bharatQRtext: String) {
        val upiExtra = QRCodeParser.parseBharatQRDataForBankAccount(bharatQRtext)
        if (upiExtra != null) {
            startSendMoneyFragment(upiExtra.copy(isBharatQR = true))
        } else {
            showToast(getString(R.string.text_error_invalid_bharat_qr))
            onbackPressed()
        }
    }

    private fun parseBharatQRDataForVPA(bharatQRtext: String) {
        val upiExtra = QRCodeParser.parseBharatQRDataForVPA(bharatQRtext)
        if (upiExtra != null) {
            startSendMoneyFragment(upiExtra.copy(isBharatQR = true))
        } else {
            throw VPANotPresentException("Transaction declined due to no VPA")
        }
    }

    private fun parseUPIQRData(upiUriString: String) {
        val upiExtra = QRCodeParser.parseUPIQrData(upiUriString)
        if (upiExtra.payeeAddress.isNotBlank()) {
            startSendMoneyFragment(upiExtra)
        } else {
            showToast(getString(R.string.text_not_valid_vpa_format, upiExtra.payeeAddress))
            onbackPressed()
        }
    }

    private fun startSendMoneyFragment(upiExtra: UPIDataModel) {
        try {
            var upiExtraData: UPIDataModel
            if (upiExtra.merchantCode.isNullOrEmpty()) {
                upiExtraData = upiExtra.copy(merchantCode = EPLSendMoneyFragment.PAYER_CODE_PERSON)
            } else {
                upiExtraData = upiExtra
            }
            val bundle = Bundle()
            bundle.putParcelable(EPLSendMoneyFragment.EXTRA_UPI_PAYLOAD, upiExtraData)
            bundle.putString(EPLSendMoneyFragment.EXTRA_SOURCE, SendMoneyReq.Source.QRCODE.name)
            var fragment = EPLSendMoneyFragment()
            fragment.arguments = bundle
            epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }


    private val callback = object : BarcodeCallback {
        override fun barcodeResult(result: BarcodeResult) {
            handleQrResult(result.text)
        }

        override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llFlashIcon -> {
                turnFlashOnOff()
            }
            R.id.rlContainer -> {
                //hideKeyboard()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        zxingBarcodeScanner?.removeAllViews()
    }

    override fun onDetach() {
        super.onDetach()
        zxingBarcodeScanner?.removeAllViews()
    }

    private fun turnFlashOnOff() {
        try {
            if (IS_FLASH_ON) {
                IS_FLASH_ON = false
                zxingBarcodeScanner?.setTorchOff()
                ivFlash.setImageResource(R.drawable.ic_flash_icon_off)
            } else {
                IS_FLASH_ON = true
                zxingBarcodeScanner?.setTorchOn()
                ivFlash.setImageResource(R.drawable.ic_flash_icon_on)
            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    fun getScreenWidth(): Int {
        val displayMetrics = context!!.getResources().displayMetrics
        return (displayMetrics.widthPixels/1.3).toInt()
    }

    fun getScreenHeight(): Int {
        val displayMetrics = context!!.getResources().displayMetrics
        return (displayMetrics.heightPixels/1.9).toInt()
    }

}