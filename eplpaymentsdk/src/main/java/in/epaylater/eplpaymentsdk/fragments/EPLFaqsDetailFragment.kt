package `in`.epaylater.eplpaymentsdk.fragments


import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.FAQSDetailsAdapter
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.FAQSModel
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_faqs_details.*

class EPLFaqsDetailFragment : EPLBaseFragment(), View.OnClickListener {

    private lateinit var epayFragmentListner: EpayFragmentListner
    private var faqsList: ArrayList<FAQSModel>? = null
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var adapter: FAQSDetailsAdapter
    private var faqsQuestion: String? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = arguments
        bundle?.let {
            faqsList = it.getParcelableArrayList(EPLConstants.KEYS.EXTRA_FAQS_QUESTIONS_ANS)
            faqsQuestion = it.getString(EPLConstants.KEYS.EXTRA_FAQS_QUESTION)
        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_faqs_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnContactUs?.setOnClickListener(this)
        try {
            faqsQuestion?.let {
                tvFAQQuestion?.text = it
            }
            faqsList?.let {
                adapter = FAQSDetailsAdapter(it)
                rvFAQSDetails?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
                rvFAQSDetails?.hasFixedSize()
                rvFAQSDetails?.adapter = adapter
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnContactUs -> {
                epayFragmentListner.changeEplFragment(EPLContactUsFragment(), EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
            }
        }
    }


}