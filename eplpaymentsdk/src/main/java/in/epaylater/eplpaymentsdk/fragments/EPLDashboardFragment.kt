package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.broadcastreceiver.SMSDeliveryBroadcast
import `in`.epaylater.eplpaymentsdk.broadcastreceiver.SMSSentBroadcast
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.DashboardContracter
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.CustomProgressDialog
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EPLSMSDeliveryInterface
import `in`.epaylater.eplpaymentsdk.interfaces.EPLSMSSentInterface
import `in`.epaylater.eplpaymentsdk.interfaces.PermissionCheckListener
import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import `in`.epaylater.eplpaymentsdk.models.IDFCInitProfileModel
import `in`.epaylater.eplpaymentsdk.models.requests.UPIProfileReq
import `in`.epaylater.eplpaymentsdk.models.responses.EncMessageResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.presenters.DashboardPresenter
import `in`.epaylater.eplpaymentsdk.utils.DeviceInfoUtil
import `in`.epaylater.eplpaymentsdk.utils.EPLCommonUtil
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import `in`.epaylater.eplpaymentsdk.utils.SimInfoUtil
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.layout_bottombar.*
import kotlinx.android.synthetic.main.layout_empty_default_view.*
import kotlinx.android.synthetic.main.layout_epl_create_upi_flow.*
import kotlinx.android.synthetic.main.layout_epl_dashboard_container.*
import kotlinx.android.synthetic.main.layout_epl_reauth.*
import kotlinx.android.synthetic.main.layout_epl_vpa_flow.*
import kotlinx.android.synthetic.main.layout_epl_vpa_flow.ivCenterIcon


class EPLDashboardFragment : EPLBaseFragment(), DashboardContracter.View, View.OnClickListener, EPLSMSDeliveryInterface,
    EPLSMSSentInterface {

    companion object {
        var SENT = "SMS_SENT"
        var DELIVERED = "SMS_DELIVERED"
    }

    private var sendSMSBroadcastReceiver = SMSSentBroadcast(this)
    private var deliveredSMSBroadcastReceiver = SMSDeliveryBroadcast(this)

    private lateinit var presenter: DashboardContracter.Presenter

    private lateinit var simInfoUtil: SimInfoUtil
    private var userProfileModel: IDFCInitProfileModel? = null
    private lateinit var deviceInfoUtil: DeviceInfoUtil
    private var snakBar: Snackbar? = null
    private var countDownTimer: CountDownTimer? = null
    private var simInfoSelected: DeviceDataModel.SimInfo? = null
    private var upiProfileReq: UPIProfileReq? = null
    var fcmToken: String? = ""
    private lateinit var customProgressDialog : CustomProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter = DashboardPresenter(this)
        context?.let { simInfoUtil = SimInfoUtil(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = arguments
        userProfileModel = bundle!!.getParcelable<IDFCInitProfileModel>(EPLConstants.KEYS.USER_DATA)
        userProfileModel?.let { presenter.saveUserProfile(it) }
        fcmToken = userProfileModel?.let { it.fcmToken }
        context?.let { deviceInfoUtil = DeviceInfoUtil(it, fcmToken!!, simInfoUtil) }
        onChildBackStack()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startProcess()
    }

    private fun startProcess() {
        tvBottomHome?.setOnClickListener(this)
        tvBottomScanPay?.setOnClickListener(this)
        tvBottomNotification?.setOnClickListener(this)
        tvBottomWhereToUse?.setOnClickListener(this)
        btnRetry?.setOnClickListener(this)
        btnReAuthDevice?.setOnClickListener(this)
        addBadgeView()
        val isRepeatedUser = sharedPref.getBoolean(EPLSharedPref.PREF_IS_LAUNCHED_REPEATED_USER_LAUNCH)
        if (isRepeatedUser) {
            layoutEPLContainer?.visibility = View.VISIBLE
            layoutVPAFlow?.visibility = View.GONE
        } else {
            setVPAFlowStep(1)
            layoutEPLContainer?.visibility = View.GONE
            layoutVPAFlow?.visibility = View.VISIBLE
        }
        getDeviceInfo()
    }

    override fun onResume() {
        super.onResume()
        activity?.title = ""
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnReAuthDevice -> {
                getSimDetails()
            }
            R.id.tvBottomHome -> {
                popAllBackStackFragment(this)
            }
            R.id.tvBottomScanPay -> {
                checkPermissions(object : PermissionCheckListener {
                    override fun onPermissionGranted(isPermissionGranted: Boolean) {
                        if (isPermissionGranted) {
                            addFragment(EPLScanNPayFragment(), EPLConstants.SCREEN_NAME.EPL_SCAN_N_PAY)
                        }
                    }
                }, true, EPLConstants.PERMISSION_REQUESTCODES.CAMERA)

            }
            R.id.tvBottomNotification -> {
                addFragment(EPLPendingCollectFragment(), EPLConstants.SCREEN_NAME.EPL_PENDING_COLLECT)
            }
            R.id.tvBottomWhereToUse -> {
                addFragment(EPLWhereToUseFragment(), EPLConstants.SCREEN_NAME.EPL_WHERE_TO_USE)
            }
            R.id.btnRetry->{
                errorView?.visibility = View.GONE
                getDeviceInfo()
            }
        }
    }

    private fun getDeviceInfo() {
        checkPermissions(
            object : PermissionCheckListener {
                override fun onPermissionGranted(isPermissionGranted: Boolean) {
                    if (isPermissionGranted) {
                        loginUser()
                    } else {
                        context?.let {
                            showSnakBar(getString(R.string.permission_denied), it)
                        }
                    }
                }
            },
            true,
            EPLConstants.PERMISSION_REQUESTCODES.READ_PHONE_STATE,
            EPLConstants.PERMISSION_REQUESTCODES.CAMERA
            //EPLConstants.PERMISSION_REQUESTCODES.SMS
        )
    }

    private fun loginUser() {
        showProgressLoader()
        userProfileModel?.let {
            presenter.getToken(it)
        }
    }

    override fun switchScreen() {
        if (isAPPInForground) {
            seekBarUpi.setProgress(100)
            val handler = Handler()
            handler.postDelayed({
                switchFragment(EPLHomeFragment(), EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
            }, 1000)
        } else {
            context?.let {
                showSnakBar(getString(R.string.error_default_network), it)
            }
        }
    }


    private fun onChildBackStack() {
        childFragmentManager.addOnBackStackChangedListener {
            val fragment = childFragmentManager.findFragmentById(R.id.eplContainer)
            fragment?.let {
                try {
                    snakBar?.let {
                        it.dismiss()
                    }
                    setTitle(fragment)
                } catch (e: java.lang.Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
                if (fragment is EPLHomeFragment) {
                    llBottomBar.setVisibility(View.VISIBLE)
                } else if (fragment is EPLScanNPayFragment) {
                    llBottomBar.setVisibility(View.GONE)
                } else if (fragment is EPLPendingCollectFragment) {
                    llBottomBar.setVisibility(View.VISIBLE)
                } else if (fragment is EPLWhereToUseFragment) {
                    llBottomBar.setVisibility(View.VISIBLE)
                } else if (fragment is EPLSendMoneyFragment) {
                    llBottomBar.setVisibility(View.VISIBLE)
                } else if (fragment is EPLConfirmPaymentFragment) {
                    llBottomBar.setVisibility(View.VISIBLE)
                } else {
                }
            }
        }
    }

    public fun addBadgeView() {
        var pendingCount = sharedPref.getInt(EPLSharedPref.PREF_PENDING_APPROVAL_COUNT)
        if (pendingCount > 0) {
            try {
                context?.let {
                    tvPendingPayment?.visibility = View.VISIBLE
                    tvPendingPayment.text = pendingCount.toString()

                }
            } catch (e: Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        } else {
            tvPendingPayment?.visibility = View.GONE
        }
    }

    override fun sendDeviceData() {
        checkPermissions(object : PermissionCheckListener {
            override fun onPermissionGranted(isPermissionGranted: Boolean) {
                if (isPermissionGranted) {
                    presenter.sendDeviceData(deviceInfoUtil.getDeviceInfo())
                } else {
                    context?.let {
                        showSnakBar(getString(R.string.permission_denied), it)
                    }
                }
            }
        }, true, EPLConstants.PERMISSION_REQUESTCODES.READ_PHONE_STATE)
    }

    override fun setVPAFlowStep(pageNo: Int) {
        when (pageNo) {
            1 -> {
                tvStatus?.text = getString(R.string.status_step1)
                ivCenterIcon?.setImageResource(R.drawable.ic_setup_deviceinfo)
                seekBarUpi?.setProgress(10)
                tvStep?.text = getString(R.string.text_upi_step, pageNo)
            }
            2 -> {
                tvStatus?.text = getString(R.string.status_step2)
                ivCenterIcon?.setImageResource(R.drawable.ic_setup_sendingsms)
                seekBarUpi?.setProgress(40)
                tvStep?.text = getString(R.string.text_upi_step, pageNo)
            }
            3 -> {
                tvStatus?.text = getString(R.string.status_step3)
                ivCenterIcon?.setImageResource(R.drawable.ic_setup_registeringdevice)
                seekBarUpi?.setProgress(70)
                tvStep?.text = getString(R.string.text_upi_step, pageNo)
            }

            4 -> {
                tvStatus?.text = getString(R.string.status_step4)
                ivCenterIcon?.setImageResource(R.drawable.ic_setup_vpa)
                seekBarUpi?.setProgress(80)
                tvStep?.text = getString(R.string.text_upi_step, pageNo)
            }

            else -> {
                tvStatus?.text = getString(R.string.status_step5)
                ivCenterIcon?.setImageResource(R.drawable.ic_setup_vpa)
                tvStep?.text = getString(R.string.text_upi_step, pageNo)
            }
        }
    }

    override fun startSimBinding(simSerialNo: String, imeiNo: String) {
        setVPAFlowStep(2)
        if (EPLCommonUtil.INSTANCE.isAirplaneModeOn(context)) {
            context?.let {
                showSnakBar(getString(R.string.turn_airplane_mode_off), it)
            }
        } else {
            if (EPLCommonUtil.INSTANCE.isConnectedToMobileNetwork(context)) {
                if (simInfoUtil.getSimInfo().isNotEmpty()) {
                    var isSameSimDetected = false;
                    for (i in 0..simInfoUtil.getSimInfo().size - 1) {
                        if (simInfoUtil.getSimInfo().get(i).simSerial.equals(simSerialNo) && simInfoUtil.getSimInfo().get(
                                i
                            ).imei.equals(
                                imeiNo
                            )
                        ) {
                            isSameSimDetected = true
                            break
                        } else {
                            isSameSimDetected = false
                        }
                    }
                    if (isSameSimDetected) {
                        presenter.startSimBinding(simSerialNo, imeiNo)
                    } else {
                        context?.let {
                            showSnakBar(getString(R.string.sim_card_changed), it)
                        }
                    }
                } else {
                    context?.let {
                        showSnakBar(getString(R.string.no_sim_card_detected), it)
                    }
                }
            } else {
                context?.let {
                    showSnakBar(getString(R.string.connect_to_data_connection), it)
                }
            }
        }
    }

    override fun sendSilentSMS(encMessageResponse: EncMessageResponse) {
        setVPAFlowStep(3)
        hideProgressLoader()
        simInfoSelected?.let {
            this.upiProfileReq = UPIProfileReq(
                it.imei,
                it.simSerial,
                it.simSlot,
                sharedPref.getString(EPLSharedPref.PREF_UPI_PHONE_NO),
                encMessageResponse.encMessage,
                deviceInfoUtil.getDeviceInfo()
            )
        }
        sendSMSIntent(encMessageResponse.encMessage, encMessageResponse.silentSmsTo)
    }

    private fun sendSMSIntent(encmessage: String, silentSmsTo: String) {
        try {
            Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("smsto:$silentSmsTo")
                putExtra("sms_body", encmessage)
                putExtra("exit_on_sent", true)
            }.run {
                context?.let {
                    if (this.resolveActivity(it.packageManager) != null) {
                        (it as Activity).startActivityForResult(
                            this,
                            EPLConstants.REQUEST_CODE.EPL_REQUEST_SEND_SMS
                        )
                    }
                }

            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun goToEPLHomeScreen() {
        setVPAFlowStep(4)
        hideProgressLoader()
        if (isAPPInForground) {
            layoutVPAFlow?.visibility = View.GONE
            layoutEPLReAuthDevice?.visibility = View.GONE
            layoutEPLContainer?.visibility = View.VISIBLE
            layoutEPLSetUpUPI?.visibility = View.GONE

            switchFragment(EPLHomeFragment(), EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
        } else {
            context?.let {
                showSnakBar(getString(R.string.error_default_network), it)
            }
        }
    }

    override fun getSimDetails() {
        layoutVPAFlow?.visibility = View.GONE
        layoutEPLReAuthDevice?.visibility = View.GONE
        layoutEPLContainer?.visibility = View.GONE
        layoutEPLSetUpUPI?.visibility = View.VISIBLE

        llSimContainer.removeAllViews()
        val listSimInfo = simInfoUtil.getSimInfo()
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val margin = getResources().getDimension(R.dimen.small_space).toInt()
        params.setMargins(margin, margin, margin, margin)
        checkImeiNotFoundFlag()

        if (!listSimInfo.isEmpty()) {
            for (i in listSimInfo.indices) {
                val simInfo = listSimInfo[i]
                val simLayout =
                    LayoutInflater.from(context).inflate(R.layout.layout_item_sim, llSimContainer, false)
                val tvSimName = simLayout.findViewById<TextView>(R.id.tvSimName)
                tvSimName.text = simInfo.carrier
                simLayout.id = simInfo.simSlot
                simLayout.setOnClickListener(getSimSelected(simInfo))
                llSimContainer.addView(simLayout)
            }
        }

        hideProgressLoader()
    }

    private fun checkImeiNotFoundFlag() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1 && simInfoUtil.isDualSim()) {
            showPutSimInSlotOne()
        }
    }

    private fun showPutSimInSlotOne() {
        tvImeiWarning.visibility = View.VISIBLE
    }

    private fun getSimSelected(simInfo: DeviceDataModel.SimInfo): View.OnClickListener {
        return View.OnClickListener {
            if (!simInfo.imei.isNullOrEmpty() && !simInfo.simSerial.isNullOrEmpty()) {
                switchToSimBinding(simInfo)
            }
        }
    }

    override fun setUpUpi() {
        checkPermissions(object : PermissionCheckListener {
            override fun onPermissionGranted(isPermissionGranted: Boolean) {
                if (isPermissionGranted) {
                    getSimDetails();
                } else {
                    context?.let {
                        showSnakBar(getString(R.string.permission_denied), it)
                    }
                }
            }
        }, true, EPLConstants.PERMISSION_REQUESTCODES.READ_PHONE_STATE)
    }

    private fun switchToSimBinding(simInfo: DeviceDataModel.SimInfo) {
        layoutVPAFlow.visibility = View.VISIBLE
        layoutEPLReAuthDevice.visibility = View.GONE
        layoutEPLContainer.visibility = View.GONE
        layoutEPLSetUpUPI.visibility = View.GONE

        this.simInfoSelected = simInfo

        startSimBinding(simInfo.simSerial, simInfo.imei)

    }

    override fun showProgressLoader() {
        customProgressDialog = CustomProgressDialog.show(context)
    }

    override fun hideProgressLoader() {
        CustomProgressDialog.dismiss(customProgressDialog)
    }

    override fun onError(errorResponse: ErrorResponse, type: Int) {
        hideProgressLoader()
        try {
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                showToast(errorResponse.message)
                reAuthUPIProfile()
            } else if (errorResponse.code == 401) {
                startProcess()
            } else {
                val message = handleNetworkError(ServerException.DefaultErrorResponse(errorResponse.message))
                context?.let { showSnakBar(message, it) }
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun showSnakBar(message: String, context: Context) {
        hideProgressLoader()
        stopTimer()
        /*view?.let {
            snakBar = Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE)
            snakBar?.let {
                it.view.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_400))
                it.setAction("Retry", View.OnClickListener { getDeviceInfo() })
                it.setActionTextColor(ContextCompat.getColor(context, R.color.white))
                it.show()
            }
        }
*/
        errorView?.visibility = View.VISIBLE
        tvEmptyText?.text = message
    }

    private fun showSnakBarWithoutRetry(message: String, context: Context) {
        hideProgressLoader()
        view?.let {
            snakBar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
            snakBar?.let {
                it.view.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_400))
                it.show()
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == EPLConstants.REQUEST_CODE.EPL_REQUEST_SETTING) {
            getDeviceInfo()
        } else if (requestCode == EPLConstants.REQUEST_CODE.EPL_REQUEST_SEND_SMS) {
            startTimer()
        }
    }

    private fun startTimer() {
        context?.let {
            var isConnectedToInternet = EPLCommonUtil.INSTANCE.isConnectedToMobileNetwork(it)
            if (isConnectedToInternet) {
                val time = 10000
                try {
                    tvTimer?.visibility = View.VISIBLE
                    countDownTimer = object : CountDownTimer(time.toLong(), 1000) {
                        override fun onTick(millisUntilFinished: Long) {
                            tvTimer?.text = (millisUntilFinished / 1000).toString() + "sec"
                        }

                        override fun onFinish() {
                            tvTimer?.visibility = View.GONE
                            createUPIProfile()
                        }
                    }.start()
                } catch (e: Exception) {
                    EPLLogger.INSTANCE.e(e)
                }
            } else {
                showSnakBar(getString(R.string.connect_to_data_connection), it)
            }
        }
    }

    private fun createUPIProfile() {
        setVPAFlowStep(4)
        if (EPLCommonUtil.INSTANCE.isAirplaneModeOn(context)) {
            context?.let {
                showSnakBar(getString(R.string.turn_airplane_mode_off), it)
            }
        } else {
            if (EPLCommonUtil.INSTANCE.isConnectedToMobileNetwork(context)) {
                showProgressLoader()
                upiProfileReq?.let { presenter.createUPIProfile(it) }
            } else {
                context?.let {
                    showSnakBar(getString(R.string.connect_to_data_connection), it)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        snakBar?.let { it.dismiss() }
        stopTimer()
        //unregisteredSMSDeliveredBroadCast()
        //unregisteredSMSSendBroadCast()
    }

    private fun stopTimer() {
        countDownTimer?.let { it.cancel() }
        tvTimer?.visibility = View.GONE
    }

    override fun reAuthUPIProfile() {
        layoutVPAFlow.visibility = View.GONE
        layoutEPLReAuthDevice.visibility = View.VISIBLE
        layoutEPLContainer.visibility = View.GONE
        layoutEPLSetUpUPI.visibility = View.GONE
    }

    private fun reLogin() {
        layoutVPAFlow.visibility = View.VISIBLE
        layoutEPLReAuthDevice.visibility = View.GONE
        layoutEPLContainer.visibility = View.GONE
        layoutEPLSetUpUPI.visibility = View.GONE

    }

    override fun switchFragment(fragment: Fragment, type: Int) {
        hideProgressLoader()
        try {
            activity?.title = ""
            snakBar?.let {
                it.dismiss()
            }
            hideKeyboard()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        if (type == EPLConstants.TYPES.TYPE_SESSION_EXPIRE) {
            reLogin()
            var result = popAllTopFragmentWithList(this)
            if (result) {
                startProcess()
            }
        } else if (type == EPLConstants.TYPES.TYPE_REAUTH_UPI) {
            var result = popAllTopFragmentWithList(this)
            if (result) {
                reAuthUPIProfile()
            }
        } else if (type == EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT) {
            when {
                fragment is EPLHomeFragment -> {
                    sharedPref.putBoolean(EPLSharedPref.PREF_IS_LAUNCHED_REPEATED_USER_LAUNCH, true)
                    addFragment(fragment, EPLConstants.SCREEN_NAME.EPL_HOME_FRAGMENT)
                }
                fragment is EPLSendMoneyFragment -> {
                    replaceFragment(fragment, EPLConstants.SCREEN_NAME.EPL_SEND_MONEY)

                }
                fragment is EPLConfirmPaymentFragment -> {
                    replaceFragment(fragment, EPLConstants.SCREEN_NAME.EPL_CONFIRM_PAYMENT)
                }
                fragment is EPLPaymentStatusFragment -> {
                    popAllBackStackFragment(this)
                    replaceFragment(fragment, EPLConstants.SCREEN_NAME.EPL_PAYMENT_STATUS)
                }
                fragment is EPLWhereToUseFragment -> {
                    replaceFragment(EPLWhereToUseFragment(), EPLConstants.SCREEN_NAME.EPL_WHERE_TO_USE)
                }
                fragment is EPLFAQSQuestionsFragment -> {
                    replaceFragment(fragment, EPLConstants.SCREEN_NAME.EPL_FAQS)
                }
                fragment is EPLPayNowFragment -> {
                    replaceFragment(fragment, EPLConstants.SCREEN_NAME.EPL_PAYNOW)
                }
                fragment is EPLFaqsDetailFragment -> {
                    replaceFragment(fragment, EPLConstants.SCREEN_NAME.EPL_FAQS_DETAIL)
                }
                fragment is EPLContactUsFragment -> {
                    replaceFragment(fragment, EPLConstants.SCREEN_NAME.EPL_CONTACT_US)
                }
            }
        }

    }

    override fun onDeliveredSMS(result: Boolean) {
        unregisteredSMSDeliveredBroadCast()
        if (result) {
            stopTimer()
            createUPIProfile()
        } else {
            context?.let {
                showSnakBar(getString(R.string.sms_not_delivered), it)
            }
        }
    }

    override fun onSentSMS(result: Boolean) {
        unregisteredSMSSendBroadCast()
        if (result) {
            startTimer()
        }
    }

    fun unregisteredSMSSendBroadCast() {
        try {
            context?.unregisterReceiver(sendSMSBroadcastReceiver)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    fun unregisteredSMSDeliveredBroadCast() {
        try {
            context?.unregisterReceiver(deliveredSMSBroadcastReceiver)
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == EPLConstants.REQUEST_CODE.REQUEST_CODE_PERMISSION) {
            if (permissions.get(0).equals(android.Manifest.permission.CALL_PHONE)) {
                val fragment = childFragmentManager.findFragmentById(R.id.eplContainer)
                fragment!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }
}