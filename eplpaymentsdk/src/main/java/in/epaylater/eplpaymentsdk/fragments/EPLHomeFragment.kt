package `in`.epaylater.eplpaymentsdk.fragments

import `in`.epaylater.eplpaymentsdk.R
import `in`.epaylater.eplpaymentsdk.adapters.TxnViewpagerAdapter
import `in`.epaylater.eplpaymentsdk.constant.EPLConstants
import `in`.epaylater.eplpaymentsdk.contracters.HomeContracter
import `in`.epaylater.eplpaymentsdk.exception.ServerException
import `in`.epaylater.eplpaymentsdk.extras.BigDecimalConverter
import `in`.epaylater.eplpaymentsdk.extras.CustomProgressDialog
import `in`.epaylater.eplpaymentsdk.extras.EPLLogger
import `in`.epaylater.eplpaymentsdk.interceptor.ServerErrorInterceptor
import `in`.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner
import `in`.epaylater.eplpaymentsdk.models.responses.CreditSummaryResponse
import `in`.epaylater.eplpaymentsdk.models.responses.ErrorResponse
import `in`.epaylater.eplpaymentsdk.presenters.HomePresenter
import `in`.epaylater.eplpaymentsdk.utils.EPLSharedPref
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.text.SpannableString
import android.text.Spanned
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_epl_home.*
import kotlinx.android.synthetic.main.layout_available_credit_details.*
import kotlinx.android.synthetic.main.layout_empty_default_view.*
import kotlinx.android.synthetic.main.layout_first_time_user_center.*
import kotlinx.android.synthetic.main.layout_header_vpa_details.*
import kotlinx.android.synthetic.main.layout_question_about_postpaid.*
import kotlinx.android.synthetic.main.layout_repeat_user_center.*
import kotlinx.android.synthetic.main.layout_transaction_bottom.*
import kotlinx.android.synthetic.main.layout_view_breakup.*
import java.math.BigDecimal


class EPLHomeFragment : EPLBaseFragment(), View.OnClickListener, HomeContracter.View,
    SwipeRefreshLayout.OnRefreshListener {


    private lateinit var epayFragmentListner: EpayFragmentListner
    private lateinit var homePresenter: HomePresenter
    private var snakBar: Snackbar? = null
    private var creditSummaryResponse: CreditSummaryResponse? = null
    private var txnAdapter: TxnViewpagerAdapter? = null
    private lateinit var eplDashboardFragment: EPLDashboardFragment
    private lateinit var customProgressDialog: CustomProgressDialog
    private var isSwipeRefreshAllowed: Boolean = true

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        epayFragmentListner = activity as EpayFragmentListner
        homePresenter = HomePresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_epl_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        rlFaqLayout?.setOnClickListener(this)
        btnPayNow?.setOnClickListener(this)
        tvWhereToUse?.setOnClickListener(this)
        rlTransactionHistory?.setOnClickListener(this)
        tvViewBreakup?.setOnClickListener(this)
        layoutViewBreakup?.setOnClickListener(this)
        btnRetry?.setOnClickListener(this)
        rlCopyVPA?.setOnClickListener(this)
        try {
            var isFirstTimeUser = sharedPref.getBoolean(EPLSharedPref.PREF_IS_FIRST_TIME_USER)
            if (isFirstTimeUser) {
                layoutFirstTimeHeader?.visibility = View.VISIBLE
                layoutFirstTimeCenter?.visibility = View.VISIBLE
                layoutRepeatUser?.visibility = View.GONE
                layoutAvailableCreditDetails?.visibility = View.GONE
            } else {
                layoutFirstTimeHeader?.visibility = View.GONE
                layoutFirstTimeCenter?.visibility = View.GONE
                layoutRepeatUser?.visibility = View.VISIBLE
                layoutAvailableCreditDetails?.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }

        swiperefresh.setOnRefreshListener(this);
        swiperefresh.setColorScheme(
            R.color.maroon_100
        );
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showVPA()
        showCreditSummary()
    }

    private fun showVPA() {
        tvVPAId?.text = getString(R.string.user_vpa_id, sharedPref.getString(EPLSharedPref.PREF_UPI_VPA_ID))
        try {
            var text = getString(R.string.view_breakup)
            var spannableString = SpannableString(text)
            spannableString.setSpan(
                UnderlineSpan(),
                0,
                text.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );
            tvViewBreakup?.text = spannableString
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun showCreditSummary() {
        errorView?.visibility = View.GONE
        showProgressLoader()
        homePresenter.getCreditSummary()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlFaqLayout -> {
                epayFragmentListner.changeEplFragment(
                    EPLFAQSQuestionsFragment(),
                    EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT
                )
            }
            R.id.btnPayNow -> {
                creditSummaryResponse?.let {
                    var bundle = Bundle()
                    bundle.putDouble(
                        EPLConstants.KEYS.EXTRA_OUTSTANDING_AMOUNT,
                        BigDecimalConverter.toDouble(it.totalOutstandingAmount)
                    )
                    var fragment = EPLPayNowFragment()
                    fragment.arguments = bundle
                    epayFragmentListner.changeEplFragment(fragment, EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
                }
            }
            R.id.tvWhereToUse -> {
                epayFragmentListner.changeEplFragment(EPLWhereToUseFragment(), EPLConstants.TYPES.TYPE_SWITCH_FRAGMENT)
            }
            R.id.tvViewBreakup -> {
                openCloseBreakUpView()
            }
            R.id.layoutViewBreakup -> {
                openCloseBreakUpView()
            }
            R.id.rlTransactionHistory -> {
                openCloseTransactionHistory()
            }
            R.id.btnRetry -> {
                showCreditSummary()
            }
            R.id.rlCopyVPA -> {
                context?.let {
                    val clipboard = it.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                    val clip = ClipData.newPlainText("VPA copied", sharedPref.getString(EPLSharedPref.PREF_UPI_VPA_ID))
                    clipboard!!.setPrimaryClip(clip)
                    showToast("UPI ID copied")
                }
            }
        }
    }

    private fun openCloseBreakUpView() {
        if (layoutViewBreakup?.visibility == View.GONE) {
            layoutViewBreakup?.visibility = View.VISIBLE
        } else {
            layoutViewBreakup?.visibility = View.GONE
        }
    }

    private fun openCloseTransactionHistory() {
        if (viewTabLayout?.visibility == View.VISIBLE) {
            viewTabLayout?.visibility = View.GONE
            viewpagerTransactions?.visibility = View.GONE
            ivUpArrow?.setImageResource(R.drawable.ic_arrow_up)
            txnAdapter?.popAllFragment()
        } else {
            context?.let {
                viewTabLayout?.visibility = View.VISIBLE
                viewpagerTransactions?.visibility = View.VISIBLE
                ivUpArrow?.setImageResource(R.drawable.ic_arrow_down)
                viewpagerTransactions.isSaveEnabled = false
                setupViewPager()
            }
        }
    }

    private fun setupViewPager() {
        try {
            txnAdapter = TxnViewpagerAdapter(childFragmentManager)
            txnAdapter?.let {
                var transactionRecentFragment = EPLUnSettledTxnFragment()
                var transactionSettledFragment = EPLSettledTxnFragment()

                it.addFragment(transactionRecentFragment, EPLConstants.SCREEN_NAME.EPL_RECENT_TRANSACTION)
                it.addFragment(transactionSettledFragment, EPLConstants.SCREEN_NAME.EPL_SETTLED_TRANSACTION)

                viewpagerTransactions?.adapter = it
                tabsTransaction?.setupWithViewPager(viewpagerTransactions)
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        setTabDividerLine()
    }

    private fun setTabDividerLine() {
        try {
            val root = tabsTransaction.getChildAt(0)
            if (root is LinearLayout) {
                context?.let {
                    (root).showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
                    val drawable = GradientDrawable()
                    drawable.setColor(ContextCompat.getColor(it, R.color.grey_400))
                    drawable.setSize(1, 1)
                    (root).dividerPadding = 20
                    (root).dividerDrawable = drawable
                }

            }
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun setCurrentStatus(creditSummaryResponse: CreditSummaryResponse) {
        isSwipeRefreshAllowed = true
        hideProgressLoader()
        try {
            this.creditSummaryResponse = creditSummaryResponse
            sharedPref.putBoolean(EPLSharedPref.PREF_IS_FIRST_TIME_USER, creditSummaryResponse.isFirstTimeUser)
            var creditAvailable =
                BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.limit) - BigDecimalConverter.toTwoDigitPlace(
                    creditSummaryResponse.outstandingAmount
                )

            sharedPref.putString(EPLSharedPref.PREF_CREDIT_AVAILABLE, creditAvailable.toString())
            sharedPref.putString(EPLSharedPref.PREF_CREDIT_LIMIT, creditSummaryResponse.limit.toString())

            if (creditSummaryResponse.isFirstTimeUser) {
                rlTransactionHistory.visibility = View.GONE
                setFirstTimeUserView(creditSummaryResponse)
            } else {
                rlTransactionHistory.visibility = View.VISIBLE
                setRepeatedUserView(creditSummaryResponse)
            }
            rlMainView.visibility = View.VISIBLE
        } catch (e: Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        try {
            sharedPref.putInt(EPLSharedPref.PREF_PENDING_APPROVAL_COUNT, creditSummaryResponse.pendingApprovalCount)
            updateNotificationCount()
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    private fun setRepeatedUserView(creditSummaryResponse: CreditSummaryResponse) {
        if (creditSummaryResponse.isCreditPaused) {
            llCreditStatus?.visibility = View.VISIBLE
            tvOutStandingTitle?.visibility = View.GONE
            context?.let {
                pbCreditDetailStatus?.progressDrawable =
                    ContextCompat.getDrawable(it, R.drawable.progressbar_red)
            }
        } else {
            llCreditStatus?.visibility = View.GONE
            tvOutStandingTitle?.visibility = View.VISIBLE
            tvInfo?.setText(creditSummaryResponse.message)
            if (creditSummaryResponse.messageSeverity.equals("INFO")) {
                tvInfo?.setBackgroundResource(R.drawable.bg_bottom_rc_box_outstanding_info)
                tvInfo.visibility = View.VISIBLE
            } else if (creditSummaryResponse.messageSeverity.equals("ALERT")) {
                tvInfo?.setBackgroundResource(R.drawable.bg_bottom_rc_box_outstanding_alert)
                tvInfo.visibility = View.VISIBLE
            } else {
                tvInfo.visibility = View.GONE
            }
            context?.let {
                pbCreditDetailStatus?.progressDrawable =
                    ContextCompat.getDrawable(it, R.drawable.progressbar_green)
            }
        }

        var totalAmount =
            BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.totalOutstandingAmount);
        if (totalAmount > BigDecimal.ZERO) {
            btnPayNow?.isEnabled = true
            btnPayNow?.setBackgroundResource(R.drawable.bg_rc_rect_solid_maroon)
        } else {
            btnPayNow?.isEnabled = false
            btnPayNow?.setBackgroundResource(R.drawable.bg_rc_rect_solid_grey)
        }

        var creditLimit = BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.limit)
        var outstandingAmount = BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.outstandingAmount)
        var totalOutstandingAmount = BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.totalOutstandingAmount)
        var creditAvailable = (creditLimit - outstandingAmount)


        tvRepeatUserOutStandingAmount?.text =
            "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(totalOutstandingAmount)}"

        tvCreditDetailAvlAmount?.text =
            "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(creditAvailable)}"

        tvCreditDetailLimitAmount?.text =
            "${getString(`in`.epaylater.eplpaymentsdk.R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(
                creditLimit
            )}"

        try {
            var percent =
                (BigDecimalConverter.toDouble(creditAvailable / creditLimit) * 100).toInt()

            pbCreditDetailStatus?.progress = percent
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        layoutFirstTimeHeader?.visibility = View.GONE
        layoutFirstTimeCenter?.visibility = View.GONE
        layoutRepeatUser?.visibility = View.VISIBLE
        layoutAvailableCreditDetails?.visibility = View.VISIBLE

        showViewBreakUp(creditSummaryResponse)
    }

    private fun showViewBreakUp(creditSummaryResponse: CreditSummaryResponse) {
        if (!creditSummaryResponse.outstandingBreakup.isNullOrEmpty()) {
            tvViewBreakup?.visibility = View.VISIBLE
            try {
                var outstandingAmount = BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.outstandingAmount)
                var totalOutstandingAmount =
                    BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.totalOutstandingAmount)
                var amount =
                    BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.outstandingBreakup.get(0).amount)
                var desc = creditSummaryResponse.outstandingBreakup.get(0).amountDescription

                if (desc.isNotBlank()) {
                    tvTxtIntrest?.text = desc
                    tvTxtIntrestValue?.text =
                        "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(amount)}"
                    tvTxtIntrestValue.visibility = View.VISIBLE
                    tvTxtIntrest.visibility = View.VISIBLE
                    ivTooltipIcon?.visibility = View.VISIBLE
                } else {
                    tvTxtIntrestValue.visibility = View.GONE
                    tvTxtIntrest.visibility = View.GONE
                }
                if (outstandingAmount.toInt() > 0) {
                    tvTxtPrincipalValue?.text =
                        "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(
                            outstandingAmount
                        )}"

                    tvTxtPrincipalValue.visibility = View.VISIBLE
                    tvTxtPrincipal.visibility = View.VISIBLE

                    ivTooltipIcon?.visibility = View.VISIBLE
                } else {
                    tvTxtPrincipalValue.visibility = View.GONE
                    tvTxtPrincipal.visibility = View.GONE
                }

                if (totalOutstandingAmount.toInt() > 0) {
                    tvTxtTotalAmountValue?.text =
                        "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(
                            totalOutstandingAmount
                        )}"

                    rlAmount.visibility = View.VISIBLE
                    ivTooltipIcon?.visibility = View.VISIBLE
                } else {
                    rlAmount.visibility = View.GONE
                }
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
                tvViewBreakup?.visibility = View.GONE
                layoutViewBreakup?.visibility = View.GONE
            }
        } else {
            tvViewBreakup?.visibility = View.GONE
            layoutViewBreakup?.visibility = View.GONE
        }
    }

    private fun setFirstTimeUserView(creditSummaryResponse: CreditSummaryResponse) {
        var creditLimit = BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.limit)
        var outstandingAmount = BigDecimalConverter.toTwoDigitPlace(creditSummaryResponse.outstandingAmount)
        var creditFirstTimeAvail = (creditLimit - outstandingAmount)


        tvCreditFirstTimeAvlAmount?.text =
            "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(creditFirstTimeAvail)}"

        tvCreditFirstTimeUsedAmount?.text =
            "${getString(R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(outstandingAmount)}"

        tvCreditFirstTimeLeftAmount?.text =
            "${getString(`in`.epaylater.eplpaymentsdk.R.string.indian_rupee)}${BigDecimalConverter.numberFormatterUS(
                creditLimit
            )}"

        try {
            var percent =
                (BigDecimalConverter.toDouble(creditFirstTimeAvail / creditLimit) * 100).toInt()

            pbCreditFirstTimeStatus?.progress = percent
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
        layoutFirstTimeHeader?.visibility = View.VISIBLE
        layoutFirstTimeCenter?.visibility = View.VISIBLE
        layoutRepeatUser?.visibility = View.GONE
        layoutAvailableCreditDetails?.visibility = View.GONE
    }

    override fun showProgressLoader() {
        customProgressDialog = CustomProgressDialog.show(context)
    }

    override fun hideProgressLoader() {
        CustomProgressDialog.dismiss(customProgressDialog)
    }


    override fun onError(errorResponse: ErrorResponse, type: Int) {
        isSwipeRefreshAllowed = true
        hideProgressLoader()
        try {
            if (!errorResponse.eplErrorCode.isNullOrEmpty()
                && errorResponse.eplErrorCode.equals(ServerErrorInterceptor.UPI_SIM_REBINDING_REQUIRED)
            ) {
                showToast(errorResponse.message)
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_REAUTH_UPI)
            } else if (errorResponse.code == 401) {
                epayFragmentListner.changeEplFragment(EPLDashboardFragment(), EPLConstants.TYPES.TYPE_SESSION_EXPIRE)
            } else {
                val message = handleNetworkError(ServerException.DefaultErrorResponse(errorResponse.message))
                errorView?.visibility = View.VISIBLE
                tvEmptyText?.setText(message)
                btnRetry?.visibility = View.VISIBLE
            }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }

    override fun onBackPressed() {
        if (viewTabLayout?.visibility == View.VISIBLE) {
            openCloseTransactionHistory()
        } else {
            try {
                var count = activity?.supportFragmentManager?.backStackEntryCount
                count?.let {
                    val backStackId = activity?.supportFragmentManager?.getBackStackEntryAt(it - 1)?.getId()
                    backStackId?.let {
                        activity?.supportFragmentManager?.popBackStack(it, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    }
                }
            } catch (e: java.lang.Exception) {
                EPLLogger.INSTANCE.e(e)
            }
        }
    }

    override fun getFragmentListner(): EpayFragmentListner {
        return epayFragmentListner
    }

    override fun onRefresh() {
        swiperefresh.setRefreshing(false)
        if (isSwipeRefreshAllowed) {
            isSwipeRefreshAllowed = false
            showCreditSummary()
        }
    }

    private fun updateNotificationCount() {
        try {
            eplDashboardFragment = parentFragment as EPLDashboardFragment
            eplDashboardFragment.let { it.addBadgeView() }
        } catch (e: java.lang.Exception) {
            EPLLogger.INSTANCE.e(e)
        }
    }
}