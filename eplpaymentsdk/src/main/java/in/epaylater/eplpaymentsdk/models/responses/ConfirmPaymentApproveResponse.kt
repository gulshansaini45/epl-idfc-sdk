package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class ConfirmPaymentApproveResponse(
    @SerializedName("txnId") val txtId: String,
    @SerializedName("resCode") val resCode: String,
    @SerializedName("resDesc") val resDesc: String
)
