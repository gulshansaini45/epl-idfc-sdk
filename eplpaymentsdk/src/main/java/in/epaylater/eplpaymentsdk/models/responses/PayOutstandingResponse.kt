package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class PayOutstandingResponse(
    @SerializedName("status") val status: String,
    @SerializedName("msg") val msg: String
)