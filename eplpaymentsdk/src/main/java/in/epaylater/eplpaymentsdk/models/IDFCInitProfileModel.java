package in.epaylater.eplpaymentsdk.models;

import android.os.Parcel;
import android.os.Parcelable;

public class IDFCInitProfileModel implements Parcelable {
    public String custPhoneNumber = "";
    public String custfirstName = "";
    public String custlastName = "";
    public String custMiddleName = "";
    public String custEmailAddress = "";
    public String merchantSdkKey = "";
    public String odAccountNumber = "";
    public String savingAccountNumber = "";
    public String savingAccountName = "";
    public String custNameLinkedSavingAcc = "";
    public String bankName = "";
    public String ifscCode = "";
    public String fcmToken ="";
    public String custGender ="";
    public double latitude;
    public double longitude;
    public double accuracy;

    IDFCInitProfileModel(Builder builder) {
        this.custPhoneNumber = builder.custPhoneNumber;
        this.custfirstName = builder.custfirstName;
        this.custMiddleName = builder.custMiddleName;
        this.custlastName = builder.custlastName;
        this.custEmailAddress = builder.custEmailAddress;
        this.merchantSdkKey = builder.merchantSdkKey;
        this.odAccountNumber = builder.odAccountNumber;
        this.savingAccountName = builder.savingAccountName;
        this.savingAccountNumber = builder.savingAccountNumber;
        this.custNameLinkedSavingAcc = builder.custNameLinkedSavingAcc;
        this.fcmToken = builder.fcmToken;
        this.bankName = builder.bankName;
        this.ifscCode = builder.ifscCode;
        this.custGender = builder.custGender;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.accuracy = builder.accuracy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(custPhoneNumber);
        parcel.writeString(custfirstName);
        parcel.writeString(custMiddleName);
        parcel.writeString(custlastName);
        parcel.writeString(custEmailAddress);
        parcel.writeString(merchantSdkKey);
        parcel.writeString(odAccountNumber);
        parcel.writeString(savingAccountName);
        parcel.writeString(savingAccountNumber);
        parcel.writeString(custNameLinkedSavingAcc);
        parcel.writeString(fcmToken);
        parcel.writeString(bankName);
        parcel.writeString(ifscCode);
        parcel.writeString(custGender);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeDouble(accuracy);
    }

    private IDFCInitProfileModel(Parcel parcel) {
        this.custPhoneNumber = parcel.readString();
        this.custfirstName = parcel.readString();
        this.custMiddleName = parcel.readString();
        this.custlastName = parcel.readString();
        this.custEmailAddress = parcel.readString();
        this.merchantSdkKey = parcel.readString();
        this.odAccountNumber = parcel.readString();
        this.savingAccountName = parcel.readString();
        this.savingAccountNumber = parcel.readString();
        this.custNameLinkedSavingAcc = parcel.readString();
        this.fcmToken = parcel.readString();
        this.bankName = parcel.readString();
        this.custGender = parcel.readString();
        this.latitude = parcel.readDouble();
        this.longitude = parcel.readDouble();
        this.accuracy = parcel.readDouble();
    }

    public static final Parcelable.Creator<IDFCInitProfileModel> CREATOR = new Parcelable.Creator<IDFCInitProfileModel>() {
        @Override
        public IDFCInitProfileModel createFromParcel(Parcel parcel) {
            return new IDFCInitProfileModel(parcel);
        }

        @Override
        public IDFCInitProfileModel[] newArray(int size) {
            return new IDFCInitProfileModel[0];
        }
    };


    public static class Builder {
        private String custPhoneNumber = "";
        private String custfirstName = "";
        private String custlastName = "";
        private String custMiddleName = "";
        private String custEmailAddress = "";
        private String merchantSdkKey = "";
        private String odAccountNumber = "";
        private String savingAccountNumber = "";
        private String savingAccountName = "";
        private String custNameLinkedSavingAcc = "";
        private String fcmToken ="";
        private String bankName = "IDFC";
        private String ifscCode = "";
        public String custGender ="Male";
        private double latitude = 100.00;
        private double longitude = 97.21;
        private double accuracy = 100;

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder setCustPhoneNo(String value) {
            this.custPhoneNumber = value;
            return this;
        }

        public Builder setCustFirstName(String value) {
            this.custfirstName = value;
            return this;
        }

        public Builder setCustLastName(String value) {
            this.custlastName = value;
            return this;
        }

        public Builder setCustMiddleName(String value) {
            this.custMiddleName = value;
            return this;
        }

        public Builder setCustEmail(String value) {
            this.custEmailAddress = value;
            return this;
        }

        public Builder setMerchantSDKKey(String value) {
            this.merchantSdkKey = value;
            return this;
        }

        public Builder setCustODAccount(String value) {
            this.odAccountNumber = value;
            return this;
        }


        public Builder setSavingAccountNo(String value) {
            this.savingAccountNumber = value;
            return this;
        }

        public Builder setSavingAccountName(String value) {
            this.savingAccountName = value;
            return this;
        }

        public Builder setCustNameLinkedSavingAcc(String value) {
            this.custNameLinkedSavingAcc = value;
            return this;
        }

        public Builder setFCMToken(String value) {
            this.fcmToken = value;
            return this;
        }
        public Builder setBankName(String value) {
            this.bankName = value;
            return this;
        }

        public Builder setIFSCCode(String value) {
            this.ifscCode = value;
            return this;
        }
        public Builder setGender(String value) {
            this.custGender = value;
            return this;
        }
        public Builder setLatitude(double value) {
            this.latitude = value;
            return this;
        }

        public Builder setLongitude(double value) {
            this.longitude = value;
            return this;
        }

        public Builder setAccuracy(double value) {
            this.accuracy = value;
            return this;
        }

        public IDFCInitProfileModel build() {
            return new IDFCInitProfileModel(this);
        }
    }
}
