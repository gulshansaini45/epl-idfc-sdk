package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

class WhereToUseResponse(
    @SerializedName("merchantList") val merchantList: List<MerchantList>
) {
    class MerchantList(
        @SerializedName("sectionTitle") val sectionTitle: String,
        @SerializedName("merchants") val merchants: List<Merchants>
    )

    class Merchants(
        @SerializedName("name") val name: String,
        @SerializedName("logoURL") val logoURL: String
    )
}