package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class ConfirmPaymentDeclineResponse(
    @SerializedName("txnId") val txtId: String,
    @SerializedName("userAction") val userAction: String
)
