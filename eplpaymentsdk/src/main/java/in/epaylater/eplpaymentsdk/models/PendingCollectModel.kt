package `in`.epaylater.eplpaymentsdk.models

import `in`.epaylater.eplpaymentsdk.extras.TimeAgo
import `in`.epaylater.eplpaymentsdk.models.requests.PendingCollectMoneyResponse
import `in`.epaylater.eplpaymentsdk.utils.DateTimeUtil
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal
import java.text.ParseException

@Parcelize
data class PendingCollect(
    val txnId: String,
    val amount: BigDecimal,
    val payeeName: String,
    val status: String,
    val timeAgo: String
) : Parcelable {

    companion object {
        fun mapPendingCollectFromPendingCollectResponse(it: PendingCollectMoneyResponse): PendingCollect {
            val timeAgo: String = try {
               DateTimeUtil.getTimeAgo(it.timeStamp)
            } catch (e: ParseException) {
                it.timeStamp
            }
            return PendingCollect(it.orgTxnId, it.amount.toBigDecimal(), it.payeeName, "Pending", timeAgo)
        }
    }
}