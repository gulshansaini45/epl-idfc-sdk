package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class UnSettledTxnResonse(
    @SerializedName("unpaidTxnCount") val unpaidTxnCount: Int,
    @SerializedName("outstandingAmount") val outstandingAmount: BigDecimal,
    @SerializedName("availableCreditLimit") val availableCreditLimit: BigDecimal,
    @SerializedName("dueTxnCount") val dueTxnCount: Int,
    @SerializedName("overdueTxnCount") val overdueTxnCount: Int,
    @SerializedName("creditPaused") val creditPaused: Boolean,
    @SerializedName("foodForThough") val foodForThough: String,
    @SerializedName("unsettledTransactionList") val unsettledTransactionList: List<UnSettledTxnList>

) {
    data class UnSettledTxnList(
        @SerializedName("id") val id: Int,
        @SerializedName("marketplaceOrderId") val marketplaceOrderId: String,
        @SerializedName("txnAmount") val txnAmount: BigDecimal,
        @SerializedName("txnLateFees") val txnLateFees: BigDecimal,
        @SerializedName("txnConvenienceFees") val txnConvenienceFees: BigDecimal,
        @SerializedName("netPayableAmount") val netPayableAmount: BigDecimal,
        @SerializedName("marketplaceName") val marketplaceName: String,
        @SerializedName("marketplaceLogoUrl") val marketplaceLogoUrl: String,
        @SerializedName("isOverdue") val isOverdue: Boolean,
        @SerializedName("overdueDays") val overdueDays: Int,
        @SerializedName("txnDate") val txnDate: String,
        @SerializedName("txnDueDate") val txnDueDate: String
    )
}