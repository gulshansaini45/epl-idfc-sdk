package `in`.epaylater.eplpaymentsdk.models

import android.os.Parcel
import android.os.Parcelable

data class SimDataModel(
    var imei: String = "",
    var simName: String = "",
    var simSerial: String = "",
    var simSubId: String = "",
    var phoneNo: String? = "",
    val simSlot: Int) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString() ?: "",
        source.readString() ?: "",
        source.readString() ?: "",
        source.readString() ?: "",
        source.readString(),
        source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(imei)
        writeString(simName)
        writeString(simSerial)
        writeString(simSubId)
        writeString(phoneNo)
        writeInt(simSlot)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<SimDataModel> = object : Parcelable.Creator<SimDataModel> {
            override fun createFromParcel(source: Parcel): SimDataModel = SimDataModel(source)
            override fun newArray(size: Int): Array<SimDataModel?> = arrayOfNulls(size)
        }
    }
}