package `in`.epaylater.eplpaymentsdk.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class UPIDataModel(
    val payeeName: String?,
    val payeeAddress: String,
    val merchantCode: String?,
    val transactionId: String?,
    val transactionReferenceId: String?,
    val transactionNote: String?,
    val payeeAmount: BigDecimal?,
    val minimumAmount: BigDecimal?,
    val currencyCode: String?,
    val referenceUrl: String?,
    val transferType: TransferType,
    val isBharatQR: Boolean = false) : Parcelable {
    companion object {
        const val DEFAULT_CURRENCY_CODE = "INR"
    }

    enum class TransferType(val type: String) {
        VIR("VIR"),
        ACC("ACC")
    }
}