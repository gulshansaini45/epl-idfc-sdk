package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

class SettledTxnResponse(
    @SerializedName("availableSettledTxns") val availableSettledTxns: Int,
    @SerializedName("settledTxnCount") val settledTxnCoun: Int,
    @SerializedName("settledTransactionList") val settledTransactionList: List<SettledTxnList>
) {
    data class SettledTxnList(
        @SerializedName("id") val id: Int,
        @SerializedName("marketplaceOrderId") val marketplaceOrderId: String,
        @SerializedName("txnAmount") val txnAmount: BigDecimal,
        @SerializedName("txnLateFees") val txnLateFees: BigDecimal,
        @SerializedName("paidAmount") val paidAmount: BigDecimal,
        @SerializedName("txnConvenienceFees") val txnConvenienceFees: BigDecimal,
        @SerializedName("marketplaceName") val marketplaceName: String,
        @SerializedName("marketplaceLogoUrl") val marketplaceLogoUrl: String,
        @SerializedName("isSettled") val isSettled: Boolean,
        @SerializedName("txnDate") val txnDate: String,
        @SerializedName("settlementDate") val settlementDate: String
    )
}