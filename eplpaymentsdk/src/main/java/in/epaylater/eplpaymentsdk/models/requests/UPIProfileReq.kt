package `in`.epaylater.eplpaymentsdk.models.requests

import `in`.epaylater.eplpaymentsdk.models.DeviceDataModel
import com.google.gson.annotations.SerializedName

data class UPIProfileReq(
    @SerializedName("imei") val imei: String,
    @SerializedName("sim_serial") val simSerial: String,
    @SerializedName("sim_slot_no") val simSlotNo: Int,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("message") val message: String,
    @SerializedName("device") val deviceModel: DeviceDataModel
)


