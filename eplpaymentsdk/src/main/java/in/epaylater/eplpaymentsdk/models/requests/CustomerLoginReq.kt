package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

data class CustomerLoginReq(
    @SerializedName("firstName") val firstName: String,
    @SerializedName("lastName") val lastName: String,
    @SerializedName("middleName") val middleName: String,
    @SerializedName("telephoneNumber") val telephoneNumber: String,
    @SerializedName("emailAddress") val emailAddress: String,
    @SerializedName("gender") val gender: String
)