package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class PaymentMethodResponse(
    @SerializedName("methods") val methods: List<MethodType>
) {
    data class MethodType(
        @SerializedName("method") val method: String,
        @SerializedName("methodCode") val methodCode: String,
        @SerializedName("description") val description: String
    )
}