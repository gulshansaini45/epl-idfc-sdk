package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

data class BankAccountDetails(
    @SerializedName("odAccountNumber") val odAccountNumber: String,
    @SerializedName("savingsAccountNumber") val savingsAccountNumber: String,
    @SerializedName("savingsAccountName") val savingsAccountName: String,
    @SerializedName("ifscCode") val ifscCode: String,
    @SerializedName("bankName") val bankName: String,
    @SerializedName("accountHolderName") val accountHolderName: String
)