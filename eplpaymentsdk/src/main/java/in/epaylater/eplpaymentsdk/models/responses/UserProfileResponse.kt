package `in`.epaylater.eplpaymentsdk.models.responses

import `in`.epaylater.eplpaymentsdk.exception.ServerException
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

class UserProfileResponse(
    @SerializedName("profile_complete") val profileComplete: Boolean,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("email") val email: String,
    @SerializedName("icici_customer") val iciciCustomer: Boolean,
    @SerializedName("device_enabled") val deviceEnabled: Boolean,
    @SerializedName("uuid") val uuID: String?,
    @SerializedName("features") val features: Features,
    @SerializedName("nach_registered") val nachRegistered: Boolean,
    @SerializedName("mpin_set") val mpinSet: Boolean,
    @SerializedName("unmasked_mobile") val unmaskedMobile: String?,
    @SerializedName("unmasked_email") val unmaskedEmail: String?,
    @SerializedName("user_properties") val userProperties: UserProperty
) {


    data class UserProperty(
        @SerializedName("upi") val upi: Int,
        @SerializedName("blacklisted") val blacklisted: Int,
        @SerializedName("dmi") val dmi: Int
    )

    data class Features(
        @SerializedName("upi") val upi: UPI,
        @SerializedName("loyalty") val loyalty: Loyalty,
        @SerializedName("referral") val referral: Referral,
        @SerializedName("dmi") val dmi: DMI,
        @SerializedName("totp") val totp: TOTP,
        @SerializedName("creditEnhnc") val creditEnhnc: CreditEnhancement
    ) {
        data class UPI(
            @SerializedName("upi_enabled") val upiEnabled: Boolean,
            @SerializedName("scan_and_pay_enabled") val scanAndPayEnabled: Boolean,
            @SerializedName("dmi_enabled") val dmiEnabled: Boolean,
            @SerializedName("mpin_enabled") val mPinEnabled: Boolean
        )

        data class Loyalty(
            @SerializedName("enabled") val enabled: Boolean,
            @SerializedName("total_points") val totalPoints: BigDecimal?,
            @SerializedName("redeemable_points") val redeemablePoints: BigDecimal?,
            @SerializedName("repayment_enabled") val repaymentEnabled: Boolean
        )

        data class Referral(
            @SerializedName("enabled") val enabled: Boolean,
            @SerializedName("refUrl") val refUrl: String?
        )

        data class DMI(
            @SerializedName("ekycEnable") val ekycEnable: Boolean,
            @SerializedName("enachEnabled") val enachEnabled: Boolean,
            @SerializedName("ekycCompleted") val ekycCompleted: Boolean,
            @SerializedName("enachCompleted") val enachCompleted: Boolean
        )

        data class TOTP(
            @SerializedName("is_totp_enabled") val isTotpEnabled: Boolean,
            @SerializedName("is_totp_key_expired") val isTotpExpired: Boolean
        )

        data class CreditEnhancement(
            @SerializedName("enabled") val isCreditEnchEnabled: Boolean
        )
    }

    fun checkProfileComplete(): UserProfileResponse {
        if (!profileComplete)
            throw ServerException.UserProfileNotCompleted("Please complete your profile")
        return this
    }
}