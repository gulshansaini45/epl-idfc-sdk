package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

/**
 * Data class that represents Pending Collect responses of a customers UPI payment
 */
data class PendingCollectMoneyResponse(
    @SerializedName("amount") val amount: String,
    @SerializedName("timestamp") val timeStamp: String,
    @SerializedName("txnId") val orgTxnId: String,
    @SerializedName("payeeVirAddr") val payeeVirAddr: String,
    @SerializedName("payeeName") val payeeName: String
)
