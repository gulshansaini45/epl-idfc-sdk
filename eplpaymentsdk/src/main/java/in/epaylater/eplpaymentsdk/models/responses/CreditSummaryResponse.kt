package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class CreditSummaryResponse(
    @SerializedName("accountStatus") val accountStatus: String,
    @SerializedName("limit") val limit: BigDecimal,
    @SerializedName("outstandingAmount") val outstandingAmount: BigDecimal,
    @SerializedName("isCreditPaused") val isCreditPaused: Boolean,
    @SerializedName("isFirstTimeUser") val isFirstTimeUser: Boolean,
    @SerializedName("totalOutstandingAmount") val totalOutstandingAmount: BigDecimal,
    @SerializedName("message") val message: String,
    @SerializedName("messageSeverity") val messageSeverity: String,
    @SerializedName("outstandingBreakup") val outstandingBreakup: List<OutstandingBreakup>,
    @SerializedName("pendingApprovalCount") val pendingApprovalCount: Int
) {
    data class OutstandingBreakup(
        @SerializedName("amountDescription") val amountDescription: String,
        @SerializedName("amount") val amount: BigDecimal
    )
}