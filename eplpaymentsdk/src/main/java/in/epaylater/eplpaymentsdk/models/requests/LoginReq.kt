package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

data class LoginReq(
    @SerializedName("customer") val customer: CustomerLoginReq,
    @SerializedName("location") val location: LocationReq,
    @SerializedName("bankAccountDetails") val bankAccountDetails: BankAccountDetails
)
