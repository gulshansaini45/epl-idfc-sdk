package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class EncMessageResponse(
    @SerializedName("encrypted_sms") val encMessage: String,
    @SerializedName("silent_sms_to") val silentSmsTo: String
)