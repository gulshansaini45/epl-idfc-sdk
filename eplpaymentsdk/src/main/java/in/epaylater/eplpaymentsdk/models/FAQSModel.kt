package `in`.epaylater.eplpaymentsdk.models

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
class FAQSModel : Parcelable {
    @IgnoredOnParcel
    var title: String? = ""
        public set
    @IgnoredOnParcel
    var answer: String? = ""
        public set
    @IgnoredOnParcel
    var isClicked: Boolean? = false
        public set
}