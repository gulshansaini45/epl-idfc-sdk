package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

data class PaymentReq(
    @SerializedName("outstandingAmount") val outstandingAmount: Double,
    @SerializedName("methodCode") val methodCode: String
)