package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class ScanAndPayResponse(
    @SerializedName("txnId") val txnId: String,
    @SerializedName("otpSent") val otpSent: String,
    @SerializedName("otpChannel") val otpChannel: String
)