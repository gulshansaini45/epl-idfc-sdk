package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

data class SendMoneyReq(
    @SerializedName("amount") val amount: String,
    @SerializedName("payee_vpa") val payeeVpa: String,
    @SerializedName("payee_code") val payeeCode: String,
    @SerializedName("payee_transfer_type") val payeeTransferType: String,
    @SerializedName("bharat_qr") val isBharatQr: Boolean,
    @SerializedName("source") val source: Source,
    @SerializedName("org_txn_id") val orgTxnId: String,
    @SerializedName("remarks") val remarks: String
) {
    enum class Source {
        INTENT, QRCODE
    }
}