package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

data class DeviceDataReq(
    @SerializedName("make") val make: String,
    @SerializedName("model") val model: String,
    @SerializedName("os") val os: String = "Android",
    @SerializedName("os_version") val osVersion: String,
    @SerializedName("app_version") val appVersion: String,
    @SerializedName("push_notification_id") val fcm: String,
    @SerializedName("sim_info") val simData: List<SimInfo>,
    @SerializedName("device_id") val deviceId: String
) {

    data class SimInfo(
        @SerializedName("imei") val imei: String,
        @SerializedName("sim_serial") val simSerial: String,
        @SerializedName("sim_slot_no") val simSlot: Int,
        @SerializedName("carrier") val carrier: String
    )
}