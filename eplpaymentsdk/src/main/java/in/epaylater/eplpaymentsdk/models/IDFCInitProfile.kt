package `in`.epaylater.eplpaymentsdk.models

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IDFCInitProfile(val builder: IDFCInitProfile.Builder) : Parcelable {

    @IgnoredOnParcel
    val custPhoneNumber: String?
    @IgnoredOnParcel
    val custfirstName: String?
    @IgnoredOnParcel
    val custlastName: String?
    @IgnoredOnParcel
    val custMiddleName: String?
    @IgnoredOnParcel
    val custEmailAddress: String?
    @IgnoredOnParcel
    val merchantSdkKey: String?
    @IgnoredOnParcel
    val odAccountNumber: String?
    @IgnoredOnParcel
    val savingAccountNumber: String?
    @IgnoredOnParcel
    val savingAccountName: String?
    @IgnoredOnParcel
    val custNameLinkedSavingAcc: String?
    @IgnoredOnParcel
    val fcmToken: String?
    @IgnoredOnParcel
    val custGender: String?
    @IgnoredOnParcel
    val latitude: Double?
    @IgnoredOnParcel
    val longitude: Double?
    @IgnoredOnParcel
    val accuracy: Double?


    init {
        this.custPhoneNumber = builder.custPhoneNumber
        this.custfirstName = builder.custfirstName
        this.custMiddleName = builder.custMiddleName
        this.custlastName = builder.custlastName
        this.custEmailAddress = builder.custEmailAddress
        this.merchantSdkKey = builder.merchantSdkKey
        this.odAccountNumber = builder.odAccountNumber
        this.savingAccountName = builder.savingAccountName
        this.savingAccountNumber = builder.savingAccountNumber
        this.custNameLinkedSavingAcc = builder.custNameLinkedSavingAcc
        this.fcmToken = builder.fcmToken
        this.custGender = builder.fcmToken

        this.latitude = builder.latitude
        this.longitude = builder.longitude
        this.accuracy = builder.accuracy
    }

    @Parcelize
    class Builder : Parcelable {
        @IgnoredOnParcel
        var custPhoneNumber: String? = ""
            private set
        @IgnoredOnParcel
        var custfirstName: String? = ""
            private set
        @IgnoredOnParcel
        var custlastName: String? = ""
            private set
        @IgnoredOnParcel
        var custMiddleName: String? = ""
            private set
        @IgnoredOnParcel
        var custEmailAddress: String? = ""
            private set
        @IgnoredOnParcel
        var merchantSdkKey: String? = ""
            private set
        @IgnoredOnParcel
        var odAccountNumber: String? = ""
            private set
        @IgnoredOnParcel
        var savingAccountNumber: String? = ""
            private set
        @IgnoredOnParcel
        var savingAccountName: String? = ""
            private set
        @IgnoredOnParcel
        var custNameLinkedSavingAcc: String? = ""
            private set
        @IgnoredOnParcel
        var fcmToken: String? = ""
            private set
        @IgnoredOnParcel
        var custGender: String? = ""
            private set
        @IgnoredOnParcel
        var latitude: Double? = 0.toDouble()
            private set
        @IgnoredOnParcel
        var longitude: Double? = 0.toDouble()
            private set
        @IgnoredOnParcel
        var accuracy: Double? = 0.toDouble()
            private set

        fun setCustPhoneNo(value: String) = apply {
            this.custPhoneNumber = value
        }

        fun setCustFirstName(value: String) = apply {
            this.custfirstName = value
        }

        fun setCustLastName(value: String) = apply {
            this.custlastName = value
        }

        fun setCustMiddleName(value: String) = apply {
            this.custMiddleName = value
        }

        fun setCustEmail(value: String) = apply {
            this.custEmailAddress = value
        }

        fun setMerchantSDKKey(value: String) = apply {
            this.merchantSdkKey = value
        }

        fun setCustODAccount(value: String) = apply {
            this.odAccountNumber = value
        }


        fun setSavingAccountNo(value: String) = apply {
            this.savingAccountNumber = value
        }

        fun setSavingAccountName(value: String) = apply {
            this.savingAccountName = value

        }

        fun setCustNameLinkedSavingAcc(value: String) = apply {
            this.custNameLinkedSavingAcc = value
        }

        fun setFCMToken(value: String) = apply {
            this.fcmToken = value
        }

        fun setGender(value: String) = apply {
            this.custGender = value
        }

        fun setLatitude(value: Double) = apply {
            this.latitude = value
        }

        fun setLongitude(value: Double) = apply {
            this.longitude = value
        }

        fun setAccuracy(value: Double) = apply {
            this.accuracy = value
        }

        fun build() = IDFCInitProfile(this)
    }
}