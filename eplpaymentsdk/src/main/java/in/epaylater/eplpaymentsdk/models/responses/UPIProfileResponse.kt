package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName


data class UPIProfileResponse(
  @SerializedName("imei") val imei: String,
  @SerializedName("sim_serial") val simSerial: String,
  @SerializedName("sim_slot_no") val simSlotNo: Int,
  @SerializedName("mobile") val mobile: String,
  @SerializedName("vpa") val vpa: String,
  @SerializedName("disabled") val disabled: Boolean,
  @SerializedName("sim_rebinding_required") val simRebindingRequired: Boolean,
  @SerializedName("pending_approval_count") val pendingApprovalCount: Int
)