package `in`.epaylater.eplpaymentsdk.models

import com.google.gson.annotations.SerializedName

data class MerchantCredModel(
    @SerializedName("merchantSDKKey") val sdkKey: String,
    @SerializedName("sha256") val sha256: String
)