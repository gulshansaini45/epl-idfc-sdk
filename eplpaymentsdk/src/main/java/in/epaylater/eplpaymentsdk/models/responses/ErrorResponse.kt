package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("type") val type: String,
    @SerializedName("reason") val reason: String,
    @SerializedName("code") val code: Int,
    @SerializedName("message") val message: String,
    @SerializedName("eplErrorCode") val eplErrorCode: String
)