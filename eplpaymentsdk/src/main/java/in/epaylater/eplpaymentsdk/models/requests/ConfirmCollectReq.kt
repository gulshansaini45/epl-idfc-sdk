package `in`.epaylater.eplpaymentsdk.models.requests

import com.google.gson.annotations.SerializedName

data class ConfirmCollectReq(
    @SerializedName("txn_id") val txnId: String,
    @SerializedName("user_action") val userAction: String,
    @SerializedName("otp") val otp: String
)