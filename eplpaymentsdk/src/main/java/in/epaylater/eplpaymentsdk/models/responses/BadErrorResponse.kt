package `in`.epaylater.eplpaymentsdk.models.responses

import com.google.gson.annotations.SerializedName

data class BadErrorResponse(
    @SerializedName("error") val error: List<String>
)