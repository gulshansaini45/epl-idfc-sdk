package `in`.epaylater.eplpaymentsdk.customviews

import `in`.epaylater.eplpaymentsdk.interfaces.TextChangeListner
import android.text.Editable
import android.text.TextWatcher

class CustomTextWatcher(val textChangeListner: TextChangeListner) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
    }
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        textChangeListner.onTextChange()
    }
}