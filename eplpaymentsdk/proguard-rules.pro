# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

##---------------Begin: proguard configuration common for all Android apps ----------
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-dontnote
-verbose
-dump class_files.txt
-printseeds seeds.txt
-printusage unused.txt
#-printmapping mapping.txt
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-allowaccessmodification

#-allowaccessmodification
#-renamesourcefileattribute SourceFile
#-repackageclasses ''

-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService


-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

# Understand the @Keep support annotation.
-keep class android.support.annotation.Keep

-keep @android.support.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}

# Explicitly preserve all serialization members. The Serializable interface
# is only a marker interface, so it wouldn't save them.
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# Preserve static fields of inner classes of R classes that might be accessed
# through introspection.
-keepclassmembers class **.R$* {
  public static <fields>;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#-keep public class * {
#    public protected *;
#}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

##---------------End: proguard configuration common for all Android apps ----------


#---------------Begin: proguard configuration for support library  ----------
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class com.actionbarsherlock.** { *; }
-keep interface com.actionbarsherlock.** { *; }

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn com.google.ads.**
##---------------End: proguard configuration for support apps  ----------



# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:



## --------------- Start Project specifics --------------- ##

-dontwarn com.google.gson.**
-dontwarn rx.internal.util.unsafe.**
-dontwarn dagger.android.**
-dontwarn com.roughike.bottombar.**
-dontwarn com.freshchat.consumer.sdk.**
-dontwarn com.squareup.picasso.**
-dontwarn org.apache.**
-dontwarn org.xmlpull.v1.**
-dontwarn org.simpleframework.**
-dontwarn com.scottyab.**

-keepattributes *Annotation*,EnclosingMethod
-keep class org.apache.http.** { *; }
-keep interface org.apache.http.** { *; }
-keep class org.npci.upi.** { *; }
#-keep class org.xmlpull.v1.** { *; }
-keep class javax.xml.** { *; }


# Advanced WebView
-keep class * extends android.webkit.WebChromeClient { *; }
-dontwarn im.delight.android.webview.**

#Secure Preference

-keep class com.tozny.crypto.android.AesCbcWithIntegrity$PrngFixes$* { *; }


# Crashlytics 2.+
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep class * extends java.lang.Exception
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**


# Updated as of Stetho 1.1.1
#
# Note: Doesn't include Javascript console lines. See https://github.com/facebook/stetho/tree/master/stetho-js-rhino#proguard
-keep class com.facebook.stetho.** { *; }
-keep class com.readystatesoftware.chuck.** { *; }


# Glide specific rules #
# https://github.com/bumptech/glide

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}


## GSON 2.2.4 specific rules ##

# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

-keepattributes EnclosingMethod

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }


# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**


# Okio
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**


# Retrofit 2.X
## https://square.github.io/retrofit/ ##
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}


-keepclassmembers class kotlin.Metadata {
    public <methods>;
}

# Niki.ai SDK

-keep public class com.niki.config.**

-keep public class com.niki.config.**{
  public protected *;
}

-keepclasseswithmembernames class * {
    native <methods>;
}
 -keep class **.R$* {
 <fields>;
}
-dontwarn com.squareup.picasso.**
-dontwarn retrofit2.**
-dontwarn okio.**
-dontwarn com.zendesk.util.**

-dontwarn com.fasterxml.jackson.databind.**
-dontwarn com.pubnub.**

-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
public static java.lang.String TABLENAME;
}
-keep class **$Properties

# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use Rx:
-dontwarn rx.**

-keep class org.greenrobot.greendao.**

-keep class android.support.v7.internal.** { *; }
-keep interface android.support.v7.internal.** { *; }
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }


# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.niki.models.** { *; }

-keep class com.google.gson.**
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

# Play services
-keep public class com.google.android.gms.* { public *; }
-dontwarn com.google.android.gms.**

# Keep Enum
-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}


# SFDC

# if excluding AltBeacon Library
-dontwarn org.altbeacon.**

#-keep class in.epaylater.eplpaymentsdk.managers.** { *; }
#-keepclasseswithmembernames class * { @in.epaylater.eplpaymentsdk.managers.** <fields>;}

#-keep class in.epaylater.eplpaymentsdk.utils.EPLCommonUtiltil#

#-keep class in.epaylater.eplpaymentsdk.fragments.Base
#-keepclasseswithmembernames class * { @in.epaylater.eplpaymentsdk.fragments.** <fields>;}
-keep class in.epaylater.eplpaymentsdk.managers.EPLManager {
   public <methods>;
}

-keep class in.epaylater.eplpaymentsdk.models.IDFCInitProfileModel$Builder { *; }

-keep enum in.epaylater.eplpaymentsdk.utils.EPLCommonUtil
-keepclassmembers enum in.epaylater.eplpaymentsdk.utils.EPLCommonUtil {
    public <methods>;
}

-keep class in.epaylater.eplpaymentsdk.fragments.EPLDashboardFragment{ *; }
-keep class in.epaylater.eplpaymentsdk.fragments.EPLBaseFragment{ *; }
-keep class in.epaylater.eplpaymentsdk.fragments.EPLHomeFragment{ *; }
-keep class in.epaylater.eplpaymentsdk.models.** { *; }
-keep class in.epaylater.eplpaymentsdk.constant.** { *; }
-keep class in.epaylater.eplpaymentsdk.managers.** { *; }

-keep class in.epaylater.eplpaymentsdk.exception.** { *; }
-keep class in.epaylater.eplpaymentsdk.interceptor.LiveNetworkInterceptor.NoNetworkException{ *; }


-keep interface in.epaylater.eplpaymentsdk.interfaces.EpayFragmentListner{*;}
-ignorewarnings

-keepclasseswithmembers class * {
    public <init>(android.app.Application, in.epaylater.eplpaymentsdk.models.IDFCInitProfileModel,android.support.v4.app.FragmentManager,int);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context,String,String);
}
-keepclasseswithmembers class * {
    public <init>(android.support.v4.app.Fragment,int);
}
-keepclasseswithmembers class * {
    public <init>(android.support.v4.app.Fragment);
}
## ---------------- End Project specifics ---------------- ##
